<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


$route['default_controller'] = 'front/home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['admin'] 		= 'admin/Login/index';
$route['do_login'] 		= 'admin/Login/do_login';
$route['admin/logout']  = 'admin/login/logout';
$route['my_profile']    = 'admin/login/my_profile';
$route['makeprofile']    = 'admin/login/makeprofile';
/***************************user********************************************/

$route['deluser']  = 'admin/user/deluser';
$route['edituser/(:num)'] = 'admin/user/edituser/$1';
$route['userupdate']	  = 'admin/user/userupdate';
$route['getcity'] 		  = 'admin/user/getcity';
/*****************************category*************************************/
$route['createcategory']      = 'admin/category/createcategory';
$route['addcategory'] 		  = 'admin/category/addcategory';
$route['editcategory/(:num)'] = 'admin/category/editcategory/$1';
$route['delcategory'] 		  = 'admin/category/delcategory';
$route['updatecategory'] 	  = 'admin/category/updatecategory';
$route['verifycategory']	  = 'admin/category/verifycategory';

/*****************************subcategory*************************************/
$route['createsubcategory']		 = 'admin/category/createsubcategory';
$route['addsubcategory'] 		 = 'admin/category/addsubcategory';
$route['updatesubcategory']		 = 'admin/category/updatesubcategory';
$route['editsubcategory/(:num)'] = 'admin/category/editsubcategory/$1';
$route['delsubcategory']         = 'admin/category/delsubcategory';
$route['verifysubcategory'] 	 = 'admin/category/verifysubcategory';

/*****************************attribute*************************************/

$route['editattribute/(:num)'] = 'admin/attribute/editattribute/$1';
$route['createattribute'] = 'admin/attribute/createattribute';
$route['editattribute/(:num)'] = 'admin/attribute/createattribute/$1';
$route['addattribute'] = 'admin/attribute/addattribute';
$route['delattribute'] = 'admin/attribute/delattribute';
$route['updateattribute'] = 'admin/attribute/updateattribute';

/*****************************coupons*************************************/

$route['coupon']      = 'admin/coupon';
$route['createcoupon']      = 'admin/coupon/createcoupon';
$route['addcoupon']         = 'admin/coupon/addcoupon';
$route['delcoupon']  = 'admin/coupon/delcoupon';
$route['editcoupon/(:num)'] = 'admin/coupon/editcoupon/$1';
$route['updatecoupon'] 		= 'admin/coupon/updatecoupon';

/*****************************Review*************************************/

$route['showreview/(:num)'] 		= 'admin/review/showreview/$1';
$route['viewreview/(:num)'] 		= 'admin/review/viewreview/$1';
$route['approvereview/(:num)'] 		= 'admin/review/approvereview/$1';
$route['proapprovereview/(:num)'] 	= 'admin/review/proapprovereview/$1';
$route['rejectview/(:num)'] 		= 'admin/review/rejectview/$1';
$route['prorejectreview/(:num)'] 	= 'admin/review/prorejectreview/$1';



/*****************************Address*************************************/

$route['address/(:any)'] = 'admin/address/addr_list/$1';

/*****************************product*************************************/
$route['createproduct'] 		= 'admin/product/createproduct';
$route['addproduct'] 		    = 'admin/product/addproduct';
$route['addproduct_action'] 		    = 'admin/product/addproduct_action';
$route['getsubcategory'] 		= 'admin/product/getsubcategory';
$route['editproduct/(:num)'] 	= 'admin/product/editproduct/$1';
$route['editproduct_action'] 		    = 'admin/product/editproduct_action';
$route['delproduct'] 			= 'admin/product/delproduct';
$route['getattributeval'] 		= 'admin/product/getattributeval';
$route['product_adddetail/(:num)'] 	= 'admin/product/product_adddetail/$1';
$route['addprodetail_action'] 		= 'admin/product/addprodetail_action';
$route['gallery/(:num)'] 		= 'admin/product/gallery/$1';
$route['gallery_action'] 		= 'admin/product/gallery_action';


$route['creategallery'] 		= 'admin/gallery/creategallery';
$route['addgallery'] 			= 'admin/gallery/addgallery';
$route['delgallery']		 	= 'admin/gallery/delgallery';
$route['editgallery/(:num)'] 	= 'admin/gallery/editgallery/$1';
$route['updategallery'] 		= 'admin/gallery/updategallery';
$route['catgallery/(:num)'] 	= 'admin/gallery/catgallery/$1';






$route['login'] 		= 'front/home/login';



/*$route['editattribute/(:num)'] = 'admin/attribute/editattribute/$1';*/
$route['createattribute'] = 'admin/attribute/createattribute';
$route['editattribute/(:num)'] = 'admin/attribute/createattribute/$1';
$route['addattribute'] = 'admin/attribute/addattribute';
$route['delattribute/(:num)'] = 'admin/attribute/delattribute/$1';
$route['updateattribute'] = 'admin/attribute/updateattribute';


/*$route['dashboard'] = 'admin/dashboard';*/
/*$route['userlist'] = 'admin/user/userlist';*/

/*****************************payment*************************************/
$route['payment']               = 'admin/payment';
$route['createpayment']         = 'admin/payment/createpayment';
$route['addpayment'] 		    = 'admin/payment/addpayment';
$route['verifypayment']	        = 'admin/payment/verifypayment';
$route['delpayment']	  	    = 'admin/payment/delpayment';
$route['editpayment/(:num)']	= 'admin/payment/editpayment/$1';
$route['paymentdetails/(:num)']	= 'admin/payment/paymentdetails/$1';
$route['updatepayment']	        = 'admin/payment/updatepayment';
$route['addpdetail_form']	    = 'admin/payment/addpdetail_form';


/****************************wishlist***************************************/
$route['wishlist'] = 'admin/wishlist';

/****************************order***************************************/

$route['orders']                  = 'admin/orders';
$route['vieworderdetails/(:num)'] = 'admin/orders/vieworderdetails/$1';
$route['statuschange_order']      = 'admin/orders/statuschange_order';
$route['invoice/(:num)']          = 'admin/orders/invoice/$1';

/****************************settings***************************************/
$route['settings']  = 'admin/settings';
$route['genderal_settings']  = 'admin/settings/genderal_settings';
$route['social_settings']  = 'admin/settings/social_settings';

/****************************extra settings***************************************/
$route['createfield']     = 'admin/extrasetting/createfield';
$route['addextrafield']   ='admin/extrasetting/addextrafield';
$route['delfield'] 		  ='admin/extrasetting/delfield';
$route['editfield/(:num)'] ='admin/extrasetting/createfield/$1';
$route['updateextrafield']     = 'admin/extrasetting/updateextrafield';

/****************************extra settings***************************************/
$route['updatestore']     = 'admin/store/updatestore';
$route['dellogo']     = 'admin/store/dellogo';

/****************************template*************************************/
$route['createtemplate']     = 'admin/template/createtemplate';
$route['addtemplate']     = 'admin/template/addtemplate';
$route['updatetemplate']     = 'admin/template/updatetemplate';
$route['deltempate']     = 'admin/template/deltempate';
$route['edittemplate/(:num)']     = 'admin/template/createtemplate/$1';

/****************************Pages*************************************/
$route['createpages']    	 = 'admin/pages/createpages';
$route['addpages']     		 = 'admin/pages/addpages';
$route['updatepages']        = 'admin/pages/updatepages';
$route['delpages']           = 'admin/pages/delpages';
$route['editpages/(:num)']   = 'admin/pages/createpages/$1';






/********************************************************/
/********************************************************/
/*********************Front Side ************************/
/********************************************************/
/********************************************************/

/****************Register and Login and forgot **************/

$route['login'] = 'front/Login/index';
$route['login_action'] = 'front/Login/login_action';
$route['register'] = 'front/Login/register';
$route['email_exist'] = 'front/Login/email_exist';
$route['register_action'] = 'front/Login/register_action';
$route['forgotpass'] = 'front/Login/forgotpass';
$route['forgotpass_action'] = 'front/Login/forgotpass_action';
$route['resetpass_action'] = 'front/Login/resetpass_action';
$route['logout_front'] = 'front/Login/logout_front';
$route['getview/(:num)'] = 'front/Login/getview/$1';
$route['addwishlist/(:num)'] = 'front/home/addwishlist/$1';
$route['showpages/(:num)']    	 = 'front/home/showpages/$1';


/********************My account*****************************/
$route['userprofile'] = 'front/Account/Profile';
$route['profile_action'] = 'front/Account/profile_action';
$route['propass_action'] = 'front/Account/propass_action';
$route['getallcity'] = 'front/Account/getallcity';
$route['showwishlist'] = 'front/Account/showwishlist';
$route['showorders'] = 'front/Account/showorders';
$route['orderdetail/(:num)'] = 'front/Account/orderdetail/$1';
$route['customeraddress'] = 'front/Account/customeraddress';
$route['remove_wishlist/(:num)'] = 'front/Account/remove_wishlist/$1';

/*$route['frontadd/(:any)'] = 'front/Account/addr_list/$1';*/


/*********************product******************************/
$route['product_detail/(:num)']    = 'front/product/product_detail/$1';
$route['quick_view/(:num)']    = 'front/product/quick_view/$1';
$route['get_proprice'] 	           = 'front/product/get_proprice';
$route['add_user_wishlist/(:num)'] = 'front/product/add_user_wishlist/$1';
$route['reviewform_action'] 	   = 'front/product/reviewform_action';
$route['productlist'] 		= 		'front/product/productlist';
$route['search_prolist_filter'] = 'front/product/search_prolist_filter';
$route['filterprice_product'] = 'front/product/filterprice_product';
$route['search_pro/(:num)'] 		= 		'front/product/search_pro/$1';
$route['productlist/(:num)'] 		= 		'front/product/pro/$1';
$route['getsize'] = 'front/product/getsize';
$route['search_size_pro'] = 'front/product/search_size_pro';
$route['all_search_action'] = 'front/product/all_search_action';

/***************************gallery*****************************************/
$route['gallerylist']   = 'front/gallery/index';
/**********************cart *****************************************/

$route['addtocart/(:num)']   = 'front/cart/addtocart/$1';
$route['addtocart_cart/(:num)']   = 'front/cart/addtocart_cart/$1';
$route['addtocart_shop/(:num)']   = 'front/cart/addtocart_shop/$1';
$route['cart_view']   = 'front/cart/cart_view';
$route['remove_cart/(:num)']   = 'front/cart/remove_cart/$1';
$route['cart_update']   = 'front/cart/cart_update';
$route['checkout']   = 'front/cart/checkout';
$route['fetchaddress']   = 'front/cart/fetchaddress';
$route['checkaddress']   = 'front/cart/checkaddress';
$route['otheradd']   = 'front/cart/otheradd';
$route['emptyshopcart']   = 'front/cart/emptyshopcart';

/******************** User payment**********************************************/
$route['userpayment']   = 'front/payment/userpayment';

/************************order****************************************************/
$route['createorder']   = 'front/orders/createorder';
$route['showcustomerorder/(:num)']   = 'front/orders/showcustomerorder/$1';





