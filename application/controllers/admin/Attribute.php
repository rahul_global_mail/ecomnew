<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attribute extends CI_Controller {

    public function __construct(){

    	parent::__construct();

    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		if($this->authentication_login() === 0){
			return redirect('admin');
		}

    }

    	public function index()
	{
		$attributes = Attributes::all();
		$this->load->view('admin/attribute/index',compact('attributes'));
	}

	public function createattribute(){
		$editid = $this->uri->segment(2);
		$catinfo = Categories::where('_Main_id','!=',0)->get()->toarray();

		$types = [
			'textbox' => 'Textbox', 
			'radio' => 'Radio', 
			'select' => 'Select', 
			'checkbox' => 'Checkbox'
		];
		if($editid) {
		  	$attribute =  Attributes::where("_ID",$editid)->first();
		  	$pagetitle = "Edit Attribute";
		  	$this->load->view('admin/attribute/edit',compact('editid','attribute', 'pagetitle', 'types','catinfo'));
		} else {
			$pagetitle = "Add Attribute";
		  	$this->load->view('admin/attribute/edit', compact('pagetitle', 'types','catinfo'));
		}
	}

	public function addattribute(){
		$name = $this->input->post('name');
		$values = $this->input->post('values');
		$type = $this->input->post('type');
		$catid = $this->input->post('category');
		$attribute  = new Attributes();

        	$attribute->_Name = $name ;
        	$attribute->_Values = $values ;
        	$attribute->_Type  = $type;
        	$attribute->_Subcat  = $catid;
        	$attribute->_Created  = date('Y-m-d H:i:s');
        	$attribute->save();
        	$res = ['type' => 'success' , 'msg' => 'Attribute created successfully ','url'=>'admin/attribute', 'result' => true];
        	echo json_encode($res);
        	exit;
	}

	public function updateattribute(){
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$values = $this->input->post('values');
		$type = $this->input->post('type');
		$catid = $this->input->post('category');
		$attribute = Attributes::where("_ID",$id)->first();

<<<<<<< HEAD
			$attribute->_Name = $name ;
=======
		$attribute->_Name = $name ;
>>>>>>> 1387377960807901e00344fd01a10caa7cc72e84
        	$attribute->_Values = $values ;
        	$attribute->_Type  = $type;
        	$attribute->_Subcat  = $catid;
        	$attribute->_Created  = date('Y-m-d H:i:s');
        	$attribute->save();
        	$res = ['type' => 'success' , 'msg' => 'Attribute updated successfully ','url'=>'admin/attribute', 'result' => true];
        	echo json_encode($res);
        	exit;
	}

<<<<<<< HEAD
	public function delattribute(){
	if($this->input->post('id'))
  {
    $checkbox_value = $this->input->post('id');
    for($count = 0; $count < count($checkbox_value); $count++)
   {
		$affectedRows =Attributes::find($checkbox_value[$count])->delete(); 

   }
   if($affectedRows == true){
=======
	public function delattribute($id){
		$affectedRows = Attributes::find($id)->delete();
		
		if($affectedRows == true){
>>>>>>> 1387377960807901e00344fd01a10caa7cc72e84
			$res = ['type' => 'success' , 'msg' => 'Attribute deleted successfully ','url'=>'admin/attribute', 'result' => true];
		}else{
			$res = ['type' => 'error' , 'msg' => 'Attribute not deleted/ ','url'=>'admin/attribute', 'result' => true];
		}
<<<<<<< HEAD
  
    echo json_encode($res);
    exit;
   
  }
=======
		echo json_encode($res);
        	exit;
>>>>>>> 1387377960807901e00344fd01a10caa7cc72e84
	}

	public function authentication_login(){
		$log_id=$this->encryption->decrypt($this->input->cookie('adminid'));
        	if($log_id > 0){
			return $log_id;
        	}
       		return 0;
    	}
}
?>   