<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PHPImageWorkshop\ImageWorkshop;

class Category extends CI_Controller {

    public function __construct(){

    	parent::__construct();

    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		if($this->authentication_login() === 0){
			return redirect('admin');
		}
    }

    public function index()
	{
		$allcatedata =  Categories::where('_Main_id',0)->get()->toarray();
		$this->load->view('admin/category/index',compact('allcatedata'));
	}

	public function createcategory(){
		$editid = $this->uri->segment(2);
		if($editid)
		{
		  $editcat =  Categories::where("_ID",$editid)->first();
		  $this->load->view('admin/category/category_add',compact('editid',compact('editcat')));
		}
		else
		{	
		  $this->load->view('admin/category/category_add');
		}
	}

	public function addcategory(){
		$catname = $this->input->post('catname');
		$catdes = $this->input->post('catdes');
		$cattab  = new Categories();

		$path =  "assets/uploads/category";
        $config['upload_path']   = $path;
        $config['allowed_types'] = 'jpeg|jpg|png';
        $config['max_size']      = 10000000;
       $this->load->library('upload');
       $this->upload->initialize($config);
       
        if (!$this->upload->do_upload('catimage')) {
            $error = array('error' => $this->upload->display_errors());
        } 
        else {
            $data = $this->upload->data();
            $cattab->_Image = $data['file_name'];
        }
        $cattab->_Name        = $catname ;
        $cattab->_Description = $catdes ;
        $cattab->_Main_id  = 0;
        $cattab->_Created  = date('Y-m-d H:i:s');
        $cattab->save();
        $res = ['type' => 'success' , 'msg' => 'category created successfully ','url'=>'admin/category', 'result' => true];
        echo json_encode($res);
        exit;
		
	}


	public function delcategory(){

    if($this->input->post('id'))
    {
	    $checkbox_value = $this->input->post('id');
	    for($count = 0; $count < count($checkbox_value); $count++)
	   {
			$catdel = Categories::where('_ID',$checkbox_value[$count])->first()->toArray();
			Categories::find($catdel['_ID'])->delete();
		    if($catdel['_Image']!='')
		    {  
				unlink('assets/uploads/category/'.$catdel['_Image']);
		    }
	   }
	   $res = ['type' => 'success' , 'msg' => 'Category  Deleted successfully ','url'=>'admin/category', 'result' => true];
	    echo json_encode($res);
	    exit;
    }

	}

	public function editcategory($id){
		$editcat =  Categories::where("_ID",$id)->first();
		$editid = $id;
		$this->load->view('admin/category/category_add',compact('editcat','editid'));
	}

	public function updatecategory(){
		$hcatid = $this->input->post('hcatid');
		$catname = $this->input->post('catname');
		$catdes = $this->input->post('catdes');
		$catresult = Categories::where("_ID",$hcatid)->first();

		$path =  "assets/uploads/category";
        $config['upload_path']   = $path;
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size']      = 10000000;
       $this->load->library('upload');
       $this->upload->initialize($config);
       
        if (!$this->upload->do_upload('catimage')) {
            $error = array('error' => $this->upload->display_errors());
        } 
        else {
            $data = $this->upload->data();
            if(!empty($data['file_name']))
            {
                $catresult->_Image = $data['file_name'];
               
            }
            else
            {
              $catresult->_Image =$catresult->_Image;
              
            }
           
        }
		$catresult->_Name = $catname;
		$catresult->_Description = $catdes ;
        $catresult->_Main_id  = 0;
        $catresult->_Created  = date('Y-m-d H:i:s');
        $catresult->save();
        $res = ['type' => 'success' , 'msg' => 'category updated successfully ','url'=>'admin/category', 'result' => true];
        echo json_encode($res);
        exit;

	}

	public function verifycategory(){
		
		if($this->input->post('_catname') == $this->input->post('catname')){
			echo 'true';
		}else {
			
			$c = Categories::where('_Name', $this->input->post('catname'))
			->where('_Main_id',0)
			->get()->toarray();
			if(count($c)> 0)
			{
				echo 'false';
			}
			else
			{
				echo 'true';
			}
		}
	}



	public function verifysubcategory(){
		
		if($this->input->post('_subcatname') == $this->input->post('subcatname')){
			echo 'true';
		}else {
			
			$c = Categories::where('_Name', $this->input->post('subcatname'))
			->where('_Main_id','!=',0)
			->get()->toarray();

			if(count($c)> 0)
			{
				echo 'false';
			}
			else
			{
				echo 'true';
			}
		}
	}

	public function subcategory(){
		$subcatedata =  Categories::where('_Main_id','!=',0)->get()->toarray();
		$this->load->view('admin/subcategory/subcategory',compact('subcatedata'));

	}

	public function createsubcategory(){
		$editsubcatid = $this->uri->segment(2);
		
		if($editsubcatid)
		{
		  $editsubcat =  Categories::where("_ID",$editid)->first();
		  $this->load->view('admin/subcategory/subcategory_add',compact('editsubcatid',compact('editsubcat')));
		}
		else
		{	
			$allcatedata =  Categories::where('_Main_id',0)->get()->toarray();
		  $this->load->view('admin/subcategory/subcategory_add',compact('allcatedata'));
		}
	}

	public function addsubcategory(){
		$res = [];
		$subcatname = $this->input->post('subcatname');
		$parent_id = $this->input->post('parent_id');
		$subcatdes = $this->input->post('subcatdes');
		$subcattab  = new Categories();

		$path =  "assets/uploads/subcategory/";
        $config['upload_path']   = $path;
        $config['allowed_types'] = '*';
        $config['max_size']      = 10240;
        $config['file_ext_tolower'] = TRUE;
        $qwe = pathinfo($_FILES['subcatimage']['name']);

        $microtime = microtime(true);
        $m = str_replace('.','_', $microtime);
			$file_name = $m.'_SUBCAT';

			$filepath = $config['upload_path'] . $file_name .".". $qwe['extension'];

        	$config['file_name']             = $file_name.".". $qwe['extension'];

       $this->load->library('upload');
       $this->upload->initialize($config);
       
        if (!$this->upload->do_upload('subcatimage')) {
            $error = array('error' => $this->upload->display_errors());
        } 
        else {
            $data = $this->upload->data();
            $pinguLayer = ImageWorkshop::initFromPath($config['upload_path'].$file_name.'.'.$qwe['extension']);

            $pinguLayer->resizeInPixel(500, 285, true);
            $dirPath = $config['upload_path'];
            $filename = $file_name."_small.".$qwe['extension'];
            $createFolders = true;  
            $backgroundColor = null;
            $imageQuality = 95;
            $pinguLayer->save($dirPath, $filename, $createFolders, $backgroundColor, $imageQuality);
           
            $imageData['small'] = $filename;
            /*array_push($res, $imageData);*/

            $subcattab->_Image = $imageData['small'];
        }
        $subcattab->_Name        = $subcatname ;
        $subcattab->_Description = $subcatdes ;
        $subcattab->_Main_id  	 = $parent_id;
        $subcattab->_Created     = date('Y-m-d H:i:s');
        $subcattab->save();

        $res = ['type' => 'success' , 'msg' => 'subcategory created successfully ','url'=>'admin/category/subcategory', 'result' => true];
        echo json_encode($res);
        exit;
		
	}


	public function delsubcategory(){
    if($this->input->post('id'))
    {
	    $checkbox_value = $this->input->post('id');
	    for($count = 0; $count < count($checkbox_value); $count++)
	   {
			$subcatdel = Categories::where('_ID',$checkbox_value[$count])->first()->toArray();
			Categories::find($subcatdel['_ID'])->delete();
		    if($subcatdel['_Image']!='')
		    {
		    	$imgarry = explode("_small", $subcatdel['_Image']);
				unlink('assets/uploads/subcategory/'.$imgarry[0].$imgarry[1]);
				unlink('assets/uploads/subcategory/'.$subcatdel['_Image']);
		    }
	   }
	   $res = ['type' => 'success' , 'msg' => 'Subcategory  Deleted successfully ','url'=>'admin/category/subcategory', 'result' => true];
	    echo json_encode($res);
	    exit;
    }

	}

	public function editsubcategory($id){
		$editsubcat =  Categories::where("_ID",$id)->first();
		$editsubcatid = $id;
		$allcatedata =  Categories::where('_Main_id',0)->get()->toarray();
		$this->load->view('admin/subcategory/subcategory_add',compact('editsubcat','editsubcatid','allcatedata'));
	}

	public function updatesubcategory(){
		$hsubcatid = $this->input->post('hsubcatid');
		$subcatname = $this->input->post('subcatname');
		$subcatdes = $this->input->post('subcatdes');
		$parent_id = $this->input->post('parent_id');
		$subcatresult = Categories::where("_ID",$hsubcatid)->first();

		$path =  "assets/uploads/subcategory/";
        $config['upload_path']   = $path;
        $config['allowed_types'] = '*';
        $config['max_size']      = 10240;
        $config['file_ext_tolower'] = TRUE;

        if($_FILES['subcatimage']['name'] != '')
        {
        	$qwe = pathinfo($_FILES['subcatimage']['name']);
	        $microtime = microtime(true);
	        $m = str_replace('.','_',$microtime);
			$file_name = $m.'_SUBCAT';
			if($qwe!='')
			{
				$filepath = $config['upload_path'] . $file_name .".". $qwe['extension'];

        		$config['file_name']             = $file_name.".". $qwe['extension'];
			}
			
        }
       


       $this->load->library('upload');
       $this->upload->initialize($config);
       
        if (!$this->upload->do_upload('subcatimage')) {
            $error = array('error' => $this->upload->display_errors());
        } 
        else {
            $data = $this->upload->data();
            $pinguLayer = ImageWorkshop::initFromPath($config['upload_path'].$file_name.'.'.$qwe['extension']);
            $pinguLayer->resizeInPixel(500, 285, true);
            $dirPath = $config['upload_path'];
            $filename = $file_name."_small.".$qwe['extension'];
            $createFolders = true;  
            $backgroundColor = null;
            $imageQuality = 95;
            $pinguLayer->save($dirPath, $filename, $createFolders, $backgroundColor, $imageQuality);
            $imageData['small'] = $filename;
            if(!empty($data['file_name']))
            {
              $subcatresult->_Image =$imageData['small'];
               
            }
            else
            {
              $subcatresult->_Image = array_merge($subcatresult->_Image,$imageData['small']);
            }
           
        }
		$subcatresult->_Name         = $subcatname;
		$subcatresult->_Description  = $subcatdes ;
        $subcatresult->_Main_id      = $parent_id;
        $subcatresult->_Created      = date('Y-m-d H:i:s');
        $subcatresult->save();
        $res = ['type' => 'success' , 'msg' => 'subcategory updated successfully ','url'=>'admin/category/subcategory', 'result' => true];
        echo json_encode($res);
        exit;

	}


	public function authentication_login(){
		$log_id=$this->encryption->decrypt($this->input->cookie('adminid'));
        if($log_id > 0){
			return $log_id;
        }
       return 0;
    }
}
?>   