<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Extrasetting extends CI_Controller {

    public function __construct(){

    	parent::__construct();

    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		if($this->authentication_login() === 0){
			return redirect('admin');
		}
    }

    public function authentication_login(){
        $log_id=$this->encryption->decrypt($this->input->cookie('adminid'));
        if($log_id > 0){
            return $log_id;
        }
       return 0;
    }

    public function index()
	{
        $fieldinfo = Extrafield::get()->toarray();
		$this->load->view('admin/extrasetting/index',compact('fieldinfo'));
	}
    public function createfield(){
        $editid = $this->uri->segment(2);
        if($editid) {
            $editfield = Extrafield::where("_ID",$editid)->first();
            $pagetitle = "Edit Field";
            $this->load->view('admin/extrasetting/edit',compact('editid', 'pagetitle','editfield'));
        }
        else
        {
            $pagetitle = "Add Field";
            $this->load->view('admin/extrasetting/edit',compact('pagetitle'));
        }
    }
    public function addextrafield(){
        $title1 = $this->input->post('title1');
        $title2 = $this->input->post('title2');
        
        $extratab  = new Extrafield();

        $path =  "assets/uploads/setting";
        $config['upload_path']   = $path;
        $config['allowed_types'] = 'jpeg|jpg|png';
        $config['max_size']      = 10000000;
        $fileinfo = pathinfo($_FILES['img_name']['name']);
        $new_name = $fileinfo['filename'].'_'.'large'.'.'.$fileinfo['extension'];
        $config['file_name'] = $new_name;
       $this->load->library('upload');
       $this->upload->initialize($config);
       
        if (!$this->upload->do_upload('img_name')) {
            $error = array('error' => $this->upload->display_errors());
        } 
        else {
            $data = $this->upload->data();
            
            $extratab->_Image = $new_name;
        }
        $extratab->_Title1 = $title1;
        $extratab->_Title2 = $title2;
        $extratab->_Created  = date('Y-m-d H:i:s');
        $extratab->save();
        $res = ['type' => 'success' , 'msg' => 'Filed created successfully ','url'=>'admin/Extrasetting', 'result' => true];
        echo json_encode($res);
        exit;
    }

    public function delfield()
    {
        if($this->input->post('id'))
        {
            $checkbox_value = $this->input->post('id');
            for($count = 0; $count < count($checkbox_value); $count++)
           {
                $affectedRows = Extrafield::where('_ID',$checkbox_value[$count])->first()->toArray();
                Extrafield::find($affectedRows['_ID'])->delete();
            if($affectedRows['_Image']!='')
            {  
                unlink('assets/uploads/setting/'.$affectedRows['_Image']);
            }
          }
           if($affectedRows == true){
                        $res = ['type' => 'success' , 'msg' => 'Setting deleted successfully ','url'=>'admin/Extrasetting', 'result' => true];
                    }else{
                        $res = ['type' => 'error' , 'msg' => 'Setting not deleted/ ','url'=>'admin/Extrasetting', 'result' => true];
                    }
                    echo json_encode($res);
                        exit;
        }
    }

    public function updateextrafield(){
        $title1 = $this->input->post('title1');
        $title2 = $this->input->post('title2');
        $himg = $this->input->post('himg');
        $hfid = $this->input->post('hfid');
        if(!empty($_FILES['img_name']['name']))
        {
            $fileinfo = pathinfo($_FILES['img_name']['name']);
        }
        $updextratab  = Extrafield::where("_ID",$hfid)->first();

        $path =  "assets/uploads/setting";
        $config['upload_path']   = $path;
        $config['allowed_types'] = 'jpeg|jpg|png';
        $config['max_size']      = 10000000;
        if(!empty($fileinfo) && $fileinfo != '')
        {
        $new_name = $fileinfo['filename'].'_'.'large'.'.'.$fileinfo['extension'];
        $config['file_name'] = $new_name;
        }
        
       $this->load->library('upload');
       $this->upload->initialize($config);
       
        if (!$this->upload->do_upload('img_name')) {
            $error = array('error' => $this->upload->display_errors());
        } 
        else {
            $data = $this->upload->data();
            if(!empty($data['file_name']))
            {
                $updextratab->_Image = $new_name;
               
            }
            else
            {
              $updextratab->_Image =$himg;
              
            }
            
        }
        $updextratab->_Title1 = $title1;
        $updextratab->_Title2 = $title2;
        $updextratab->_Created  = date('Y-m-d H:i:s');
        $updextratab->save();
        $res = ['type' => 'success' , 'msg' => 'Filed Updated successfully ','url'=>'admin/Extrasetting', 'result' => true];
        echo json_encode($res);
        exit;
    }

}