<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PHPImageWorkshop\ImageWorkshop;

class Gallery extends CI_Controller {

    public function __construct(){

    	parent::__construct();

    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		if($this->authentication_login() === 0){
			return redirect('admin');
		}

    }

    public function index()
	{
		$galldetail = Galleries::get()->toarray();
		$this->load->view('admin/gallery/index',compact('galldetail'));
	}

	public function creategallery()
	{
		$this->load->view('admin/gallery/creategallery');
	}

	public function addgallery()
	{
		$url = $this->input->post('url');
        $title = $this->input->post('title');
        $galdata = New Galleries();
		$galdata->_Title = $title;
		$galdata->_Url =   $url;
		$galdata->_Created = date('Y-m-d H:i:s');
		$galdata->save();
		$data = [
	    	"msg" => "Gallery created successfully",
	    	"type" => "success",
	    	"url"=>"admin/gallery", 
	    	'result' => true

    	];
    	echo json_encode($data);
    	exit();
		
	}

	public function updategallery()
	{
		$url = $this->input->post('url');
        $title = $this->input->post('title');
        $gid = $this->input->post('gid');
        $galedit = Galleries::where('_ID',$gid)->first();
		$galedit->_Title = $title;
		$galedit->_Url =   $url;
		$galedit->_Created = date('Y-m-d H:i:s');
		$galedit->save();
		$data = [
	    	"msg" => "Gallery updated successfully",
	    	"type" => "success",
	    	"url"=>"admin/gallery", 
	    	'result' => true

    	];
    	echo json_encode($data);
    	exit();
		
	}

	public function catgallery($id){
		$data = Galleries::where('_ID',$id)
						->first()
						->toArray();
		
		$this->load->view('admin/gallery/catimage',compact('data'));
	}


	public function editImage(){
		$gcid = $this->input->post('gcid');
		$files = $_FILES;
        $res = [];
		
		for($i=0; $i< count($_FILES['proImage']['name']); $i++) {
        	$_FILES['proimage']['name']= $files['proImage']['name'][$i];
        	$_FILES['proimage']['type']= $files['proImage']['type'][$i];
        	$_FILES['proimage']['tmp_name']= $files['proImage']['tmp_name'][$i];
        	$_FILES['proimage']['error']= $files['proImage']['error'][$i];
        	$_FILES['proimage']['size']= $files['proImage']['size'][$i];

            $config['upload_path']          = 'assets/uploads/gallery/';
        	//$config['allowed_types']        = 'jpg|gif|png|jpeg|JPG|PNG|webp';
        	$config['allowed_types']        = '*';
        	$config['max_size']             = 10240;
        	$config['file_ext_tolower'] = TRUE;

        	$qwe = pathinfo($_FILES['proimage']['name']);

        	$microtime = microtime(true);
        	$m = str_replace('.','_', $microtime);
			$file_name = $m.$this->input->post('product_id');

			$filepath = $config['upload_path'] . $file_name .".". $qwe['extension'];

        	$config['file_name']             = $file_name.".". $qwe['extension'];
        	$this->upload->initialize($config);
            if ( ! $this->upload->do_upload('proimage')) {
                    $error = array('error' => $this->upload->display_errors());
            } else {
                $data = array('upload_data' => $this->upload->data());
            	

                $pinguLayer3 = ImageWorkshop::initFromPath($config['upload_path'].$file_name.'.'.$qwe['extension']);
                $pinguLayer3->resizeInPixel(960, 750, true);

                $dirPath = $config['upload_path'];
           
                $createFolders = true;  
                $backgroundColor = null;
                $imageQuality = 95;
          
                $imageData['name'] = $file_name.".". $qwe['extension'];
             /*   $imageData['thumb'] = $filename1;
                $imageData['medium'] = $filename2;*/
                /*$imageData['cat'] = $filename3;*/
              /*  $imageData['large'] = $filename;*/
                /*$imageData['size'] = $_FILES['proimage']['size'];*/
                array_push($res, $imageData);
            }
		}
		
		$galedit = Galleries::where('_ID',$gcid)->first();
		$exist_gal = $galedit['_Image'];
			if($exist_gal){
	        $final_gal = array_merge($exist_gal, $res);
    	}else{
	        $final_gal = $res;
		}

		$galedit->_Image = $final_gal;
		$galedit->_Created = date('Y-m-d H:i:s');
		$galedit->save();
		$data = [
	    	"msg" => "Image upload successfully",
	    	"type" => "success",
	    	"url"=>"admin/gallery", 
	    	'result' => true


    	];
    	header('Content-type: application/json');
    	echo json_encode($data);
    	exit();
		

	}

	public function deleteImage() {
		$target_dir = "assets/uploads/gallery/";
		$id = $this->input->post('gallery_id');
		 $mainname = $this->input->post('mainname');

		$gallerydata = Galleries::find($id);
	    	$exist_gal = $gallerydata['_Image'];
	    	
	    	$new_arr = [];
	    	foreach ($exist_gal as $key => $value) {
		        if($value['name'] == $mainname){

		        	if(file_exists($target_dir.$value['name'])){
				        unlink($target_dir.$value['name']);
			    	}
		        }else{
		            $new_arr[] = $value;
		        }
	    	}

	    	$gallerydata->_Image = $new_arr;
	    	$gallerydata->save();

	    	$data = [
		    	"msg" => "Image delete successfully",
		    	"type" => "success"
	    	];
	    	header('Content-type: application/json');
	    	echo json_encode($data);exit();
	}


	public function delgallery()
	{
		if($this->input->post('id'))
  		{
	    	$checkbox_value = $this->input->post('id');
		    for($count = 0; $count < count($checkbox_value); $count++)
		   {
				$affectedRows = Galleries::where('_ID',$checkbox_value[$count])->first()->toArray();
				Galleries::find($affectedRows['_ID'])->delete();
					foreach ($affectedRows['_Image'] as $key => $value) {
			        	if(file_exists('assets/uploads/gallery/'.$value['name'])){
					        unlink('assets/uploads/gallery/'.$value['name']);
						}
					}
		   }
		   if($affectedRows == true){
				$res = ['type' => 'success' , 'msg' => 'Gallery deleted successfully ','url'=>'admin/gallery', 'result' => true];
			}else{
				$res = ['type' => 'error' , 'msg' => 'Gallery not deleted/ ','url'=>'admin/gallery', 'result' => true];
			}
			echo json_encode($res);
	        	exit;
  		}

	}

	public function editgallery($id)
	{
		$galedit = Galleries::where("_ID",$id)->first();
		$this->load->view('admin/gallery/editgallery',compact('galedit'));

	}

	 public function authentication_login(){
		$log_id=$this->encryption->decrypt($this->input->cookie('adminid'));
        if($log_id > 0){
			return $log_id;
        }
       return 0;
    }
}
?>  