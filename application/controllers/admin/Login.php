<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct(){

    	parent::__construct();

    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
    }

    public function index()
	{
		$pagename = "Login";
		$this->load->view('admin/login',compact('pagename'));
	}

	public function do_login(){
		$username = $this->input->post('username');
		$pass = $this->input->post('pass');
		$admin_details = Usermaster::where('_Username',$username)
		->where('_Password',md5($pass))->get()->toarray();
		
		if(count($admin_details)>0){
		set_cookie('adminname',$username,60*60*24*365*1);
		set_cookie('password',$this->encryption->encrypt($pass),60*60*24*365*1);
		set_cookie('adminid',$this->encryption->encrypt($admin_details[0]['_ID']),60*60*24*365*1);
		$res = ['type' => 'success' , 'msg' => 'You are login successfully.', 'url' => 'admin/dashboard', 'result' => true];
		}else{
		$res = ['type' => 'error' , 'msg' => 'Invalid email or password.', 'url' => 'admin', 'result' => true];
		}
		echo json_encode($res);
		exit;
		
	}

    public function logout()
	{
		delete_cookie("adminname");
		delete_cookie("password");
		delete_cookie("adminid");
		redirect('admin','refresh');
	}

	public function my_profile()
	{
		$id = $this->encryption->decrypt($this->input->cookie('adminid'));
		$profiledata = Usermaster::where("_ID",$id)->first()->toarray();
		$this->load->view('admin/profile',compact('profiledata'));
	}
	public function makeprofile()
	{
		$fname = $this->input->post('fname');
		$phid = $this->input->post('phid');
		$lname = $this->input->post('lname');
		$email = $this->input->post('email');
		$address = $this->input->post('address');
		$mobile = $this->input->post('mobile');
		$username = $this->input->post('username');
		$curpass = $this->input->post('curpass');
		$newpass = $this->input->post('newpass');
		$editudata = Usermaster::where("_ID",$phid)->first();
		$editudata->_Firstname = $fname;
		$editudata->_Lastname = $lname;
		$editudata->_Email = $email;
		$editudata->_Address = $address;
		$editudata->_Mobile = $mobile;
		$editudata->_Username = $username;
		$editudata->_Password = (($newpass!='')?md5($newpass):$curpass);
		$editudata->save();
		$res = ['type' => 'success' , 'msg' => 'Profile Updated successfully.', 'url' => 'my_profile', 'result' => true];
		
		echo json_encode($res);
		exit;

	}
    
}
?>