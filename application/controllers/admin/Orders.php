<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

    public function __construct(){

    	parent::__construct();

    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		if($this->authentication_login() === 0){
			return redirect('admin');
		}
    }

    public function index()
	{
		$data = Order::with('users')->get()->all();
		$this->load->view('admin/order/index',compact('data'));
	}

	public function authentication_login(){
		$log_id=$this->encryption->decrypt($this->input->cookie('adminid'));
        if($log_id > 0){
			return $log_id;
        }
       return 0;
    }

    public function vieworderdetails($id){
    	$data = Order::where('_ID',$id)->with('users','payment','product')->get()->all();
		$this->load->view('admin/order/vieworderdetails',compact('data'));
    }

    public function statuschange_order(){
        $id = $this->input->post('id');
        $order = $this->input->post('order');
        $payment = $this->input->post('payment');
    	
    	$upd = Order::find($id);
    	$upd->_Orderstatus = $order;
        $upd->_Paymentstatus = $payment;
    	$upd->save();

        $res = ['type' => 'success' , 'msg' => 'Order Status Updated Successfully..','url'=>'admin/orders', 'result' => true];
        echo json_encode($res);
        exit;

    }

    public function invoice($id){

        $admin_data = Stores::get()->first();
        $order_data = Order::with('product')->where('_ID',$id)->with('users')->get()->all();

        if($order_data[0]['_Invoiceid'] == ''){
            $order_lastid = Order::where('_Invoiceid','!=','')->orderBy('_Invoiceid','DESC')->first();
            if(!$order_lastid){
                $invoice_first = Order::find($id);
                $invoice_first->_Invoiceid = '#001000';
                $invoice_first->_Invoicedate =  date('Y-m-d H:i:s');
                $invoice_first->save();
            }
            else{
                $invid = $order_lastid['_Invoiceid'];
                $newinid = substr($invid,'3')+1;
                $invoice_first = Order::find($id);
                $invoice_first->_Invoiceid = '#00'.$newinid;
                $invoice_first->_Invoicedate =  date('Y-m-d H:i:s');
                $invoice_first->save();
            }
        }

        $this->load->view('admin/order/invoice',compact('admin_data','order_data'));
    }

   
}
?>   