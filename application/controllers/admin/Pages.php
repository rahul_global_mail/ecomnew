<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

    public function __construct(){

    	parent::__construct();

    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
    }

    public function index()
	{
		$pagename = "pages";
		$allpage  = Page::get()->toarray();
		$this->load->view('admin/pages/index',compact('pagename','allpage'));
	}

	public function createpages()
	{
		$editid = $this->uri->segment(2);
		if($editid) {
			$editpages = Page::where('_ID',$editid)->first();
		  	$pagetitle = "Edit Pages";
		  	$this->load->view('admin/pages/edit',compact('editid','editpages', 'pagetitle'));
		} else {
			$pagetitle = "Add Pages";
		  	$this->load->view('admin/pages/edit', compact('pagetitle', 'types','catinfo'));
		}
	}

	public function addpages()
	{
		$title = $this->input->post('title');
		$content = $this->input->post('content');
		$status = $this->input->post('status');
		$s 		= (($status=='on')?1:0);
		$tabpage = new Page();
		$tabpage->_Title = $title;
		$tabpage->_Content = $content;
		$tabpage->_Status = $s;
		$tabpage->_Created = date('Y-m-d H:i:s');
		$tabpage->save();
		$res = ['type' => 'success' , 'msg' => 'pages created successfully ','url'=>'admin/pages', 'result' => true];
        echo json_encode($res);
        exit;

	}


	public function delpages()
	{
		 if($this->input->post('id'))
        {
            $checkbox_value = $this->input->post('id');
            for($count = 0; $count < count($checkbox_value); $count++)
           {
           		$affectedRows = Page::where('_ID',$checkbox_value[$count])->first()->toArray();
               Page::find($affectedRows['_ID'])->delete();
           }
           if($affectedRows == true){
                        $res = ['type' => 'success' , 'msg' => 'Pages deleted successfully ','url'=>'admin/pages', 'result' => true];
                    }else{
                        $res = ['type' => 'error' , 'msg' => 'Pages not deleted/ ','url'=>'admin/pages', 'result' => true];
                    }
                    echo json_encode($res);
                        exit;
        }
	
	}

	public function updatepages()
	{
		$hpid = $this->input->post('hpid');
		$title = $this->input->post('title');
		$content = $this->input->post('content');
		$status = $this->input->post('status');
		$s 		= (($status=='on')?1:0);
		$edittabpage = Page::find($hpid);
		$edittabpage->_Title = $title;
		$edittabpage->_Content = $content;
		$edittabpage->_Status = $s;
		$edittabpage->_Created = date('Y-m-d H:i:s');
		$edittabpage->save();
		$res = ['type' => 'success' , 'msg' => 'pages updated successfully ','url'=>'admin/pages', 'result' => true];
        echo json_encode($res);
        exit;

	}

	
    
}
?>