<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PHPImageWorkshop\ImageWorkshop;

class Product extends CI_Controller {

    public function __construct(){

    	parent::__construct();

    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		if($this->authentication_login() === 0){
			return redirect('admin');
		}

    }
 
    public function index()
	{

		$catresult= Products::with('categories','subcategory')
							->get()
							->toArray();
		$this->load->view('admin/product/index',compact('catresult'));
	}


	public function createproduct()
	{
		$editid = $this->uri->segment(2);
		$catdata = Categories::where('_Main_id',0)->get()->toarray();
		if($editid) {
		  	$product =  Products::where("_ID",$editid)->first();
		  	$proatt  = Products::get()->toarray();
		  	$attribute  = Attributes::get()->toarray();
		  	$attdetail  = Attdetail::with('Attributes')->where("_ProID",$editid)->get()->toarray();
		  	$attinfo = $attdetail[0]['_Attinfo'];

		  	$pagetitle = "Edit Product";
		  	$this->load->view('admin/product/edit',compact('editid','product', 'pagetitle','catdata','proatt','attribute','attdetail','attinfo'));
		} else {
			$pagetitle = "Add Product";
		  	$this->load->view('admin/product/edit', compact('pagetitle','catdata'));

		  }
	

		
	}
	
	public function addproduct(){
		$catdata = Categories::where('_Main_id',0)
								->get()
								->toarray();
		$this->load->view('admin/product/addproduct',compact('catdata'));
	}

	public function editproduct($id){
		$catdata = Categories::where('_Main_id',0)
							->get()
							->toarray();

		$product_data = Products::where('_ID',$id)
							->first()
							->toArray();

		$attr_detail = Attdetail::where('_ProID',$product_data['_ID'])
								->get()
								->toArray();

		$this->load->view('admin/product/editproduct',compact('product_data','catdata','attr_detail'));
	}

	public function addproduct_action()
	{
		$name   = $this->input->post('name');
		$desc   = $this->input->post('description');
		$cat    = $this->input->post('category');
		$subc   = $this->input->post('subcategory');
		$spe    = $this->input->post('special');
		$status = $this->input->post('status');
		$s 		= ($status=='on') ? 1 : 0;

		$prodata = new Products();
		$prodata->_Name     = $name;
		$prodata->_Des      = $desc;
		$prodata->_CatID    = $cat;
		$prodata->_SubcatID = $subc;
		$prodata->_Special  = $spe;
		$prodata->_Status   = $s;
		$prodata->_Created = date("Y-m-d H:i:s");
		$prodata->save();

		$size      =  $this->input->post('size');
		$color     =  $this->input->post('color');
		$quantity  = $this->input->post('quantity');
		$price     = $this->input->post('price');
		$sellprice = $this->input->post('sellprice');
		$default = $this->input->post('default');

		foreach ($size as $key => $value) {

			$attdetail = new Attdetail();
			$attdetail->_ProID = $prodata->_ID;
			$attdetail->_Size = $value;
			$attdetail->_Color = $color[$key];
			$attdetail->_Quantity = $quantity[$key];
			$attdetail->_Price = $price[$key];
			$attdetail->_Sellprice = $sellprice[$key];
			$attdetail->_Default = $default[$key];
			$attdetail->_Created = date("Y-m-d H:i:s");
			$attdetail->save();
		}
		
		$res = ['type' => 'success' , 'msg' => 'Product Created successfully ','url'=>'admin/product', 'result' => true];
    	echo json_encode($res);
    	exit;
	}

	public function editproduct_action(){

		$id     = $this->input->post('pid');
		$name   = $this->input->post('name');
		$desc   = $this->input->post('description');
		$cat    = $this->input->post('category');
		$subc   = $this->input->post('subcategory');
		$spe    = $this->input->post('special');
		$status = $this->input->post('status');
		$s 		= (($status=='on')?1:0);

		$updpdata = Products::find($id);
		$updpdata->_Name     = $name;
		$updpdata->_Des      = $desc;
		$updpdata->_CatID    = $cat;
		$updpdata->_SubcatID = $subc;
		$updpdata->_Special  = $spe;
		$updpdata->_Status   = $s;
		$updpdata->save();

		$atts = Attdetail::where('_ProID', $id)->delete();

		$size      =  $this->input->post('size');
		$color     =  $this->input->post('color');
		$quantity  = $this->input->post('quantity');
		$price     = $this->input->post('price');
		$sellprice = $this->input->post('sellprice');
		$default = $this->input->post('default');
	
		foreach ($size as $key => $value) {

			$attdetail = new Attdetail();
			$attdetail->_ProID = $id;
			$attdetail->_Size = $value;
			$attdetail->_Color = $color[$key];
			$attdetail->_Quantity = $quantity[$key];
			$attdetail->_Price = $price[$key];
			$attdetail->_Sellprice = $sellprice[$key];
			$attdetail->_Default = $default[$key];
			$attdetail->_Created = date("Y-m-d H:i:s");
			$attdetail->save();
		}

		$res = ['type' => 'success' , 'msg' => 'Product Updated successfully ','url'=>'admin/product', 'result' => true];
    	echo json_encode($res);
    	exit;

	}

	public function getsubcategory()
	{
		$catid = $this->input->post('id');
		$catdata = Categories::where('_Main_id',$catid)->get()->toarray();
		echo json_encode($catdata);
	}


	public function delproduct()
	{
		if($this->input->post('id'))
  		{
	    	$checkbox_value = $this->input->post('id');
		    for($count = 0; $count < count($checkbox_value); $count++)
		   {
				$affectedRows = Products::where('_ID',$checkbox_value[$count])->first()->toArray();
				Products::find($affectedRows['_ID'])->delete();
				Attdetail::where('_ProID',$affectedRows['_ID'])->delete();
				
				foreach ($affectedRows['_Image'] as $key => $value) {
		        	if(file_exists('assets/uploads/product/'.$value['name'])){
				        unlink('assets/uploads/product/'.$value['name']);
				        unlink('assets/uploads/product/'.$value['thumb']);
						unlink('assets/uploads/product/'.$value['medium']);
						unlink('assets/uploads/product/'.$value['large']);
						unlink('assets/uploads/product/'.$value['cat']);
					}
		}
		   }
		   if($affectedRows == true){
				$res = ['type' => 'success' , 'msg' => 'Product deleted successfully ','url'=>'admin/product', 'result' => true];
			}else{
				$res = ['type' => 'error' , 'msg' => 'Product not deleted/ ','url'=>'admin/product', 'result' => true];
			}
			echo json_encode($res);
	        	exit;
  		}

	}

	public function getattributeval(){
		$proid = $this->input->post('id');
		$attdata = Attributes::where('_ID',$proid)->get()->toarray();
		echo json_encode($attdata);
	}

	public function product_adddetail($id){
		$data = Products::where('_ID',$id)		
						->get()
						->toArray();

		$this->load->view('admin/product/product_adddetail',compact('data'));
	}

	public function addprodetail_action(){
		$id = $this->input->post('pid');

		$new_key = array_filter($this->input->post('key'));
		$new_val = array_filter($this->input->post('value'));

		$data = ["key" => $new_key,"value" => $new_val];

		$upd_data = Products::find($id);
		$upd_data->_Adddetails = $data;
		$upd_data->save();

		$res = ['type' => 'success' , 'msg' => 'Product Additional Details Added successfully ','url'=>'editproduct/'.$id.'', 'result' => true];
		echo json_encode($res);
        exit;

	}

	public function gallery($id){
		$data = Products::where('_ID',$id)
						->first()
						->toArray();
		
		$this->load->view('admin/product/gallery',compact('data'));
	}

	public function uploadImage() {

        $files = $_FILES;
        $pro_id = $this->input->post('product_id');
        $res = [];
		
		for($i=0; $i< count($_FILES['proImage']['name']); $i++) {
        	$_FILES['proimage']['name']= $files['proImage']['name'][$i];
        	$_FILES['proimage']['type']= $files['proImage']['type'][$i];
        	$_FILES['proimage']['tmp_name']= $files['proImage']['tmp_name'][$i];
        	$_FILES['proimage']['error']= $files['proImage']['error'][$i];
        	$_FILES['proimage']['size']= $files['proImage']['size'][$i];

            $config['upload_path']          = 'assets/uploads/product/';
        	//$config['allowed_types']        = 'jpg|gif|png|jpeg|JPG|PNG|webp';
        	$config['allowed_types']        = '*';
        	$config['max_size']             = 10240;
        	$config['file_ext_tolower'] = TRUE;

        	$qwe = pathinfo($_FILES['proimage']['name']);
        	$microtime = microtime(true);
       	    $m = str_replace('.','_', $microtime);
			$file_name = $m.'_PRO_'.$this->input->post('product_id');
			$filepath = $config['upload_path'] . $file_name .".". $qwe['extension'];

        	$config['file_name']             = $file_name.".". $qwe['extension'];
        	$this->upload->initialize($config);
            if ( ! $this->upload->do_upload('proimage')) {
                    $error = array('error' => $this->upload->display_errors());
            } else {
                $data = array('upload_data' => $this->upload->data());
            	$pinguLayer = ImageWorkshop::initFromPath($config['upload_path'].$file_name.'.'.$qwe['extension']);

                $pinguLayer->resizeInPixel(772, 960, true);
                //$pinguLayer->resizeByLargestSideInPercent($image_size, true);
                $pinguLayer1 = ImageWorkshop::initFromPath($config['upload_path'].$file_name.'.'.$qwe['extension']);
                $pinguLayer1->resizeInPixel(194, 240, true);

                $pinguLayer2 = ImageWorkshop::initFromPath($config['upload_path'].$file_name.'.'.$qwe['extension']);
                $pinguLayer2->resizeInPixel(240, 280, true);

                $pinguLayer3 = ImageWorkshop::initFromPath($config['upload_path'].$file_name.'.'.$qwe['extension']);
                $pinguLayer3->resizeInPixel(386, 480, true);

                $dirPath = $config['upload_path'];
                $filename = $file_name."_large.".$qwe['extension'];
                $filename1 = $file_name."_thumb.".$qwe['extension'];
                $filename2 = $file_name."_med.".$qwe['extension'];
                $filename3 = $file_name."_cat.".$qwe['extension'];
                $createFolders = true;  
                $backgroundColor = null;
                $imageQuality = 95;
                
                $pinguLayer->save($dirPath, $filename, $createFolders, $backgroundColor, $imageQuality);
                $pinguLayer1->save($dirPath, $filename1, $createFolders, $backgroundColor, $imageQuality);
                $pinguLayer2->save($dirPath, $filename2, $createFolders, $backgroundColor, $imageQuality);
                $pinguLayer3->save($dirPath, $filename3, $createFolders, $backgroundColor, $imageQuality);

                $imageData['name'] = $file_name.".". $qwe['extension'];
                $imageData['thumb'] = $filename1;
                $imageData['medium'] = $filename2;
                $imageData['cat'] = $filename3;
                $imageData['large'] = $filename;
                $imageData['size'] = $_FILES['proimage']['size'];
                array_push($res, $imageData);
            }
		}

    	$product = Products::find($pro_id);

    	$exist_gal = $product['_Image'];
    	if($exist_gal){
	        $final_gal = array_merge($exist_gal, $res);
    	}else{
	        $final_gal = $res;
		}

    	$product->_Image = $final_gal;
    	$product->save();
    	$data = [
	    	"msg" => "Image upload successfully",
	    	"type" => "success"
    	];
    	header('Content-type: application/json');
    	echo json_encode($data);
    	exit();
	}

	public function deleteImage() {
		$target_dir = "assets/uploads/product/";
		$pro_id = $this->input->post('product_id');
		$mainname = $this->input->post('mainname');

		$product = Products::find($pro_id);
	    	$exist_gal = $product['_Image'];
	    	
	    	$new_arr = [];
	    	foreach ($exist_gal as $key => $value) {
		        if($value['name'] == $mainname){

		        	if(file_exists($target_dir.$value['name'])){
				        unlink($target_dir.$value['name']);
			    	}
			    	if(file_exists($target_dir.$value['thumb'])){
				        unlink($target_dir.$value['thumb']);
			    	}
			    	if(file_exists($target_dir.$value['medium'])){
				        unlink($target_dir.$value['medium']);
			    	}
			    	if(file_exists($target_dir.$value['large'])){
				        unlink($target_dir.$value['large']);
			    	}
			    	if(file_exists($target_dir.$value['cat'])){
				        unlink($target_dir.$value['cat']);
			    	}
		            	unset($key);
		        }else{
		            $new_arr[] = $value;
		        }
	    	}

	    	$product->_Image = $new_arr;
	    	$product->save();

	    	$data = [
		    	"msg" => "Image delete successfully",
		    	"type" => "success"
	    	];
	    	header('Content-type: application/json');
	    	echo json_encode($data);exit();
	}

	public function addspeci(){
		$id =  $this->input->post('hpid');
		$data = ["key" => $this->input->post('property'),"value" => $this->input->post('value')];
		$prodetail = Products::where('_ID',$id)->first();
	
			    $prodetail->_Specification = $data;
				$prodetail->save();
			
				$res = ['type' => 'success' , 'msg' => 'Product Specification Created successfully ','url'=>'editproduct/'.$id.'', 'result' => true];
				echo json_encode($res);
			    	exit;
			
	}

	public function productgallery(){
 if ($this->input->post('upload_img')) {
            $path = 'assets/uploads/product';
            // Define file rules
            $initi = $this->upload->initialize(array(
                "upload_path" => $path,
                "allowed_types" => "gif|jpg|jpeg|png",
                "remove_spaces" => TRUE
            ));
            $imagename = 'no-img.jpg';
            if (!$this->upload->do_upload('imageURL')) {
                $error = array('error' => $this->upload->display_errors());
                echo $this->upload->display_errors();
            } else {
                $data = $this->upload->data();
                $imagename = $data['file_name'];
            }            
            // create Thumbnail -- IMAGE_SIZES;
            $image_sizes = array('_mobile'=>array(75,75), '_thumb' => array(193, 240),'_medium' => array(240, 280), '_large' => array(772, 960));
            // load library
            $this->load->library('image_lib');
            foreach ($image_sizes as $key=>$resize) {
                $config = array(
                    'source_image' => $data['full_path'],


                    
                    'new_image' => 'assets/uploads/product' .'/'.$key,
                    'maintain_ratio' => FALSE,
                    'width' => $resize[0],
                    'height' => $resize[1],
                    'quality' =>70,
                );
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $this->image_lib->clear();
            }            
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $this->upl->setURL($imagename);            
            $this->upl->create();                           
          /*  $this->session->set_flashdata('img_uploaded_msg', '<div class="alert alert-success">Image uploaded successfully!</div>');
            $this->session->set_flashdata('img_uploaded', $imagename);
            redirect('/');*/
        }

	}
		
	


	public function authentication_login(){
		$log_id=$this->encryption->decrypt($this->input->cookie('adminid'));
        if($log_id > 0){
			return $log_id;
        }
       return 0;
    }
}
?>   