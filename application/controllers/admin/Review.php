<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Review extends CI_Controller {

    public function __construct(){

    	parent::__construct();

    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		if($this->authentication_login() === 0){
			return redirect('admin');
		}

    }


    public function index()
	{
		//->groupBy('_ProductID');
		$reviewdata = Reviews::with('Products')->get()->toarray();
	    $url = $this->uri->segment(2);
		$this->load->view('admin/review/index',compact('reviewdata','url'));
	}


	public function showreview($id)
	{
		$reviewdata = Reviews::where('_ProductID',$id)->get()->toarray();

		$url = $this->uri->segment(2);
		/*$reviewdata = Reviews::with('Products','Users')->groupBy('_ProductID')->get()->toarray();*/
		$this->load->view('admin/review/index',compact('reviewdata','url'));

	}

	public function viewreview($id)
	{
		$reviewdetail = Reviews::with('Products','Users')->where('_ID',$id)->first()->toarray();
		$this->load->view('admin/review/showreview',compact('reviewdetail'));
	}

	public function approvereview($id)
	{
		$reapp = Reviews::where('_ID',$id)->first();
		$reapp->_Status = '1';
		$reapp->save();
		$proid = $reapp->_ProductID;
		redirect(base_url("showreview/$proid"));

		
	}
	public function proapprovereview($id)
	{
		$reapp = Reviews::where('_ID',$id)->first();
		$reapp->_Status = '1';
		$reapp->save();
		$proid = $reapp->_ProductID;
		redirect(base_url("admin/review"));

	}
	public function rejectview($id)
	{
		$re = Reviews::where('_ID',$id)->first();
		$re->_Status = '0';
		$re->save();
		$proid = $re->_ProductID;
		redirect(base_url("showreview/$proid"));
	}

	public function prorejectreview($id)
	{
		$re = Reviews::where('_ID',$id)->first();
		$re->_Status = '0';
		$re->save();
		$proid = $re->_ProductID;
		redirect(base_url("admin/review"));
	}

	 public function authentication_login(){
		$log_id=$this->encryption->decrypt($this->input->cookie('adminid'));
        if($log_id > 0){
			return $log_id;
        }
       return 0;
    }
}
?>   