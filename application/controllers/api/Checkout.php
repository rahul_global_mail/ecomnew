<?php

use chriskacerguis\RestServer\RestController;

class Checkout extends RestController {

    public function __construct() {
       	parent::__construct();
    }

    /*********Get Checkout Data *************/
    public function getCheckout_get($loguser_id = ''){
        
        $cart_data = Carts1::with('product','user')
                            ->where('_UserID',$loguser_id)
                            ->get()
                            ->toArray();

        $addetail = Stores::where('_ID',1)
                            ->first()
                            ->toarray();
        
        $now = date('Y-m-d');
        $coupon_data = Couponcode::whereDate('_Fromdate','<=',$now)
                                ->whereDate('_Todate','>=',$now)
                                ->get()
                                ->toArray();
        
        $j = 0;
        $yes_cat = 0;
        $dis_price = 0;
        $other_price = 0;
        $final_dis_price = 0;
        $discount_offer = 0;
        $total_price = 0;

        for($i = 0;$i < count($cart_data) ; $i++){
            $total_price +=  $cart_data[$i]['_Subtotal'];
        }

        for($i = 0;$i < count($cart_data) ; $i++){
        
            if(in_array($cart_data[$i]['product']['_SubcatID'],$coupon_data[$j]['_Subcatid'])){
                $yes_cat = $yes_cat+1;
            }
        }
       
       if($yes_cat == count($coupon_data[$j]['_Subcatid'])){
           for($i = 0;$i < count($cart_data) ; $i++){
                
                if(in_array($cart_data[$i]['product']['_SubcatID'],$coupon_data[$j]['_Subcatid'])){
                    $dis_price += $cart_data[$i]['_Unitprice'];
                    $old_q = $cart_data[$i]['_Quantity'] - 1;
                    if($old_q != '0'){
                        $other_price += $cart_data[$i]['_Unitprice'] * $old_q;
                    }
                }
                else{
                    $other_price += $cart_data[$i]['_Subtotal'];
                }
            }
        }

        if($yes_cat == count($coupon_data[$j]['_Subcatid'])){
            $discount_offer  = 1;
            $final_dis_price = $dis_price - $coupon_data[0]['_Amount'];
        }
        else{
            for($i = 0;$i < count($cart_data) ; $i++){
                $other_price += $cart_data[$i]['_Subtotal'];
            }
        }

        $sub_total1 = 0;
        for($i = 0;$i < count($cart_data) ; $i++){
            $sub_total1 += $cart_data[$i]['_Subtotal'];
        }

        $final_price = $total_price - $final_dis_price;

        $addetail = Stores::where('_ID',1)
                            ->first()
                            ->toarray();

        $allpage  = Page::get()->toarray();
        $coupon_data = Couponcode::get()->toArray();

        $order_check = Order::orderBy('_ID', 'desc')
                            ->first();

        $order_data = Order::where('_Userid',$loguser_id)
                    ->where('_Orderstatus','0')
                    ->first();

        if($order_data != ''){
            $order_id = $order_data->_Orderid;
        }
        else if($order_check != ''){
            $order_id = $order_check->_Orderid + 1;
        }
        else{
            $order_id = '1001';   
        }

        if($discount_offer == '1'){
            $disc = $coupon_data[0]['_Amount'];
        }
        else{
            $disc = 0;
        }

        if($order_data != ''){
            $order_data->_Orderid       = $order_id;
            $order_data->_Userid        = $loguser_id;
            $order_data->_Couponid      = $coupon_data[$j]['_ID'];
            $order_data->_Subtotal      = $sub_total1;
            $order_data->_Getwayid      = '0';
            $order_data->_Paymentstatus = '0';
            $order_data->_Orderstatus   = '0';
            $order_data->_Grandtotal    = $final_price;
            $order_data->_Discount      = $disc;
            $order_data->_Created       = date("Y-m-d H:i:s");
            $order_data->save();
        }
        else{
            $ins_order                  = new Order();
            $ins_order->_Orderid        = $order_id;
            $ins_order->_Userid         = $loguser_id;
            $ins_order->_Couponid      = $coupon_data[$j]['_ID'];
            $ins_order->_Subtotal       = $sub_total1;
            $ins_order->_Getwayid       = '0';
            $ins_order->_Paymentstatus  = '0';
            $ins_order->_Orderstatus    = '0';
            $ins_order->_Grandtotal     = $final_price;
            $ins_order->_Discount       = $disc;
            $ins_order->_Created        = date("Y-m-d H:i:s");
            $ins_order->save();
        }

        $new_order_data = Order::with('coupon')
                                ->where('_Userid',$loguser_id)
                                ->where('_Orderstatus','0')
                                ->get()
                                ->toArray();

        $res = [];
        $new_res = [];
        $new_res['cart_data'] = $cart_data;
        $new_res['addetail'] = $addetail;
        $new_res['loguser_id'] = $loguser_id;
        $new_res['coupon_data'] = $coupon_data;
        $new_res['allpage'] = $allpage;
        $new_res['new_order_data'] = $new_order_data;
        array_push($res, $new_res);

        $this->response($res, RestController::HTTP_OK);
    }

}