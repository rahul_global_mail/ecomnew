<?php

use chriskacerguis\RestServer\RestController;

class Product extends RestController {

	/**
     * Get All Data from this method.
     *
     * @return Response
    */

    public function __construct() {
       	parent::__construct();
    }

    /**
     * Get All Data from this method.
     *
     * @return Response
    */

    /********All Categories get ***************/
    public function category_get(){
        $data = Categories::where('_Main_id','1')
                        ->get()
                        ->all();
        $this->response($data, RestController::HTTP_OK);
    }

    /*******All products get *******************/
    public function product_get(){

        $data = Attdetail::default()
                        ->with(['product','product.subcategory:_ID,_Name'])
                        ->orderBy('_ID', 'DESC')
                        ->take(2)
                        ->get()
                        ->toArray();

        $res = [];
        foreach ($data as $key => $value) {
            $pro_data = [];
            $pro_data['proid']       = (string) $value['_ProID'];
            $pro_data['name']        = $value['product']['_Name'];
            $pro_data['description'] = $value['product']['_Des'];
            $pro_data['price']       = (integer) $value['_Price'];
            $pro_data['sellprice']   = $value['_Sellprice'];
            $pro_data['special']     = $value['product']['_Special'];
            $pro_data['size']        = $value['_Size'];
            $pro_data['color']       = $value['_Color'];
            $pro_data['category']    = $value['product']['subcategory']['_Name'];
            $pro_data['image']       = $value['product']['_Image'][0]['thumb'];
            $pro_data['images']      = $value['product']['_Image'];
            $pro_data['quantity']    = (integer) $value['_Quantity'];

            array_push($res, $pro_data);
        }
        $this->response($res, RestController::HTTP_OK);
    }

    /***********Product detail fetch using product id *******/
    public function productDetail_post(){
        $res        = [];
        $post       = $this->request->body;
        $loguser_id = $post['userid'];
        $productid  = $post['productid'];

        $res['productid'] = $productid;

        $res['product'] = Products::with('attributes','reviews')
                            ->where('_ID',$productid)
                            ->first()
                            ->toArray();

        $res['cart_data'] = Carts1::where('_ProductID',$productid)
                            ->where('_UserID',$loguser_id)
                            ->get()
                            ->toArray();

        $review_pro_data = Reviews::where('_ProductID',$productid)
                            ->get()
                            ->toArray();
        $res['review_pro_data'] = $review_pro_data;

        if($loguser_id) {
            $res['allreview'] = Reviews::with('users')
                                ->where('_ProductID',$productid)
                                ->where('_Status','1')
                                ->get()
                                ->toarray();

            $res['reviews']  = Reviews::with('users')
                                ->where('_ProductID',$productid)
                                ->get()
                                ->toarray();
        }else{
            $res['allreview'] = [];
            $res['reviews']   = [];
        }

        $res['default_price'] = Attdetail::where('_ProID',$res['product']['_ID'])
                                    ->where('_Default','1')
                                    ->get()
                                    ->toArray();

        $res['addetail'] = Stores::where('_ID',1)->first()->toarray();
        $res['allpage']  = Page::get()->toarray();

        $res['wishlist'] = Wishlists::where('_Userid',$loguser_id)
                            ->where('_Productid',$productid)
                            ->get()
                            ->toArray();

        $review_old_count = 0;

        for($i = 0 ; $i < count($review_pro_data); $i++)
        {
            $review_old_count += $review_pro_data[$i]['_Rate'];
        }
        $review_count = 0;

        if(count($review_pro_data) > 0){
            $review_count = round($review_old_count / count($review_pro_data));
        }

        $res['review_count'] = $review_count;
        $this->response($res, RestController::HTTP_OK);
    }

    /************Product Serach ***********************/
    public function searchProduct_post(){
        $res    = [];
        $post   = $this->request->body;
        $log_id = $post['userid'];
        $search = $post['search'];

        $res['cat_data'] = Categories::where('_Main_id','0')
                                ->get()
                                ->all();

        $res['category'] = Categories::get()->toArray();

        $cat_search = Categories::where('_Name',$search)
                                ->get()
                                ->toArray();

        if(count($cat_search) > 0){
            $sid = $cat_search[0]['_ID'];
            $search = "'".$search."'";
            $res['products'] = Attdetail::default()
                                ->with(['product' => function($query) use ($search, $sid) {
                                    $query->where('_SubcatID',$sid);
                                    $query->orWhere('_Name', $search);
                                },'product.wishlists' => function ($query) use ($log_id){
                                  if($log_id > 0){
                                    $query->where('_Userid', $log_id);
                                  }
                                }])
                                ->orderBy('_ID', 'DESC')
                                ->get()
                                ->toArray();
        }
        else{
            $res['products'] = Attdetail::default()
                        ->with(['product' => function($query) use ($search) {
                            $query->where('_Name','like','%'.$search.'%');
                        }
                        ,'product.wishlists' => function ($query) use ($log_id){
                          if($log_id > 0){
                            $query->where('_Userid', $log_id);
                          }
                        }])
                        ->orderBy('_ID', 'DESC')
                        ->get()
                        ->toArray();
        }
        
        $res['addetail'] = Stores::where('_ID',1)
                                ->first()
                                ->toarray();
        $res['allpage']  = Page::get()->toarray();
        $this->response($res, RestController::HTTP_OK);
    }

    public function filterProduct_post(){

        $res      = [];
        $post     = $this->input->post();
        $category = isset($post['category']) ? $post['category'] : '';
        $size     = isset($post['size']) ? $post['size'] : '';
        $color    = isset($post['color']) ? $post['color'] : '';
        $minprice = isset($post['minprice']) ? $post['minprice'] : '0';
        $maxprice = isset($post['maxprice']) ? $post['maxprice'] : '10000';

        $query = Attdetail::/*with(['product' => function($query) use ($category) {
                    if($category){
                        for($i = 0 ;$i < count($category); $i++ ){
                            if($i == 0){
                                $query->where('_SubcatID',$category[$i]);
                            }
                            else{
                                $query->orWhere('_SubcatID',$category[$i]);   
                            }
                        }
                    }
                    }])->*/where(function($query) use ($size){
                        if($size){
                            for($j = 0 ;$j < count($size); $j++ ){
                                if($j == 0){
                                   $query->where('_Siz e', $search = "'".$size[$j]."'");
                                }
                                else{
                                    $query->orWhere('_Size', $search = "'".$size[$j]."'");
                                }
                            }
                        }
                    })->where(function($query) use ($color){
                        if($color){
                            for($k = 0 ;$k < count($color); $k++ ){
                                if($k == 0){
                                   $query->where('_Color', $search = "'".$color[$k]."'");
                                }
                                else{
                                    $query->orWhere('_Col or', $search = "'".$color[$k]."'");
                                }
                            }
                        }
                    })->where(function($query) use ($minprice,$maxprice){
                        if($minprice != '' || $maxprice != ''){
                            $query->whereBetween('_Pric e', ["'".$minprice."'", "'".$maxprice."'"]);
                        }
                    })->get()->toArray();

        echo '<pre>';
        print_r($query);exit;
         
    }

}