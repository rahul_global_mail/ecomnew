<?php

use chriskacerguis\RestServer\RestController;

class Review extends RestController {

    public function __construct() {
       	parent::__construct();
    }

    /***********Review Form insert****************/
    public function addReview_post(){
        $res       = [];
        $post      = $this->request->body;
        $userid    = $post['userid'];
        $title     = $post['title'];
        $review    = $post['review'];
        $star      = $post['star'];
        $productid = $post['productid'];

        $check_review = Reviews::where('_Productid',$productid)
                                ->where('_Userid',$userid)
                                ->get()
                                ->toArray();

        if($check_review){
            $upd_review             = Reviews::find($check_review[0]['_ID']);
            $upd_review->_Title     = $title;
            $upd_review->_Review    = $review;
            $upd_review->_Rate      = $star;
            $upd_review->_ProductID = $productid;
            $upd_review->_UserID    = $userid;
            $upd_review->_Status    = '0';
            $upd_review->save();
        }
        else{
            $ins_review             = new Reviews();
            $ins_review->_Title     = $title;
            $ins_review->_Review    = $review;
            $ins_review->_Rate      = $star;
            $ins_review->_ProductID = $productid;
            $ins_review->_UserID    = $userid;
            $ins_review->_Status    = '0';
            $ins_review->_Created   = date("Y-m-d H:i:s");
            $ins_review->save();
        }

        $res = ['type' => 'success' , 
                'msg' => 'Review Added successfully ',
                'result' => true];
        $this->response($res, RestController::HTTP_OK);
    }
}