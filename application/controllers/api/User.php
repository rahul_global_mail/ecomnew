<?php

use chriskacerguis\RestServer\RestController;

class User extends RestController {



	  /**

     * Get All Data from this method.

     *

     * @return Response

    */


    public function __construct() {
       	parent::__construct();
    }

    /**
     * Get All Data from this method.
     *
     * @return Response
    */

	public function myprofile_get($id = 0){
        if(!empty($id)){
            $data = Users::where('_ID',$id)
            		->first()->toarray();

        }else{
            $data = [];
        }
        $this->response($data, RestController::HTTP_OK);
	}


	public function myorder_get($id = 0){
        if(!empty($id)){
            $data = Users::with('order', 'order.product')
            		->where('_ID',$id)
            		->first()->toarray();
        }else{
            $data = Users::with('Cities')->get()->toarray();

        }
        $this->response($data, RestController::HTTP_OK);
    }

    public function myaddress_get($id = 0){
    	if(!empty($id)){
            $data = Users::with('address')
            		->where('_ID',$id)
            		->first()->toarray();
        }else{
            $data = Users::with('Cities')->get()->toarray();
        }
        $this->response($data, RestController::HTTP_OK);
    }

    public function category_get(){
        $data = Categories::where('_Main_id','1')->get()->all();
        $this->response($data, RestController::HTTP_OK);
    }

    public function product_get(){
        $data = Products::with('attributes:_ProID,_Attinfo', 'subcategory:_ID,_Name')
                        ->orderBy('_ID', 'ASC')
                        ->take(2)
                        ->get()
                        ->toarray();
        $res = [];
        foreach ($data as $key => $value) {
            $pro_data = [];
            $pro_data['id'] = (string) $value['_ID'];
            $pro_data['name'] = $value['_Name'];
            $pro_data['description'] = $value['_Des'];
            $pro_data['price'] = (integer) $value['_Price'];
            $pro_data['sellprice'] = $value['_Sellprice'];
            $pro_data['special'] = $value['_Special'];
            $pro_data['sellprice'] = $value['_Sellprice'];
            $pro_data['category'] = $value['subcategory']['_Name'];
            $pro_data['image'] = $value['_Image'][0]['thumb'];
            $pro_data['quantity'] = (integer) $value['attributes'][0]['_Attinfo'][0]['quantity'];

            array_push($res, $pro_data);
        }
        $this->response($res, RestController::HTTP_OK);
    }

    public function cart_get(){
        $data = Products::with('attributes:_ProID,_Attinfo', 'subcategory:_ID,_Name')
                        ->orderBy('_ID', 'ASC')
                        ->take(2)
                        ->get()
                        ->toarray();
        $res = [];
        foreach ($data as $key => $value) {
            $pro_data = [];
            $pro_data['id'] = (string) $value['_ID'];
            $pro_data['name'] = $value['_Name'];
            $pro_data['description'] = $value['_Des'];
            $pro_data['price'] = (integer) $value['_Price'];
            $pro_data['sellprice'] = $value['_Sellprice'];
            $pro_data['special'] = $value['_Special'];
            $pro_data['sellprice'] = $value['_Sellprice'];
            $pro_data['category'] = $value['subcategory']['_Name'];
            $pro_data['image'] = $value['_Image'][0]['thumb'];
            $pro_data['quantity'] = (integer) $value['attributes'][0]['_Attinfo'][0]['quantity'];

            array_push($res, $pro_data);
        }
        $this->response($res, RestController::HTTP_OK);
    }


    /********Address Details get using Login Id*********/
    public function myaddress_get($id = 0){
    	if(!empty($id)){
            $data = Users::with('address')
                		->where('_ID',$id)
                		->first()
                        ->toarray();
        }else{
            $data = Users::with('Cities')
                        ->get()
                        ->toarray();
        }
        $this->response($data, RestController::HTTP_OK);
    }
  

    public function deal_get(){
        $data = Extrafield::get()
                        ->toArray();
        $this->response($data, RestController::HTTP_OK);
    }


    /**
     * Get All Data from this method.
     *
     * @return Response
    */

    public function userLogin_post()
    {
    	$post = $this->request->body;
        $email = $post['email'];
        $password = $post['password'];

		$check_data = Users::where('_Email',$email)
							->where('_Password',md5($password))
							->first();
		if($check_data){

			$res = ['type' => 'success' , 'msg' => 'You are login successfully.', 'data' => $check_data];
		}else{
			$res = ['type' => 'error' , 'msg' => 'Invalid email or password.', 'data' => []];

		}
		$this->response($res , RestController::HTTP_OK);
    }

    public function index_post()
    {
        $input = $this->input->post();
        $this->db->insert('items',$input);
        $this->response(['Item created successfully.'], RestController::HTTP_OK);
    }

    public function addtocart_post()
    {
        $post = $this->request->body;
        $userid = $post['userid'];
        $product_id = $post['product_id'];
        
        
        $this->response($product, RestController::HTTP_OK);
    } 

     

    /**

     * Get All Data from this method.

     *

     * @return Response

    */

    public function index_put($id) {
        $input = $this->put();
        $this->db->update('items', $input, array('id'=>$id));    
        $this->response(['Item updated successfully.'], RestController::HTTP_OK);
    }

     

    /**

     * Get All Data from this method.

     *

     * @return Response

    */

    public function index_delete($id)
    {
        $this->db->delete('items', array('id'=>$id));
        $this->response(['Item deleted successfully.'], RestController::HTTP_OK);
    }

    /*********Register Functionality***********/
    public function userRegister_post()
    {
        $post       = $this->request->body;
        $fname      = $post['fname'];
        $lname      = $post['lname'];
        $mno        = $post['mno'];
        $email      = $post['email'];
        $password   = $post['password'];

        $test = Users::where('_Email','=',$email)
                    ->get();
        
        if(count($test)<=0){
            $ins = new Users();
            $ins->_Firstname = $fname;
            $ins->_Lastname  = $lname;
            $ins->_Mobile    = $mno;
            $ins->_Email     = $email;
            $ins->_Password  = $password;
            $ins->_Created   = date('Y-m-d H:i:s');
            $ins->save();

            /*$tempdata = Templates::where('_Slug','register')->first()->toarray();
            $subject  = $tempdata['_Subject'];
            $body     = $tempdata['_Content'];
            $toEmail  =$tempdata['_Mail_to'];
            $body     = str_replace("%user%", $fname.' '.$lname, $body);
            $toEmail  = str_replace("%mail%", $email, $toEmail);
            $body     = str_replace("%url%", siteurl, $body);
            $body     = str_replace("%sitename%", sitename, $body);
            $sitename = sitename;

            $config = array();
            $config['useragent']           = 'CodeIgniter';
            $config['mailpath']            = '/usr/bin/sendmail'; // or "/usr/sbin/sendmail"
            $config['protocol']            = 'mail';
            $config['smtp_host']           = 'localhost';
            $config['smtp_port']           = '25';
            $config['mailtype']            = 'html';
            $config['charset']             = 'utf-8';
            $config['newline']             = '\r\n';
            $config['wordwrap']            = TRUE;

            $this->load->library('email');
            $this->email->initialize($config);

            $this->email->from($email);
            $this->email->to($toEmail);
            $this->email->reply_to($email);
            $this->email->subject($subject);
            $this->email->message($body);
            $this->email->send();*/
          
            $res = ['type' => 'success' ,
                    'msg' => 'Registration Successfully..',
                    'result' => true];
        }
        else{
            $res = ['type' => 'error' , 
                    'msg' => 'Email ID already exist..',
                    'result' => true];
        }
        $this->response($res , RestController::HTTP_OK);
    }

    /***Forgot password functionality*********/
    public function forgotPass_post(){

        $post       = $this->request->body;
        $email      = $post['email'];
        $res = [];

        $email_id = $post['email'];
        $user_data = Users::where('_Email','=',$email_id)
                                ->get()
                                ->toArray();
                            
        if($user_data){
            $otpstring     = "1234567890";
            $otpsuffel     = str_shuffle($otpstring);
            $otp           = substr($otpsuffel,0,8);
            $reset_token   = md5($otp);
            $upd_data      = ['reset_token' => $reset_token,
                              'email_id'    => $email_id];

            $upd_forg_user              = Users::find($user_data[0]['_ID']);
            $upd_forg_user->reset_token = $reset_token;
            $upd_forg_user->save();

            $tempdata = Templates::where('_Slug','forgot-password')
                                ->first()
                                ->toarray();

            /*$subject   = $tempdata['_Subject'];
            $toEmail   = $tempdata['_Mail_to'];
            $body      = $tempdata['_Content'];
            $body      = str_replace("%user%", $upd_forg_user->_Firstname.' '.$upd_forg_user->_Lastname, $body);
            $toEmail   = str_replace("%mail%", $email_id, $toEmail);
            $body      = str_replace("%emailid %", $email_id, $body);
            $body      = str_replace("%url%", base_url('front/Login/reset_password?resettoken='.$reset_token), $body);
            
            $sitename = sitename;

            //$body = $this->load->view('email/forgot_mail.php',$upd_data,TRUE);
            //echo $body;exit;
            $config = array();
            $config['useragent']  = 'CodeIgniter';
            $config['mailpath']   = '/usr/bin/sendmail'; // or "/usr/sbin/sendmail"
            $config['protocol']   = 'mail';
            $config['smtp_host']  = 'localhost';
            $config['smtp_port']  = '25';
            $config['mailtype']   = 'html';
            $config['charset']    = 'utf-8';
            $config['newline']    = '\r\n';
            $config['wordwrap']   = TRUE;

            $this->load->library('email');
            $this->email->initialize($config);
            $fromEmail = $email_id;
            $fromName  = "Ecomnew Support";
                        
            $this->email->from($fromEmail, $fromName);
            $this->email->to($toEmail);
            $this->email->reply_to($fromEmail);
            $this->email->subject($subject);
            $this->email->message($body);
            $this->email->send();*/

            $res = array('result' => "success",
                         "type" => "success", 
                         "msg" => "You get mail to reset password. Thank you!");
        }else{
            $res = array('result' => "error", 
                         "type" => "error", 
                         "msg" => "Your E-mail is not exists in our system, please try with yor exists email id. Thank you!");
        }
        $this->response($res , RestController::HTTP_OK);
    }

    /*******Update profile functionality(form-data)**********/
    public function updateProfile_post(){
        
        $res     = [];
        $post    = $this->input->post();
        $prohid  = $post['prohid'];
        $usertab = Users::find($prohid);
        $fname   = $post['fname'];
        $lname   = $post['lname'];
        $email   = $post['email'];
        $mno     = $post['mno'];
        $gender  = $post['gender'];
   
        $config['upload_path']      = 'assets/uploads/user/';
        $config['max_size']         = 10240;
        $config['allowed_types']    = '*';
        $config['file_ext_tolower'] = TRUE;

        $qwe                 = pathinfo($_FILES['user_photo']['name']);
        $file_name           = str_replace('.','',microtime(true)).'_USER_';
        $filepath            = $config['upload_path'] . $file_name .".". $qwe['extension'];
        $config['file_name'] = $file_name.".". $qwe['extension'];

        $this->load->library('upload');
        $this->upload->initialize($config);
        $error = [];

        if(!$this->upload->do_upload('user_photo')) {
            $error = array('error' => $this->upload->display_errors());
        }
        else{
            $imgdata = $this->upload->data();
            if(!empty($imgdata['file_name'])){
                $usertab->_Profilepic = $imgdata['file_name'];
            }
            else{
                $usertab->_Profilepic =$usertab->_Profilepic;
            }
        }
        $usertab->_Firstname = $fname;
        $usertab->_Lastname  = $lname;
        $usertab->_Email     = $email;
        $usertab->_Mobile    = $mno;
        $usertab->_Gender    = $gender;
        $usertab->_Created   = date("Y-m-d H:i:s");
        $usertab->save();

        $res = ['type' => 'success' , 
                'msg' => 'User Detail Updated successfully ',
                'result' => true];
        $this->response($res , RestController::HTTP_OK);
    }

    /*********change password functionality*/
    public function changePass_post(){
        $res     = [];
        $post    = $this->request->body;
        $prohid  = $post['prohid'];
        $cpass   = $post['cpass'];
        $newpass = $post['newpass'];
        
        $editusertab            = Users::where('_Password',md5($cpass))->first();
        if($editusertab){
            $editusertab->_Password = md5($newpass);
            $editusertab->_Created  = date("Y-m-d H:i:s");
            $editusertab->save();
            $res = ['type' => 'success' , 
                    'msg' => 'Password Updated successfully ',
                    'result' => true];
        }
        else{
            $res = ['type' => 'error' , 
                    'msg' => 'Curren Password is Wrong.',
                    'result' => true];
        }
        
        $this->response($res , RestController::HTTP_OK);
    }


}