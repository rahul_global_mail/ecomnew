<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {


    public function __construct(){

    	parent::__construct();
    	
    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		  $this->output->set_header('Pragma: no-cache');
    }

    public function index(){
            
     
	}

    public function addtocart($id){


        if($this->input->cookie('userid')){
            $loguser_id=$this->encryption->decrypt($this->input->cookie('userid'));
        }
        elseif($this->input->cookie('userid_new')){
            $loguser_id=$this->input->cookie('userid_new');   
        }
        else{
            $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
 
                
                    $input_length = strlen($permitted_chars);
                    $random_string = '';
                    for($i = 0; $i < 20; $i++) {
                        $random_character = $permitted_chars[mt_rand(0, $input_length - 1)];
                        $random_string .= $random_character;
                    }

            $loguser_id = $random_string;
            /*set_cookie('userid', $loguser_id,  time()+86400);*/
           /*set_cookie('userid',$loguser_id,time() + 24 * 3600);*/
            set_cookie('userid_new',$loguser_id,time() + 24 * 3600);
        }


        $p_data = Products::where('_ID',$id)
                        ->get()
                        ->toArray();
        
        $cart_check = Carts1::where('_ProductID',$id)
                            ->where('_UserID',$loguser_id)
                            ->get()
                            ->toArray();

        $att_data_fetch = Attdetail::where('_ProID',$p_data[0]['_ID'])
                                    ->where('_Default','1')
                                    ->get()
                                    ->toArray();

        $size_val1 = $att_data_fetch[0]['_Size'];
        $color_val1 = $att_data_fetch[0]['_Color'];

        if($_POST){

            if($att_data_fetch[0]['_Sellprice'] != ''){
                $price = $att_data_fetch[0]['_Sellprice'];    
            }
            else{
                $price = $att_data_fetch[0]['_Price'];
            }

            if($this->input->post('qty')){
                $qty = $this->input->post('qty');
            }
            else{
                $qty = 1;
            }
            $stotal = $price * $qty;
        }
        else{

            if($att_data_fetch[0]['_Sellprice'] != ''){
                $price = $att_data_fetch[0]['_Sellprice'];    
            }
            else{
                $price = $att_data_fetch[0]['_Price'];
            }
            $qty = '1';
            $stotal = $price;
        }

        if($cart_check){
            if($this->input->cookie('userid_new')){
                $find_id=$this->input->cookie('userid_new');   
            }
            else{
                $find_id=$cart_check[0]['_UserID'];
            }

            $upd_cart  = Carts1::where('_UserID',$find_id)->where('_ProductID',$id)->first(); 
            if($upd_cart){
                $upd_cart->_UserID    = $loguser_id;
                $upd_cart->_Quantity  = $qty;
                $upd_cart->_Unitprice = $price;
                $upd_cart->_Subtotal  = $stotal;
                $upd_cart->_Size      = $size_val1;
                $upd_cart->_Color     = $color_val1;
                $upd_cart->_Created   = date("Y-m-d H:i:s");
                $upd_cart->save();    
            }
            else{
                $upd_cart     = Carts1::where('_UserID',$loguser_id)->where('_ProductID',$id)->first();
                $upd_cart->_UserID    = $loguser_id;
                $upd_cart->_Quantity  = $qty;
                $upd_cart->_Unitprice = $price;
                $upd_cart->_Subtotal  = $stotal;
                $upd_cart->_Size      = $size_val1;
                $upd_cart->_Color     = $color_val1;
                $upd_cart->_Created   = date("Y-m-d H:i:s");
                $upd_cart->save();
            }
        }
        else{
            $ins_cart             = new Carts1();
            $ins_cart->_ProductID = $id;
            $ins_cart->_UserID    = $loguser_id;
            $ins_cart->_Quantity  = $qty;
            $ins_cart->_Unitprice = $price;
            $ins_cart->_Subtotal  = $stotal;
            $ins_cart->_Size      = $size_val1;
            $ins_cart->_Color     = $color_val1;
            $ins_cart->_Created   = date("Y-m-d H:i:s");
            $ins_cart->save();
        }
        redirect('front/home');
    }

    public function addtocart_shop($id){

        if($this->input->cookie('userid')){
            $loguser_id=$this->encryption->decrypt($this->input->cookie('userid'));
        }
        elseif($this->input->cookie('userid_new')){
            $loguser_id=$this->input->cookie('userid_new');   
        }
        else{
            $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
 
                
                    $input_length = strlen($permitted_chars);
                    $random_string = '';
                    for($i = 0; $i < 20; $i++) {
                        $random_character = $permitted_chars[mt_rand(0, $input_length - 1)];
                        $random_string .= $random_character;
                    }

            $loguser_id = $random_string;
            /*set_cookie('userid', $loguser_id,  time()+86400);*/
           /*set_cookie('userid',$loguser_id,time() + 24 * 3600);*/
            set_cookie('userid_new',$loguser_id,time() + 24 * 3600);
        }

        $p_data = Products::where('_ID',$id)
                        ->get()
                        ->toArray();
        
        $cart_check = Carts1::where('_ProductID',$id)
                            ->where('_UserID',$loguser_id)
                            ->get()
                            ->toArray();

        $att_data_fetch = Attdetail::where('_ProID',$p_data[0]['_ID'])
                                    ->where('_Default','1')
                                    ->get()
                                    ->toArray();

        $size_val1 = $att_data_fetch[0]['_Size'];
        $color_val1 = $att_data_fetch[0]['_Color'];

        if($_POST){

            if($att_data_fetch[0]['_Sellprice'] != ''){
                $price = $att_data_fetch[0]['_Sellprice'];    
            }
            else{
                $price = $att_data_fetch[0]['_Price'];
            }

            if($this->input->post('qty')){
                $qty = $this->input->post('qty');
            }
            else{
                $qty = 1;
            }
            $stotal = $price * $qty;
        }
        else{

            if($att_data_fetch[0]['_Sellprice'] != ''){
                $price = $att_data_fetch[0]['_Sellprice'];    
            }
            else{
                $price = $att_data_fetch[0]['_Price'];
            }
            $qty = '1';
            $stotal = $price;
        }
        
        if($cart_check){
            if($this->input->cookie('userid_new')){
                $find_id=$this->input->cookie('userid_new');   
            }
            else{
                $find_id=$cart_check[0]['_UserID'];
            }

            $upd_cart  = Carts1::where('_UserID',$find_id)->where('_ProductID',$id)->first(); 
            if($upd_cart){
                $upd_cart->_UserID    = $loguser_id;
                $upd_cart->_Quantity  = $qty;
                $upd_cart->_Unitprice = $price;
                $upd_cart->_Subtotal  = $stotal;
                $upd_cart->_Size      = $size_val1;
                $upd_cart->_Color     = $color_val1;
                $upd_cart->_Created   = date("Y-m-d H:i:s");
                $upd_cart->save();    
            }
            else{
                $upd_cart     = Carts1::where('_UserID',$loguser_id)->where('_ProductID',$id)->first();
                $upd_cart->_UserID    = $loguser_id;
                $upd_cart->_Quantity  = $qty;
                $upd_cart->_Unitprice = $price;
                $upd_cart->_Subtotal  = $stotal;
                $upd_cart->_Size      = $size_val1;
                $upd_cart->_Color     = $color_val1;
                $upd_cart->_Created   = date("Y-m-d H:i:s");
                $upd_cart->save();
            }
        }
        else{
            $ins_cart             = new Carts1();
            $ins_cart->_ProductID = $id;
            $ins_cart->_UserID    = $loguser_id;
            $ins_cart->_Quantity  = $qty;
            $ins_cart->_Unitprice = $price;
            $ins_cart->_Subtotal  = $stotal;
            $ins_cart->_Size      = $size_val1;
            $ins_cart->_Color     = $color_val1;
            $ins_cart->_Created   = date("Y-m-d H:i:s");
            $ins_cart->save();
        }
        
        redirect('productlist');
    }

     public function cart_view(){
        //$loguser_id=$this->encryption->decrypt($this->input->cookie('userid'));
        $loguser_id= null !== get_cookie('userid') ? $this->encryption->decrypt(get_cookie('userid')) : get_cookie('userid_new');

        $cart_data = Carts1::with('product')
                            ->where('_UserID',$loguser_id)
                            ->get()
                            ->toArray();

        $addetail = Stores::where('_ID',1)->first()->toarray();
        
        $now = date('Y-m-d');
        $coupon_data = Couponcode::whereDate('_Fromdate','<=',$now)
                                ->whereDate('_Todate','>=',$now)
                                ->get()
                                ->toArray();
        
        $j = 0;
        $yes_cat = 0;
        $dis_price = 0;
        $other_price = 0;
        $final_dis_price = 0;
        $discount_offer = 0;
        $total_price = 0;

        for($i = 0;$i < count($cart_data) ; $i++){
        
            if(in_array($cart_data[$i]['product']['_SubcatID'],$coupon_data[$j]['_Subcatid'])){
                $yes_cat = $yes_cat+1;
            }
        }

       
       if($yes_cat == count($coupon_data[$j]['_Subcatid'])){
        for($i = 0;$i < count($cart_data) ; $i++){
            $total_price +=  $cart_data[$i]['_Subtotal'];
        }
       
       if($yes_cat == count($coupon_data[$j]['_Subcatid'])){
           for($i = 0;$i < count($cart_data) ; $i++){
             
                if(in_array($cart_data[$i]['product']['_SubcatID'],$coupon_data[$j]['_Subcatid'])){
                    /*$dis_price += $cart_data[$i]['_Subtotal'];*/
                    $dis_price += $cart_data[$i]['_Unitprice'];
                    $old_q = $cart_data[$i]['_Quantity'] - 1;
                    if($old_q != '0'){
                        $other_price += $cart_data[$i]['_Unitprice'] * $old_q;
                    }
                }
                else{
                    $other_price += $cart_data[$i]['_Subtotal'];
                }
            }
        }

        if($yes_cat == count($coupon_data[$j]['_Subcatid'])){
            $discount_offer = 1;
            $final_dis_price = $dis_price - $coupon_data[0]['_Amount'];
        }
        else{
            for($i = 0;$i < count($cart_data) ; $i++){
                $other_price += $cart_data[$i]['_Subtotal'];
            }
        }

        /*$final_price = $final_dis_price + $other_price;*/
        $final_price = $total_price - $final_dis_price;
        $allpage  = Page::get()->toarray();
      
        $this->load->view('front/cart/cart_view',compact('cart_data','addetail','coupon_data','loguser_id','final_price','discount_offer','allpage'));

    }

    public function remove_cart($id){
        $del = Carts1::where('_ID',$id)->delete();

        redirect('cart_view');
    }


    public function cart_update(){
        for($i = 0; $i < count($_POST['cart_id']) ; $i++){

            $id    = $_POST['cart_id'][$i];
            $qty   = $_POST['qty_value'][$i];
            $price = $_POST['sub_total'][$i];
         
            $upd_cart_qp            = Carts1::find($id);
            $upd_cart_qp->_Quantity =  $qty;
            $upd_cart_qp->_Subtotal = $price;
            $upd_cart_qp->save();
        }

        $res = ['type' => 'success' , 'msg' => 'Cart Updated successfully ','url'=>'cart_view', 'result' => true];
        echo json_encode($res);
        exit;

    }

    public function addtocart_cart($id){

        if($this->input->cookie('userid')){
            $loguser_id=$this->encryption->decrypt($this->input->cookie('userid'));
        }
        elseif($this->input->cookie('userid_new')){
            $loguser_id=$this->input->cookie('userid_new');   
        }
        else{
            $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
 
                
                    $input_length = strlen($permitted_chars);
                    $random_string = '';
                    for($i = 0; $i < 20; $i++) {
                        $random_character = $permitted_chars[mt_rand(0, $input_length - 1)];
                        $random_string .= $random_character;
                    }

            $loguser_id = $random_string;
            /*set_cookie('userid', $loguser_id,  time()+86400);*/
           /*set_cookie('userid',$loguser_id,time() + 24 * 3600);*/
            set_cookie('userid_new',$loguser_id,time() + 24 * 3600);
        }

        $size_val = $this->input->post('size_val');
        $color_val = $this->input->post('color_val');

        $p_data = Products::where('_ID',$id)
                        ->get()
                        ->toArray();
        
        $cart_check = Carts1::where('_ProductID',$id)
                            ->where('_UserID',$loguser_id)
                            ->get()
                            ->toArray();

        $att_data_fetch = Attdetail::where('_ProID',$p_data[0]['_ID'])
                                    ->where('_Default','1')
                                    ->get()
                                    ->toArray();
        if($_POST){

            if($att_data_fetch[0]['_Sellprice'] != ''){
                $price = $att_data_fetch[0]['_Sellprice'];    
            }
            else{
                $price = $att_data_fetch[0]['_Price'];
            }

            $qty = $this->input->post('qty');
            $stotal = $price * $qty;
        }
        else{

            if($att_data_fetch[0]['_Sellprice'] != ''){
                $price = $att_data_fetch[0]['_Sellprice'];    
            }
            else{
                $price = $att_data_fetch[0]['_Price'];
            }
            $qty = '1';
            $stotal = $price;

        }
       
        if($cart_check){
            if($this->input->cookie('userid_new')){
                $find_id=$this->input->cookie('userid_new');   
            }
            else{
                $find_id=$cart_check[0]['_UserID'];
            }

            $upd_cart  = Carts1::where('_UserID',$find_id)
                            ->where('_ProductID',$id)
                            ->where('_Size',$size_val)
                            ->where('_Color',$color_val)
                            ->first(); 
            if($upd_cart){
                $upd_cart->_UserID    = $loguser_id;
                $upd_cart->_Quantity  = $qty;
                $upd_cart->_Unitprice = $price;
                $upd_cart->_Subtotal  = $stotal;
                $upd_cart->_Size      = $size_val;
                $upd_cart->_Color     = $color_val;
                $upd_cart->_Created   = date("Y-m-d H:i:s");
                $upd_cart->save();    
            }
            else{
                $upd_cart1     = Carts1::where('_UserID',$loguser_id)
                                        ->where('_ProductID',$id)
                                        ->where('_Size',$size_val)
                                        ->where('_Color',$color_val)
                                        ->first();

                if($upd_cart1){

                    $upd_cart1->_UserID    = $loguser_id;
                    $upd_cart1->_Quantity  = $qty;
                    $upd_cart1->_Unitprice = $price;
                    $upd_cart1->_Subtotal  = $stotal;
                    $upd_cart1->_Size      = $size_val;
                    $upd_cart1->_Color     = $color_val;
                    $upd_cart1->_Created   = date("Y-m-d H:i:s");
                    $upd_cart1->save();
                }
                else{
                    $ins_cart             = new Carts1();
                    $ins_cart->_ProductID = $id;
                    $ins_cart->_UserID    = $loguser_id;
                    $ins_cart->_Quantity  = $qty;
                    $ins_cart->_Unitprice = $price;
                    $ins_cart->_Subtotal  = $stotal;
                    $ins_cart->_Size      = $size_val;
                    $ins_cart->_Color     = $color_val;
                    $ins_cart->_Created   = date("Y-m-d H:i:s");
                    $ins_cart->save();
                }
            }
        }
        else{
            $ins_cart             = new Carts1();
            $ins_cart->_ProductID = $id;
            $ins_cart->_UserID    = $loguser_id;
            $ins_cart->_Quantity  = $qty;
            $ins_cart->_Unitprice = $price;
            $ins_cart->_Subtotal  = $stotal;
            $ins_cart->_Size      = $size_val;
            $ins_cart->_Color     = $color_val;
            $ins_cart->_Created   = date("Y-m-d H:i:s");
            $ins_cart->save();
        }
        $res = ['type' => 'success' , 'msg' => 'Add to Cart successfully ','url'=>'product_detail/'.$id, 'result' => true];
        echo json_encode($res);
        exit;
    }

    public function checkout(){
        $loguser_id=$this->encryption->decrypt($this->input->cookie('userid'));
         $cart_data = Carts1::with('product','user')
                            ->where('_UserID',$loguser_id)
                            ->get()
                            ->toArray();

        $addetail = Stores::where('_ID',1)->first()->toarray();
        
        $now = date('Y-m-d');
        $coupon_data = Couponcode::whereDate('_Fromdate','<=',$now)
                                ->whereDate('_Todate','>=',$now)
                                ->get()
                                ->toArray();
        
        $j = 0;
        $yes_cat = 0;
        $dis_price = 0;
        $other_price = 0;
        $final_dis_price = 0;
        $discount_offer = 0;
        $total_price = 0;

        for($i = 0;$i < count($cart_data) ; $i++){

            $total_price +=  $cart_data[$i]['_Subtotal'];
        }

        for($i = 0;$i < count($cart_data) ; $i++){

        
            if(in_array($cart_data[$i]['product']['_SubcatID'],$coupon_data[$j]['_Subcatid'])){
                $yes_cat = $yes_cat+1;
            }
        }
       
       if($yes_cat == count($coupon_data[$j]['_Subcatid'])){
           for($i = 0;$i < count($cart_data) ; $i++){
                $total_price +=  $cart_data[$i]['_Subtotal'];

                if(in_array($cart_data[$i]['product']['_SubcatID'],$coupon_data[$j]['_Subcatid'])){
                    /*$dis_price += $cart_data[$i]['_Subtotal'];*/
                    $dis_price += $cart_data[$i]['_Unitprice'];
                    $old_q = $cart_data[$i]['_Quantity'] - 1;
                    if($old_q != '0'){
                        $other_price += $cart_data[$i]['_Unitprice'] * $old_q;
                    }
                }
                else{
                    $other_price += $cart_data[$i]['_Subtotal'];
                }
            }
        }

        if($yes_cat == count($coupon_data[$j]['_Subcatid'])){
            $discount_offer = 1;
            $final_dis_price = $dis_price - $coupon_data[0]['_Amount'];
        }
        else{
            for($i = 0;$i < count($cart_data) ; $i++){
                $other_price += $cart_data[$i]['_Subtotal'];
            }
        }

        $sub_total1 = 0;
        for($i = 0;$i < count($cart_data) ; $i++){
            $sub_total1 += $cart_data[$i]['_Subtotal'];
        }

        /*$final_price = $final_dis_price + $other_price;*/
        $final_price = $total_price - $final_dis_price;

        $addetail = Stores::where('_ID',1)->first()->toarray();
        $allpage  = Page::get()->toarray();
        $coupon_data = Couponcode::get()->toArray();
        $order_check = Order::orderBy('_ID', 'desc')->first();

        $order_data = Order::where('_Userid',$loguser_id)
                    ->where('_Orderstatus','0')
                    ->first();

        if($order_data != ''){
            $order_id = $order_data->_Orderid;
        }
        else if($order_check != ''){
            $order_id = $order_check->_Orderid + 1;
        }
        else{
            $order_id = '1001';   
        }

        if($discount_offer == '1'){
            $disc = $coupon_data[0]['_Amount'];
        }
        else{
            $disc = 0;
        }

        if($order_data != ''){
            $order_data->_Orderid       = $order_id;
            $order_data->_Userid        = $loguser_id;
            $order_data->_Couponid      = $coupon_data[$j]['_ID'];
            $order_data->_Subtotal      = $sub_total1;
            $order_data->_Getwayid      = '0';
            $order_data->_Paymentstatus = '0';
            $order_data->_Orderstatus   = '0';
            $order_data->_Grandtotal    = $final_price;
            $order_data->_Discount      = $disc;
            $order_data->_Created       = date("Y-m-d H:i:s");
            $order_data->save();
        }
        else{
            $ins_order                  = new Order();
            $ins_order->_Orderid        = $order_id;
            $ins_order->_Userid         = $loguser_id;
            $ins_order->_Subtota        = $sub_total1;
            $ins_order->_Couponid      = $coupon_data[$j]['_ID'];
            $ins_order->_Subtotal       = $sub_total1;
            $ins_order->_Getwayid       = '0';
            $ins_order->_Paymentstatus  = '0';
            $ins_order->_Orderstatus    = '0';
            $ins_order->_Grandtotal     = $final_price;
            $ins_order->_Discount       = $disc;
            $ins_order->_Created        = date("Y-m-d H:i:s");
            $ins_order->save();
        }

        $new_order_data = Order::where('_Userid',$loguser_id)
                    ->where('_Orderstatus','0')
                    ->get()
                    ->toArray();
        $new_order_data = Order::with('coupon')
                                ->where('_Userid',$loguser_id)
                                ->where('_Orderstatus','0')
                                ->get()
                                ->toArray();

        $this->load->view('front/checkout/index',compact('cart_data','addetail','loguser_id','coupon_data','allpage','new_order_data'));
    }

    public function fetchaddress(){
        $id = $this->encryption->decrypt($this->input->cookie('userid'));
        $custadd = Addresses::with('users','cities','states')->where('_UserID',$id)->get()->toarray();
        $addetail = Stores::where('_ID',1)->first()->toarray();
        $allcity = Cities::get()->toarray();
        $allstate = States::get()->toarray();
        $allpage  = Page::get()->toarray();
        $this->load->view('front/checkout/useradd',compact('custadd','addetail','allcity','allstate','allpage'));
    }
    public function checkaddress(){
       $checkid =  $this->input->post('id');
        $custadd = Addresses::with('users','cities','states')->where('_ID',$checkid)->
        first()->toarray();
            $res = ['data'=>$custadd,'result' => true];
            echo json_encode($res);
            exit;
    }

    public function otheradd(){
        $userid = $this->encryption->decrypt($this->input->cookie('userid'));

        $addr_id = $this->input->post('addr_id');

        /*$url = "userpayment/$userid";*/
        if($this->input->post('other')=='save' && $this->input->post('other')!='')
        {
            $name = $this->input->post('aname', null);
            $addr1 = $this->input->post('addr1', null);
            $addr2 = $this->input->post('addr2', null);
            $zip = $this->input->post('zip', null);
            $city = $this->input->post('city', null);
            $state = $this->input->post('state', null);
            $othadd = new Addresses();
            $othadd->_Name = $name;
            $othadd->_UserID = $userid;
            $othadd->_Line1 = $addr1;
            $othadd->_Line2 = $addr2;
            $othadd->_Postcode = $zip;
            $othadd->_City = $city;
            $othadd->_State = $state;
            $othadd->save();

            $lid = $othadd->_ID;

            $res = array('result' => "success", "type" => "success", "msg" => "Address saved successfully",'url' => 'userpayment');
        }
        else
        {
            $res = array('result' => "success", "type" => "success", "msg" => "Address Fetch successfully",'url' => 'userpayment');
        }


        if($addr_id){
            set_cookie('addr_id',$addr_id,'3600');
        }
        else{
            set_cookie('addr_id',$lid,'3600');   
        }

        echo json_encode($res);exit;
    }

    public function emptyshopcart(){
        $userid = $this->encryption->decrypt($this->input->cookie('userid'));
        $userid_new = $this->input->cookie('userid_new');
        if($userid)
        {
            $cartd = Carts1::where('_UserID',$userid)->delete();
            redirect('cart_view');
        }
        else
        {
            $cartd = Carts1::where('_UserID',$userid_new)->delete();
            redirect('cart_view');
        }
        
      
    }

}	

