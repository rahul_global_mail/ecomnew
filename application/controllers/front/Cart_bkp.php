<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {


    public function __construct(){

    	parent::__construct();
    	
    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		  $this->output->set_header('Pragma: no-cache');
    }

    public function index(){
            
     
	}

    public function addtocart($id){


        if($this->input->cookie('userid')){
            $loguser_id=$this->encryption->decrypt($this->input->cookie('userid'));
        }
        elseif($this->input->cookie('userid_new')){
            $loguser_id=$this->input->cookie('userid_new');   
        }
        else{
            $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
 
                
                    $input_length = strlen($permitted_chars);
                    $random_string = '';
                    for($i = 0; $i < 20; $i++) {
                        $random_character = $permitted_chars[mt_rand(0, $input_length - 1)];
                        $random_string .= $random_character;
                    }

            $loguser_id = $random_string;
            /*set_cookie('userid', $loguser_id,  time()+86400);*/
           /*set_cookie('userid',$loguser_id,time() + 24 * 3600);*/
            set_cookie('userid_new',$loguser_id,time() + 24 * 3600);
        }


        $p_data = Products::where('_ID',$id)
                        ->get()
                        ->toArray();
        
        $cart_check = Carts1::where('_ProductID',$id)
                            ->where('_UserID',$loguser_id)
                            ->get()
                            ->toArray();
                           /* echo '<pre>';
                            print_r($cart_check);
                            exit;*/

        if($_POST){
            if($p_data[0]['_Sellprice'] != ''){
                $price = $p_data[0]['_Sellprice'];    
            }
            else{
                $price = $p_data[0]['_Price'];
            }
            $qty = $this->input->post('qty');
            $stotal = $price * $qty;
        }
        else{
            if($p_data[0]['_Sellprice'] != ''){
                $price = $p_data[0]['_Sellprice'];    
            }
            else{
                $price = $p_data[0]['_Price'];
            }
            $qty = '1';
            $stotal = $price;
        }
       // echo $price;exit;

        if($cart_check){
            if($this->input->cookie('userid_new')){
                $find_id=$this->input->cookie('userid_new');   
            }
            else{
                $find_id=$cart_check[0]['_UserID'];
            }

            $upd_cart  = Carts1::where('_UserID',$find_id)->where('_ProductID',$id)->first(); 
            if($upd_cart){
                $upd_cart->_UserID    = $loguser_id;
                $upd_cart->_Quantity  = $qty;
                $upd_cart->_Unitprice = $price;
                $upd_cart->_Subtotal  = $stotal;
                $upd_cart->_Created   = date("Y-m-d H:i:s");
                $upd_cart->save();    
            }
            else{
                $upd_cart     = Carts1::where('_UserID',$loguser_id)->where('_ProductID',$id)->first();
                $upd_cart->_UserID    = $loguser_id;
                $upd_cart->_Quantity  = $qty;
                $upd_cart->_Unitprice = $price;
                $upd_cart->_Subtotal  = $stotal;
                $upd_cart->_Created   = date("Y-m-d H:i:s");
                $upd_cart->save();
            }
        }
        else{
            $ins_cart             = new Carts1();
            $ins_cart->_ProductID = $id;
            $ins_cart->_UserID    = $loguser_id;
            $ins_cart->_Quantity  = $qty;
            $ins_cart->_Unitprice = $price;
            $ins_cart->_Subtotal  = $stotal;
            $ins_cart->_Created   = date("Y-m-d H:i:s");
            $ins_cart->save();
        }
        redirect('front/home');
    }

    public function addtocart_shop($id){
        
        if($this->input->cookie('userid')){
            $loguser_id=$this->encryption->decrypt($this->input->cookie('userid'));
        }
        elseif($this->input->cookie('userid_new')){
            $loguser_id=$this->input->cookie('userid_new');   
        }
        else{
            $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
 
                
                    $input_length = strlen($permitted_chars);
                    $random_string = '';
                    for($i = 0; $i < 20; $i++) {
                        $random_character = $permitted_chars[mt_rand(0, $input_length - 1)];
                        $random_string .= $random_character;
                    }

            $loguser_id = $random_string;
            /*set_cookie('userid', $loguser_id,  time()+86400);*/
           /*set_cookie('userid',$loguser_id,time() + 24 * 3600);*/
            set_cookie('userid_new',$loguser_id,time() + 24 * 3600);
        }

        $p_data = Products::where('_ID',$id)
                        ->get()
                        ->toArray();
        
        $cart_check = Carts1::where('_ProductID',$id)
                            ->where('_UserID',$loguser_id)
                            ->get()
                            ->toArray();

         if($_POST){
            if($p_data[0]['_Sellprice'] != ''){
                $price = $p_data[0]['_Sellprice'];    
            }
            else{
                $price = $p_data[0]['_Price'];
            }
            $qty = $this->input->post('qty');
            $stotal = $price * $qty;
        }
        else{
            if($p_data[0]['_Sellprice'] != ''){
                $price = $p_data[0]['_Sellprice'];    
            }
            else{
                $price = $p_data[0]['_Price'];
            }
            $qty = '1';
            $stotal = $price;
        }
        if($cart_check){
            if($this->input->cookie('userid_new')){
                $find_id=$this->input->cookie('userid_new');   
            }
            else{
                $find_id=$cart_check[0]['_UserID'];
            }

            $upd_cart  = Carts1::where('_UserID',$find_id)->where('_ProductID',$id)->first(); 
            if($upd_cart){
                $upd_cart->_UserID    = $loguser_id;
                $upd_cart->_Quantity  = $qty;
                $upd_cart->_Unitprice = $price;
                $upd_cart->_Subtotal  = $stotal;
                $upd_cart->_Created   = date("Y-m-d H:i:s");
                $upd_cart->save();    
            }
            else{
                $upd_cart     = Carts1::where('_UserID',$loguser_id)->where('_ProductID',$id)->first();
                $upd_cart->_UserID    = $loguser_id;
                $upd_cart->_Quantity  = $qty;
                $upd_cart->_Unitprice = $price;
                $upd_cart->_Subtotal  = $stotal;
                $upd_cart->_Created   = date("Y-m-d H:i:s");
                $upd_cart->save();
            }
        }
        else{
            $ins_cart             = new Carts1();
            $ins_cart->_ProductID = $id;
            $ins_cart->_UserID    = $loguser_id;
            $ins_cart->_Quantity  = $qty;
            $ins_cart->_Unitprice = $price;
            $ins_cart->_Subtotal  = $stotal;
            $ins_cart->_Created   = date("Y-m-d H:i:s");
            $ins_cart->save();
        }
        redirect('productlist');
    }

    public function cart_view(){
        //$loguser_id=$this->encryption->decrypt($this->input->cookie('userid'));
        $loguser_id= null !== get_cookie('userid') ? $this->encryption->decrypt(get_cookie('userid')) : get_cookie('userid_new');

        $cart_data = Carts1::with('product')
                            ->where('_UserID',$loguser_id)
                            ->get()
                            ->toArray();

        $addetail = Stores::where('_ID',1)->first()->toarray();
		
        $now = date('Y-m-d');
        $coupon_data = Couponcode::whereDate('_Fromdate','<=',$now)
                                ->whereDate('_Todate','>=',$now)
                                ->first()
                                ->get()
                                ->toArray();
        
        $yes_cat = 0;
        $dis_price = 0;
        $other_price = 0;

        for($i = 0;$i < count($cart_data) ; $i++){
        
            if(in_array($cart_data[$i]['product']['_SubcatID'],$coupon_data[0]['_Subcatid'])){
                $yes_cat = $yes_cat+1;
            }
        }

        if($yes_cat == count($coupon_data[0]['_Subcatid'])){
           for($i = 0;$i < count($cart_data) ; $i++){
            
                if(in_array($cart_data[$i]['product']['_SubcatID'],$coupon_data[0]['_Subcatid'])){
                    $dis_price += $cart_data[$i]['_Subtotal'];
                }
                else{
                    $other_price += $cart_data[$i]['_Subtotal'];
                }
            }
        }

        $final_dis_price = $dis_price - $coupon_data[0]['_Amount'];
        $final_price = $final_dis_price + $other_price;
        

	   $this->load->view('front/cart/cart_view',compact('cart_data','addetail','coupon_data','loguser_id','final_price'));

    }

    public function remove_cart($id){
        $del = Carts1::where('_ID',$id)->delete();

        redirect('cart_view');
    }


    public function cart_update(){
        for($i = 0; $i < count($_POST['cart_id']) ; $i++){

            $id    = $_POST['cart_id'][$i];
            $qty   = $_POST['qty_value'][$i];
            $price = $_POST['sub_total'][$i];
         
            $upd_cart_qp            = Carts1::find($id);
            $upd_cart_qp->_Quantity =  $qty;
            $upd_cart_qp->_Subtotal = $price;
            $upd_cart_qp->save();
        }

        $res = ['type' => 'success' , 'msg' => 'Cart Updated successfully ','url'=>'cart_view', 'result' => true];
        echo json_encode($res);
        exit;

    }

    public function addtocart_cart($id){

        if($this->input->cookie('userid')){
            $loguser_id=$this->encryption->decrypt($this->input->cookie('userid'));
        }
        elseif($this->input->cookie('userid_new')){
            $loguser_id=$this->input->cookie('userid_new');   
        }
        else{
            $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
 
                
                    $input_length = strlen($permitted_chars);
                    $random_string = '';
                    for($i = 0; $i < 20; $i++) {
                        $random_character = $permitted_chars[mt_rand(0, $input_length - 1)];
                        $random_string .= $random_character;
                    }

            $loguser_id = $random_string;
            /*set_cookie('userid', $loguser_id,  time()+86400);*/
           /*set_cookie('userid',$loguser_id,time() + 24 * 3600);*/
            set_cookie('userid_new',$loguser_id,time() + 24 * 3600);
        }


        $p_data = Products::where('_ID',$id)
                        ->get()
                        ->toArray();
        
        $cart_check = Carts1::where('_ProductID',$id)
                            ->where('_UserID',$loguser_id)
                            ->get()
                            ->toArray();

        if($_POST){
            if($p_data[0]['_Sellprice'] != ''){
                $price = $p_data[0]['_Sellprice'];    
            }
            else{
                $price = $p_data[0]['_Price'];
            }
            $qty = $this->input->post('qty');
            $stotal = $price * $qty;
        }
        else{
            if($p_data[0]['_Sellprice'] != ''){
                $price = $p_data[0]['_Sellprice'];    
            }
            else{
                $price = $p_data[0]['_Price'];
            }
            $qty = '1';
            $stotal = $price;
        }

        $attid = $this->input->post('attid');
        $attdetail = $this->input->post('attdetail');
       
        if($cart_check){
            if($this->input->cookie('userid_new')){
                $find_id=$this->input->cookie('userid_new');   
            }
            else{
                $find_id=$cart_check[0]['_UserID'];
            }

            $upd_cart  = Carts1::where('_UserID',$find_id)->where('_ProductID',$id)->first(); 
            if($upd_cart){
                $upd_cart->_UserID    = $loguser_id;
                $upd_cart->_Quantity  = $qty;
                $upd_cart->_Unitprice = $price;
                $upd_cart->_Subtotal  = $stotal;
                $upd_cart->_Attid     = $attid;
                $upd_cart->_Attdetail = $attdetail;
                $upd_cart->_Created   = date("Y-m-d H:i:s");
                $upd_cart->save();    
            }
            else{
                $upd_cart     = Carts1::where('_UserID',$loguser_id)->where('_ProductID',$id)->first();
                $upd_cart->_UserID    = $loguser_id;
                $upd_cart->_Quantity  = $qty;
                $upd_cart->_Unitprice = $price;
                $upd_cart->_Subtotal  = $stotal;
                $upd_cart->_Attid     = $attid;
                $upd_cart->_Attdetail = $attdetail;
                $upd_cart->_Created   = date("Y-m-d H:i:s");
                $upd_cart->save();
            }
        }
        else{
            $ins_cart             = new Carts1();
            $ins_cart->_ProductID = $id;
            $ins_cart->_UserID    = $loguser_id;
            $ins_cart->_Quantity  = $qty;
            $ins_cart->_Unitprice = $price;
            $ins_cart->_Subtotal  = $stotal;
            $ins_cart->_Attdetail = $attdetail;
            $ins_cart->_Created   = date("Y-m-d H:i:s");
            $ins_cart->_Created   = date("Y-m-d H:i:s");
            $ins_cart->save();
        }
        $res = ['type' => 'success' , 'msg' => 'Add to Cart successfully ','url'=>'product_detail/'.$id, 'result' => true];
        echo json_encode($res);
        exit;
    }

    public function checkout(){
        $loguser_id=$this->encryption->decrypt($this->input->cookie('userid'));
         $cart_data = Carts1::with('product','user')
                            ->where('_UserID',$loguser_id)
                            ->get()
                            ->toArray();
        $addetail = Stores::where('_ID',1)->first()->toarray();
    $this->load->view('front/checkout/index',compact('cart_data','addetail','loguser_id'));
    }

    public function fetchaddress(){
        $id = $this->encryption->decrypt($this->input->cookie('userid'));
        $custadd = Addresses::with('users','cities','states')->where('_UserID',$id)->get()->toarray();
        $addetail = Stores::where('_ID',1)->first()->toarray();
        $allcity = Cities::get()->toarray();
        $allstate = States::get()->toarray();
        $this->load->view('front/checkout/useradd',compact('custadd','addetail','allcity','allstate'));
    }
    public function checkaddress(){
       $checkid =  $this->input->post('id');
        $custadd = Addresses::with('users','cities','states')->where('_ID',$checkid)->
        first()->toarray();
            $res = ['data'=>$custadd,'result' => true];
            echo json_encode($res);
            exit;
    }

    public function otheradd(){
        $userid = $this->encryption->decrypt($this->input->cookie('userid'));
        /*$url = "userpayment/$userid";*/
        if($this->input->post('other')=='save' && $this->input->post('other')!='')
        {
            $name = $this->input->post('aname', null);
            $addr1 = $this->input->post('addr1', null);
            $addr2 = $this->input->post('addr2', null);
            $zip = $this->input->post('zip', null);
            $city = $this->input->post('city', null);
            $state = $this->input->post('state', null);
            $othadd = new Addresses();
            $othadd->_Name = $name;
            $othadd->_UserID = $userid;
            $othadd->_Line1 = $addr1;
            $othadd->_Line2 = $addr2;
            $othadd->_Postcode = $zip;
            $othadd->_City = $city;
            $othadd->_State = $state;
            $othadd->save();
            $res = array('result' => "success", "type" => "success", "msg" => "Address saved successfully",'url' => 'userpayment');
        }
        else
        {
            $res = array('result' => "success", "type" => "success", "msg" => "Address Fetch successfully",'url' => 'userpayment');
        }
        echo json_encode($res);exit;
    }

    public function emptyshopcart(){
        $userid = $this->encryption->decrypt($this->input->cookie('userid'));
        $cartd = Carts1::where('_UserID',$userid)->delete();
        redirect('cart_view');
      
    }

}	

