<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {
	


    public function __construct(){

    	parent::__construct();
    	
    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		  $this->output->set_header('Pragma: no-cache');
    }

    public function index(){
    	$galldetail = Galleries::get()->toarray();
      $addetail = Stores::where('_ID',1)->first()->toarray();
      $allpage  = Page::get()->toarray();

      
		$this->load->view('front/gallery/index',compact('galldetail', 'addetail','allpage'));
	  }

   
}	

