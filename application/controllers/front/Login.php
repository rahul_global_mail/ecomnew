<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct(){

    	parent::__construct();

    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
    }

    public function index()
	{
		$pagename = "Login";
		$addetail = Stores::where('_ID',1)->first()->toarray();
		$allpage  = Page::get()->toarray();
		$this->load->view('front/login',compact('pagename','addetail','allpage'));
	}

	public function register(){
		$pagename = "Register";
		$addetail = Stores::where('_ID',1)->first()->toarray();
		$allpage  = Page::get()->toarray();
		$this->load->view('front/register',compact('pagename','addetail','allpage'));
	}
	  public function email_exist()
  {
   if($this->input->post('_email') == $this->input->post('email')) {
      echo 'true'; 
    } else {
      $existdata  = Users::where('_Email',$this->input->post('email'))->get()->toArray();
      if(count($existdata)>0)
      {
        echo 'false';
      }
      else {
        echo 'true';
      }
      
    }
  } 

	public function register_action(){
		$fname = $this->input->post('fname');
		$lname = $this->input->post('lname');
		$mno = $this->input->post('mno');
		$email = $this->input->post('email');
		$pass  = md5($this->input->post('pass'));

		$test = Users::where('_Email','=',$email)->get();		
		if(count($test)<=0){
			$ins = new Users();
			$ins->_Firstname = $fname;
			$ins->_Lastname  = $lname;
			$ins->_Mobile    = $mno;
			$ins->_Email     = $email;
			$ins->_Password  = $pass;
			$ins->_Created   = date('Y-m-d H:i:s');
			$ins->save();
			$tempdata = Templates::where('_Slug','register')->first()->toarray();

			$subject = $tempdata['_Subject'];
			$body = $tempdata['_Content'];
			$toEmail=$tempdata['_Mail_to'];
			$body = str_replace("%user%", $fname.' '.$lname, $body);
			$toEmail = str_replace("%mail%", $email, $toEmail);
			$body = str_replace("%url%", siteurl, $body);
			$body = str_replace("%sitename%", sitename, $body);
			$sitename= sitename;

			$config = array();
	        $config['useragent']           = 'CodeIgniter';
	        $config['mailpath']            = '/usr/bin/sendmail'; // or "/usr/sbin/sendmail"
	        $config['protocol']            = 'mail';
	        $config['smtp_host']           = 'localhost';
	        $config['smtp_port']           = '25';
	        $config['mailtype'] = 'html';
	        $config['charset']  = 'utf-8';
	        $config['newline']  = '\r\n';
	        $config['wordwrap'] = TRUE;

	        $this->load->library('email');
        	$this->email->initialize($config);

        	$this->email->from($email);
			$this->email->to($toEmail);
			$this->email->reply_to($email);
			$this->email->subject($subject);
			$this->email->message($body);
			$this->email->send();
          
			$res = ['type' => 'success' , 'msg' => 'Registration Successfully..','url'=>'front/Login', 'result' => true];
        	echo json_encode($res);
        	exit;
		}
		
	}

	public function login_action(){
		$email = $this->input->post('email');
		$pass = $this->input->post('pass');

		$check_data = Users::where('_Email',$email)
							->where('_Password',md5($pass))
							->get()
							->toarray();

		
		if(count($check_data)>0){
		$username = $check_data[0]['_Firstname'];
		set_cookie('username',$username,60*60*24*365*1);
		set_cookie('user_password',$this->encryption->encrypt($pass),60*60*24*365*1);
		set_cookie('userid',$this->encryption->encrypt($check_data[0]['_ID']),60*60*24*365*1);

        
		$res = ['type' => 'success' , 'msg' => 'You are login successfully.', 'url' => 'front', 'result' => true];
		}else{
		$res = ['type' => 'error' , 'msg' => 'Invalid email or password.', 'url' => 'login', 'result' => true];
		}
		
		echo json_encode($res);
		exit;
		
	}

	public function forgotpass(){
		$addetail = Stores::where('_ID',1)->first()->toarray();
		$allpage  = Page::get()->toarray();
		$this->load->view('front/forgot',compact('addetail','allpage'));
	}

	public function forgotpass_action(){
    	$this->form_validation->set_rules('email','E - mail', 'required');
    		$err='';
    		if ($this->form_validation->run() === FALSE){
				if(validation_errors()){
					$data = validation_errors().$err;	
					
				} 
			}else{
				$email_id = $this->input->post('email');
				$user_data = Users::where('_Email','=',$email_id)
										->get()
										->toArray();
									
				if($user_data){
					$otpstring="1234567890";
					$otpsuffel= str_shuffle($otpstring);
					$otp = substr($otpsuffel,0,8);
					$reset_token = md5($otp);
					$upd_data = ['reset_token' => $reset_token,'email_id' => $email_id];
					$upd_forg_user = Users::find($user_data[0]['_ID']);
					$upd_forg_user->reset_token = $reset_token;
					$upd_forg_user->save();

					$tempdata = Templates::where('_Slug','forgot-password')->first()->toarray();
					

					$subject = $tempdata['_Subject'];
					$toEmail=$tempdata['_Mail_to'];
					$body = $tempdata['_Content'];
					$body = str_replace("%user%", $upd_forg_user->_Firstname.' '.$upd_forg_user->_Lastname, $body);
					$toEmail = str_replace("%mail%", $email_id, $toEmail);
					$body = str_replace("%emailid %", $email_id, $body);
					$body = str_replace("%url%", base_url('front/Login/reset_password?resettoken='.$reset_token), $body);
					
					$sitename= sitename;

					//$body = $this->load->view('email/forgot_mail.php',$upd_data,TRUE);
					//echo $body;exit;
					$config = array();
			        $config['useragent']           = 'CodeIgniter';
			        $config['mailpath']            = '/usr/bin/sendmail'; // or "/usr/sbin/sendmail"
			        $config['protocol']            = 'mail';
			        $config['smtp_host']           = 'localhost';
			        $config['smtp_port']           = '25';
			        $config['mailtype'] = 'html';
			        $config['charset']  = 'utf-8';
			        $config['newline']  = '\r\n';
			        $config['wordwrap'] = TRUE;

			        $this->load->library('email');
		        	$this->email->initialize($config);
					/*$toEmail=$this->input->post('email');*/
					$fromEmail= $email_id;
					$fromName= "Digimining Support";
					/*$toEmail = $this->input->post('email');*/
								
					$this->email->from($fromEmail, $fromName);
					$this->email->to($toEmail);
					$this->email->reply_to($fromEmail);
					$this->email->subject($subject);
					$this->email->message($body);
					$this->email->send();

					$res = array('result' => "success", "type" => "success", "msg" => "You get mail to reset password. Thank you!",'url' => 'login');
				}else{
					$res = array('result' => "error", "type" => "error", "msg" => "Your E-mail is not exists in our system, please try with yor exists email id. Thank you!",'url' => 'forgotpass');
				}
			}
		echo json_encode($res);
	}

	public function reset_password(){
		$reset_token = $this->input->get('resettoken');
		$addetail = Stores::where('_ID',1)->first()->toarray();
		$allpage  = Page::get()->toarray();
		$this->load->view('front/reset_password',compact('reset_token','addetail','allpage'));
	}

	public function resetpass_action(){
		if($this->input->post('reset_token') != ''){
    		$reset_token = $this->input->post('reset_token');
    	}else{
    		$reset_token =  end($this->uri->segment_array());	
    	}

    	$user_data = Users::where('reset_token','=',$reset_token)
    							->get()
    							->toArray();
    	
    	if($user_data){
    		if($this->input->post('pass') && $this->input->post('cpass')){

    			$user_upd_pass = Users::find($user_data[0]['_ID']);
    			$user_upd_pass->_Password = md5($this->input->post('pass'));
    			$user_upd_pass->save();
    			$tempdata = Templates::where('_Slug','reset-password')->first()->toarray();
    			$subject = $tempdata['_Subject'];
				$body = $tempdata['_Content'];
				$body = str_replace("%user%", $user_upd_pass->_Firstname.' '.$user_upd_pass->_Lastname, $body);
				$body = str_replace("%mail %", siteurl, $body);

				$toEmail= $tempdata['_Mail_to'];
				$email = $user_upd_pass->_Email;
				$toEmail = str_replace("%mail%", $email, $toEmail);

				$this->load->library('email');

	        	$this->email->from($email);
				$this->email->to($toEmail);
				$this->email->reply_to($email);
				$this->email->subject($subject);
				$this->email->message($body);
				$this->email->send();

    			$res = array('result' => "success", "type" => "success", "msg" => "You have successfully reset your password. You can now login with your new password",'url' => 'login');
    			
    	    }
    	    else{
    	    	$res = array('result' => "error", "type" => "error", "msg" => "Something went wrong.",'url' => 'login');
    	    
    	    }
    	}else{
    		$res = array('result' => "error", "type" => "error", "msg" => "Something went wrong ,Please try again.",'url' => 'login');
    	}
    	echo json_encode($res);exit;
		
	}

    function logout_front()
	{
		delete_cookie("userid");
		delete_cookie("username");
		delete_cookie("user_password");
		redirect('front/home','refresh');
	}
    
}
?>