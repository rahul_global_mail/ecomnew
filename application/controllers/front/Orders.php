<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {


    public function __construct(){

    	parent::__construct();
    	
    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		  $this->output->set_header('Pragma: no-cache');
    }

    public function index(){
            
     
	}

	public function createorder(){
           
	     $logid = $this->input->post('logid');
       $orderid = $this->input->post('orderid');

       $total = $this->input->post('total');
       $getwayid = $this->input->post('pay');
       
       $cart_data =  Carts1::where('_UserID',$logid)
                    ->get()
                    ->toArray();

        for($i = 0 ;$i < count($cart_data); $i++){
       $getwayid = $this->input->post('pay');
       $addr_id = $this->input->cookie('addr_id');

       $order_addr_up = Order::where('_Orderid',$orderid)->first();
       $order_addr_up->_Addressid = $addr_id;
       $order_addr_up->save();
                            
       $cart_data =  Carts1::where('_UserID',$logid)
                    ->get()
                    ->toArray();
        for($i = 0 ;$i < count($cart_data); $i++){


          $ins_order_detail             = new Orderdetail();
          $ins_order_detail->_UserID    = $logid;
          $ins_order_detail->_OrderID   = $orderid;
          $ins_order_detail->_ProductID = $cart_data[$i]['_ProductID'];
          $ins_order_detail->_Quantity  = $cart_data[$i]['_Quantity'];
          $ins_order_detail->_Size      = $cart_data[$i]['_Size'];
          $ins_order_detail->_Color     = $cart_data[$i]['_Color'];
          $ins_order_detail->_Unitprice = $cart_data[$i]['_Unitprice'];
          $ins_order_detail->_Subtotal  = $cart_data[$i]['_Subtotal'];
          $ins_order_detail->_Created   = date("Y-m-d H:i:s");
          $ins_order_detail->save();

        }
       
       /*echo '<pre>';print_r($cart_data);exit;*/
        /*$cartdata = Carts1::where('_UserID',$logid)->delete();*/
        exit;

          $att_fetch = Attdetail::where('_ProID',$cart_data[$i]['_ProductID'])
                                  ->where('_Size',$cart_data[$i]['_Size'])
                                  ->where('_Color',$cart_data[$i]['_Color'])
                                  ->get()
                                  ->toArray();

          $upd_att = Attdetail::where('_ProID',$cart_data[$i]['_ProductID'])
                                ->where('_Size',$cart_data[$i]['_Size'])
                                ->where('_Color',$cart_data[$i]['_Color'])
                                ->first();
          if(count($att_fetch)){
            $quan_val = $att_fetch[0]['_Quantity'] - $cart_data[$i]['_Quantity'];
          }
          else{
            $quan_val = 0;
          }
          
          $upd_att->_Quantity = $quan_val;
          $upd_att->save();

        }

        $cartdata = Carts1::where('_UserID',$logid)->delete();

        $order_fetch = Order::where('_UserID',$logid)
                          ->where('_Orderstatus','0')
                          ->first();
        
        $order_fetch->_Orderstatus = '1';
        $order_fetch->save();


       /* $tempdata = Templates::where('_Slug','place-order')->first()->toarray();

            $subject = $tempdata['_Subject'];
            $body = $tempdata['_Content'];
            $toEmail=$tempdata['_Mail_to'];
            $body = str_replace("%orderid%", $ordertab->_Orderid, $body);
            $body = str_replace("%user%",$cart_pdata[0]['user']['fullname'], $body);
            $body = str_replace("%url%", siteurl, $body);
            $body = str_replace("%orderdate%", $ordertab->_Orderdate, $body);
            $body = str_replace("%sitename%", sitename, $body);
            $i =1;
            $product =array();
            foreach ($cart_pdata as  $value) {
              $body=  str_replace("%no%",$i, $body);
              $body=  str_replace("%name%",$value['product']['_Name'], $body);
              $body=  str_replace("%quantity%",$value['_Quantity'], $body);
              $body=  str_replace("%price%",$value['_Unitprice'], $body);
              $body= str_replace("%total%",$value['_Subtotal'], $body);
              $i++;
            }
            echo'<pre>';
            print_r($body);
           
           
            $toEmail = str_replace("%mail%", $cart_pdata[0]['user']['_Email'], $toEmail);
            $sitename= sitename;*/

        $orderid = "showcustomerorder/$ordertab->_Orderid";
        $res = array('result' => "success", "type" => "success", "msg" => "Order created successfully",'url' => $orderid);
        echo json_encode($res);exit;
	}
    public function showcustomerorder($id){
        $orderinfo = Order::with('users','product')->where('_Orderid',$id)->first()->toarray();
        $coupon_data = Couponcode::get()->toArray();
        $addetail = Stores::where('_ID',1)->first()->toarray();
        $allpage  = Page::get()->toarray();

        $orderid = "showcustomerorder/$order_fetch->_Orderid";
        $res = array('result' => "success", "type" => "success", "msg" => "Order created successfully",'url' => $orderid);
        echo json_encode($res);exit;
	}
  
    public function showcustomerorder($id){
        $orderinfo = Order::with('users','orderdetail','orderdetail.product','coupon')
                          ->where('_Orderid',$id)
                          ->get()
                          ->toarray();

        $coupon_data = Couponcode::get()->toArray();

        $addetail = Stores::where('_ID',1)
                          ->first()
                          ->toarray();

        $allpage  = Page::get()->toarray();


        $this->load->view('front/order/showcustomerorder',compact('orderinfo','coupon_data','allpage','addetail'));

    }
}