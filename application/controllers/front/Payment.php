<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {


    public function __construct(){

    	parent::__construct();
    	
    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		  $this->output->set_header('Pragma: no-cache');
    }

    public function index(){
            
     
	}

	public function userpayment(){
		$loguser_id=$this->encryption->decrypt($this->input->cookie('userid'));


        $cart_data = Carts1::with('product','user')
                            ->where('_UserID',$loguser_id)
                            ->get()
                            ->toArray();


        $allProd = [];

        $sum = 0;
        foreach ($cart_data as  $value) {
        	$sum  = $value['_Subtotal'] + $sum;
        	$allProd['total']=$sum;
        }
		$paydata = Payments::get()->toarray();
        $allpage  = Page::get()->toarray();


        $new_order_data = Order::where('_Userid',$loguser_id)
                    ->where('_Orderstatus','0')
                    ->get()
                    ->toArray();


        
		$addetail = Stores::where('_ID',1)->first()->toarray();
		$this->load->view('front/payment/index',compact('paydata','addetail','cart_data','allProd','loguser_id','allpage','new_order_data'  ));
	}
}