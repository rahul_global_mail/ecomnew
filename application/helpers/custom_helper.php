<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

// ------------------------------------------------------------------------

if ( ! function_exists('response_json'))
{
	/**
	 *
	 * convert array to json string.
	 *
	 * @param	array
	 * @return	json string	depends on what the array contains
	 */
	function response_json($array)
	{
		header('Content-Type: application/json');
		return collect($array)->toJson();
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('random_string'))
{
	/**
	 * Create a "Random" String
	 *
	 * @param	string	type of random string.  basic, alpha, alnum, numeric, nozero, unique, md5, encrypt and sha1
	 * @param	int	number of characters
	 * @return	string
	 */
	function random_string($type = 'alnum', $len = 8)
	{
		switch ($type):
			case 'basic':
				return mt_rand();
			case 'alnum':
			case 'numeric':
			case 'nozero':
			case 'alpha':
				switch ($type):
					case 'alpha':
						$pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
						break;
					case 'alnum':
						$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
						break;
					case 'numeric':
						$pool = '0123456789';
						break;
					case 'nozero':
						$pool = '123456789';
						break;
				endswitch;
				return substr(str_shuffle(str_repeat($pool, ceil($len / strlen($pool)))), 0, $len);
			case 'unique': // todo: remove in 3.1+
			case 'md5':
				return md5(uniqid(mt_rand()));
			case 'encrypt': // todo: remove in 3.1+
			case 'sha1':
				return sha1(uniqid(mt_rand(), TRUE));
		endswitch;
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('nice_date'))
{
	/**
	 * Turns many "date" strings into system date format
	 * that is actually useful.
	 *
	 * @param	string	date
	 * @param	string	format to return
	 * @return	string
	 */
	function nice_date($bad_date = '', $format = FALSE)
	{
		if(empty($bad_date)):
			$bad_date = date('Y-m-d');
		endif;
		if(!$format):
			$CI =& get_instance();
			$format = $CI->config->item('ta_date_format');
		endif;
   		return Carbon::parse($bad_date)->format($format);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('nice_price'))
{
	/**
	 * Turns many "price" strings into something
	 * that is actually useful.
	 *
	 * @param	string	The terribly formatted amount string
	 * @param	string	format to return (same as php number function)
	 * @return	string
	 */
	function nice_price($bad_price = '', $format = FALSE)
	{
		if(empty($bad_price)):
			return '$0.00';
		endif;
		$CI =& get_instance();
		$price_format = $CI->config->item('ta_price_format');
		if($format):
			$bad_price = number_format($bad_price,2);
		endif;
   		return str_replace("S", $CI->config->item('ta_currency'), str_replace("A", $bad_price, $price_format));
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('upload_file'))
{
	/**
	 *
	 * Upload file to give location with given configuration.
	 *
	 * @param	string fileinput
	 * @param	array of file upload configurations
	 * @return	response array either error or uploaded file data
	 */
	function upload_file($fileinput,$configarr)
	{
		$CI =& get_instance();
		$CI->load->library('upload');
		$CI->upload->initialize($configarr);
		if(!$CI->upload->do_upload($fileinput)):
			$response = array('error' => $CI->upload->display_errors());
		else:
			$response = array('upload_data' => $CI->upload->data());
		endif;
		return $response;
	}
}

// --------------------------------------------------------------------

if ( ! function_exists('do_hash'))
{
	/**
	 * Hash encode a string
	 *
	 * @todo	Remove in version 3.1+.
	 * @deprecated	3.0.0	Use PHP's native hash() instead.
	 * @param	string	$str
	 * @param	string	$type = 'sha1'
	 * @return	string
	 */
	function do_hash($str, $type = 'sha1')
	{
		if ( ! in_array(strtolower($type), hash_algos())):
			$type = 'md5';
		endif;

		return hash($type, $str);
	}
}

// --------------------------------------------------------------------

if ( ! function_exists('get_year_range'))
{
	/**
	 * Get range between current year and given year
	 *
	 * @param	int	$year = 49(50 year)
	 * @return	arr
	 */

	function get_year_range($year = 49)
	{
		$rangearr = range((Carbon::now()->format('Y') - $year), Carbon::now()->format('Y'), 1);
		array_unshift($rangearr, "");
		unset($rangearr[0]);
		return array_reverse($rangearr);
	}
}

// --------------------------------------------------------------------

if ( ! function_exists('subject_dropdown'))
{
	/**
	 * Get subject array with id as key and name as value. 
	 *
	 * @param	string $year with comma separated
	 * @return	arr
	 */

	function subject_dropdown($year = array("0"),$isDropdown = false)
	{
		$data = array();
		$subject = SubjectMaster::with('year')->where('Publish',"1")->whereIn('YearID',$year)->get();
		if($isDropdown):
			foreach ($subject as $sub):
				$data[$sub->SubjectID] = ($sub->YearID != "0") ? $sub->SubjectName." - ".$sub->year['Year'] : $sub->SubjectName;
			endforeach;
		else:
			foreach ($subject as $sub):
				$data[] = array("id"=>$sub->SubjectID,"text"=>$sub->SubjectName." - ".$sub->year->Year);
			endforeach;
		endif;
		return $data;
	}
}

if ( ! function_exists('subject_dropdown_home'))
{
	/**
	 * Get subject array with id as key and name as value. 
	 *
	 * @param	string $year with comma separated
	 * @return	arr
	 */

	function subject_dropdown_home($year = array("0"))
	{
		$data = array();
		$subject = SubjectMaster::with('year')->where('Publish',"1")->whereIn('YearID',$year)->get();
		foreach ($subject as $sub):
			$data[] = array("id"=>$sub->SubjectID,"text"=>$sub->SubjectName);
		endforeach;
		return $data;
	}
}

// --------------------------------------------------------------------

if ( ! function_exists('subject'))
{
	/**
	 * Get subject array with id as key and name as value. 
	 *
	 * @param	string $subject ids with comma separated
	 * @return	arr
	 */

	function subject($sub = "")
	{
		$subject = SubjectMaster::with('year')->where('Publish',"1")->whereIn('SubjectID',explode(',',trim($sub,',')))->get();
		return $subject;
	}
}

// --------------------------------------------------------------------

if ( ! function_exists('create_timeslot'))
{
	/**
	 * Get subject array with id as key and name as value. 
	 *
	 * @param	time $start time 
	 * @param	time $end time 
	 * @param	integer $duration in hour 
	 * @return	arr
	 */

	function create_timeslot($start,$end,$duration = 1)
	{
		$periods = array();
		$start_time = Carbon::parse($start)->format('h:i');
		$end_time = Carbon::parse($end)->format('h:i');
	    while(strtotime($start_time) <= strtotime($end_time)):
	        $start = Carbon::parse($start_time)->format('h:i');
	        $end = Carbon::parse($start_time)->addHours($duration)->format('h:i');
	        $start_time = Carbon::parse($start_time)->addHours($duration);
	        if(strtotime($start_time) <= strtotime($end_time)):
	            $periods[] = $start.'-'.$end;
	        endif;
	    endwhile;
	    return $periods;
	}
}

// --------------------------------------------------------------------

if ( ! function_exists('create_dayslot'))
{
	/**
	 * Get subject array with id as key and name as value. 
	 *
	 * @param	time $start date 
	 * @param	time $end date 
	 * @param	integer $duration in day 
	 * @return	arr
	 */

	function create_dayslot($start,$end,$duration = 1)
	{
		$periods = array();
		$start_date = Carbon::parse($start)->format('Y-m-d');
		$end_date = Carbon::parse($end)->format('Y-m-d');
	    while($start_date <= $end_date):
	        $periods[] = $start_date;
	        $start_date = Carbon::parse($start_date)->addDay()->format('Y-m-d');
	    endwhile;
	    return $periods;
	}
}

// ----------------------------------------------------

if ( ! function_exists('character_limiter'))
{
	/**
	 * Character Limiter
	 *
	 * Limits the string based on the character count.  Preserves complete words
	 * so the character count may not be exactly as specified.
	 *
	 * @param	string
	 * @param	int
	 * @param	string	the end character. Usually an ellipsis
	 * @return	string
	 */
	function character_limiter($str, $n = 500, $end_char = '&#8230;')
	{
		if (mb_strlen($str) < $n):
			return $str;
		endif;

		// a bit complicated, but faster than preg_replace with \s+
		$str = preg_replace('/ {2,}/', ' ', str_replace(array("\r", "\n", "\t", "\v", "\f"), ' ', $str));

		if (mb_strlen($str) <= $n):
			return $str;
		endif;

		$out = '';
		foreach (explode(' ', trim($str)) as $val):
			$out .= $val.' ';

			if (mb_strlen($out) >= $n):
				$out = trim($out);
				return (mb_strlen($out) === mb_strlen($str)) ? $out : $out.$end_char;
			endif;
		endforeach;
	}
}

// --------------------------------------------------------------------

if ( ! function_exists('image_resizer'))
{
	/**
	 * Check API token for APIs for mobile application
	 *
	 * @param	string path
	 * @param	int width
	 * @param	int height
	 * @return	image
	 */

	function image_resizer($path, $width = 50, $height = 50)
	{
		// for convert to square image
		// if ($width > $height) {
		//     $square = $height;              // $square: square side length
		//     $offsetX = ($width - $height) / 2;  // x offset based on the rectangle
		//     $offsetY = 0;              // y offset based on the rectangle
		// }
		// // vertical rectangle
		// elseif ($height > $width) {
		//     $square = $width;
		//     $offsetX = 0;
		//     $offsetY = ($height - $width) / 2;
		// }
		// // it's already a square
		// else {
		//     $square = $width;
		//     $offsetX = $offsetY = 0;
		// }
		// imagecopyresampled($image_p,$image,0,0,$offsetX,$offsetY,$width,$height,$info[0],$info[1]);
		if(empty($path)):
			return '';
		endif;
		$info = getimagesize($path);
		$mime = $info['mime'];
		header('Content-type: $mime');

		$ratio_orig = ($info[0] / $info[1]);

		if(($width / $height) > $ratio_orig):
			$width = ($height * $ratio_orig);
		else:
			$height = ($width / $ratio_orig);
		endif;
		$image_p = imagecreatetruecolor($width,$height);
		switch($mime):
			case 'image/jpeg':
				$image = imagecreatefromjpeg($path);
				imagecopyresampled($image_p,$image,0,0,0,0,$width,$height,$info[0],$info[1]);
				imagejpeg($image_p,null,100);
				break;
			case 'image/png':
				$image = imagecreatefrompng($path);
				imagealphablending($image_p,false);
				imagesavealpha($image_p,true);
				imagefilledrectangle($image_p,0,0,$width,$height,imagecolorallocatealpha($image_p,255,255,255,127));
				imagecopyresampled($image_p,$image,0,0,0,0,$width,$height,$info[0],$info[1]);
				imagepng($image_p,null,9);
				break;
			default: 
				throw new Exception('Unknown image type.');
		endswitch;
	}
}

// --------------------------------------------------------------------

if ( ! function_exists('verify_token'))
{
	/**
	 * Check API token for APIs for mobile application
	 *
	 * @param	int userid
	 * @param	string token
	 * @return	boolean
	 */

	function verify_token($userid = "",$token = "")
	{
		if($userid == "" || $token == "")
			return array("success"=>"false","message" => "Parameter should not be null.");
		$user = UserMaster::find($userid);
		if($user):
			if($token === "TAapp ".$user->ApiToken):
				return array("success"=>"true");
			else:
				return array("success"=>"false","message" => "Token doesn't match or expire.");
			endif;
		else:
			return array("success"=>"false","message" => "User not found.");
		endif;
	}
}

// --------------------------------------------------------------------

if ( ! function_exists('safe_base64_encode'))
{
	/**
	 * Encode data
	 *
	 * @param	int data
	 * @return	string encrypted
	 */

	function safe_base64_encode($data)
	{
		return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
	}
}

// --------------------------------------------------------------------

if ( ! function_exists('safe_base64_decode'))
{
	/**
	 * Decrypt data
	 *
	 * @param	int data
	 * @return	string decrypted
	 */

	function safe_base64_decode($data)
	{
		return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
	}
}