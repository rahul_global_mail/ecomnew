<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Addresses extends Eloquent {

    protected $table = "ec_user_address"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "_ID";

    public function users(){
        return $this->belongsTo('Users','_UserID');
    }

	public function cities(){
        return $this->belongsTo('Cities','_City');
    }

    public function states(){
        return $this->belongsTo('States','_State');
    }
}