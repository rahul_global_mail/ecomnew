<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Attdetail extends Eloquent {

    protected $table = "ec_attribute_detail"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "_ID";

    	/*public function products(){
        return $this->belongsTo('Products','_ProID');
    }
   */
    /* public function attributes(){
       return $this->belongsTo('Attributes','_AttID', '_ID');

    }
     
    public function product(){

    }*/
     public function product(){
       return $this->belongsTo('Products','_ProID');
    }

    public function scopeDefault($query)
    {
        return $query->where('_Default', '=', 1);
    }
  
}