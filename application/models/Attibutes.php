<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Attributes extends Eloquent {

    protected $table = "ec_attributes"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "_ID";
   
}