<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Carts1 extends Eloquent {

    protected $table = "ec_cart"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "_ID";

    public function product(){
    	return $this->belongsTo('Products','_ProductID', '_ID');
    }
    public function user(){
    	return $this->belongsTo('Users','_UserID', '_ID');
    }
   

}