<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Categories extends Eloquent {

    protected $table = "ec_category"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "_ID";
}