<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Cities extends Eloquent {

    protected $table = "ec_cities"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "id";
}