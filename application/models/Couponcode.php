<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Couponcode extends Eloquent {

    protected $table = "ec_coupon"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "_ID";
     protected $casts = [
            '_Subcatid' => 'array',
    ];

}