<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Galleries extends Eloquent {

    protected $table = "ec_gallery"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "_ID";
     protected $casts = [
            '_Image' => 'array',
            '_Url' => 'array',
        ];

  
}