<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Order extends Eloquent {
    use \Staudenmeir\EloquentJsonRelations\HasJsonRelationships;

    protected $table = "ec_orders"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "_ID";

  	public function users(){
        return $this->belongsTo('Users','_Userid');
    }

    public function payment(){
        return $this->belongsTo('Payments','_Getwayid');
    }

 	public function product(){ 
        return $this->belongsToJson('Products', '_Productdetail[]->id');
    }

    public function orderdetail(){
        return $this->hasMany('Orderdetail','_OrderID', '_Orderid');
    }

    public function coupon(){
        return $this->belongsTo('Couponcode','_Couponid');

    }
}