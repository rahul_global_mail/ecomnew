<?php 
use Carbon\Carbon;
use \Illuminate\Database\Eloquent\Model as Eloquent;

class Products extends Eloquent {
    use \Staudenmeir\EloquentJsonRelations\HasJsonRelationships;

    protected $table = "ec_product"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "_ID";
    protected $casts = [
            '_Specification' => 'array',
            '_Adddetails' => 'array',
            '_Image' => 'array',
    ];

    protected $appends = ['is_new', 'is_sale'];

 	public function categories(){
    	return $this->belongsTo('Categories','_CatID', '_ID');
    }

    public function subcategory(){
       return $this->belongsTo('Categories','_SubcatID', '_ID');
    }

    public function orders() {
        return $this->hasManyJson(Order::class, '_Productdetail[]->id');
    }

    public function reviews(){
        return $this->hasMany('Reviews','_ProductID');  
    }

    public function wishlists(){
       return $this->hasMany('Wishlists','_Productid');
    }


    public function attributes(){
       return $this->hasMany('Attdetail','_ProID');
    }

    public function getIsNewAttribute()
    {   
        $dt = Carbon::now();
        $compare_date = $dt->subDays(3);
        return $this->attributes['is_new'] = ($this['_Created'] > $compare_date) ? true : false;
    }

    public function getIsSaleAttribute()
    {   

        $sell_price = (null !== $this->attributes()) ? $this->attributes()->default()->select('_Sellprice')->first()->toArray() : 0;
        return $this->attributes['is_sale'] = ($sell_price['_Sellprice'] > 0) ? true : false;
    }

}