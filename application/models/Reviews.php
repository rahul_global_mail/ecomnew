<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Reviews extends Eloquent {

    protected $table = "ec_review"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "_ID";
    
    public function products(){
    	return $this->belongsTo('Products','_ProductID', '_ID');
    }
    public function users(){
       return $this->belongsTo('Users','_UserID', '_ID');
    }

}