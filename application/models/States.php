<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class States extends Eloquent {

    protected $table = "ec_states"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "id";
}