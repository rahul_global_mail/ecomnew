<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Stores extends Eloquent {

    protected $table = "ec_store"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "_ID";

    
}