<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Users extends Eloquent {

    protected $table = "ec_users"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "_ID";
    protected $casts = [
            '_Address' => 'array'
        ];
    protected $appends = ['fullname'];

    public function states(){
        return $this->belongsTo('States','_State', 'id');
    }

    public function cities(){
        return $this->belongsTo('Cities','_City', 'id');
    }
    
    public function order(){
        return $this->hasMany('Order','_Userid','_ID');
    }

    public function address(){
        return $this->hasMany('Address','_Userid','_ID');
    }

    public function getFullNameAttribute()
    {
        return "{$this->_Firstname} {$this->_Lastname}";
    }
    

   
}