<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Wishlists extends Eloquent {

    protected $table = "ec_wishlist"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "_ID";

  	public function users(){
        return $this->belongsTo('Users','_Userid');
    }

    public function products(){
    	return $this->belongsTo('Products','_Productid');	
    }
}