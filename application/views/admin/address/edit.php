<form action="{{base_url()}}admin/address/update" id="editAddressform" name="editAddressform" method="post"  >
    <div class="row">
        <div class="col-md-6">
            <input type="hidden" name="id" value="{{ address._ID}}">
            <input type="hidden" name="userid" value="{{ this.session.userdata.address_user }}">
        	<div class="form-group">
				<label for="addr1" class="control-label">Address Name *</label>
				<input type="text" name="aname" id="aname" class="form-control" placeholder="Address Name" value="{{ address._Name}}" maxlength="50" required/>
			</div>
            <div class="form-group">
                <label for="addr1" class="control-label">Address Line 1</label>
                <input type="text" name="addr1" class="form-control" placeholder="Address Line 1" value="{{ address._Line1}}" required/>
            </div>
            <div class="form-group">
                <label for="addr2" class="control-label">Address Line 2</label>
                <input type="text" name="addr2" class="form-control" placeholder="Address Line 2" value="{{ address._Line2}}" required/>
            </div>
            <div class="form-group">
                <label for="zip" class="control-label">Zipcode</label>
                <input type="number" name="zip" class="form-control" min="0" placeholder="Zipcode" value="{{ address._Postcode}}" required/>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="country" class="control-label">Country</label>
                <input type="text" name="country" class="form-control" placeholder="Country" value="India" required/>
            </div>
            <div class="form-group">
            	<label for="exampleInputEmail1" >state</label>
            	<select name="state" id="state" class="form-control">
              		<option value="">Select State</option>
          			{% if count(allstate) > 0 %}
          			{% for statedata in allstate %} 
              		<option value="{{statedata.id}}"{% if address._State == statedata.id %} selected {% endif %}>{{statedata.name}}</option>
              		{% endfor %}
              		{% endif %} 
        		</select>
          	</div>
           	<div class="form-group">
	            <label for="exampleInputEmail1">city</label>
	            <select name="city" id="city" class="form-control">
              		<option value="">Select City</option>
          			{% if count(allcity) > 0 %}
          			{% for citydata in allcity %} 
              		<option value="{{citydata.id}}"{% if address._City == citydata.id %} selected {% endif %}>{{citydata.name}}</option>
              		{% endfor %}
              		{% endif %} 
        		</select>
            	
          	</div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
    </div>
</form>

{% block scripts %}
  	<script>
  		$('#state').on('change', function () {
		var state_id = $(this).val();
        ecommerce.ajax_req({id: state_id},"getcity").done(function (response) {
        	var res = $.parseJSON(response);
            $("#city").empty();
            $("#city").append("<option value=''>Please select</option>");
            $.each(res,function (index,value){
                $("#city").append("<option value='"+value.id+"' > "+value.name+" </option>");
            });
         })
    })
  	</script>
{% endblock %}