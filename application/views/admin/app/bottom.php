 <!-- jQuery -->
<script src="{{ constant('cmstheme') }}plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
 <script src="{{ constant('cmstheme') }}plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- <script src="{{ constant('cmstheme') }}js/jquery-ui.min.js"></script> -->
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ constant('cmstheme') }}plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="{{ constant('cmstheme') }}plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
  <script src="{{ constant('cmstheme') }}plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
 <script src="{{ constant('cmstheme') }}plugins/jqvmap/jquery.vmap.min.js"></script> 
<script src="{{ constant('cmstheme') }}plugins/jqvmap/maps/jquery.vmap.usa.js"></script> 
<!-- jQuery Knob Chart -->
 <script src="{{ constant('cmstheme') }}plugins/jquery-knob/jquery.knob.min.js"></script> 
<!-- daterangepicker -->
<script src="{{ constant('cmstheme') }}plugins/moment/moment.min.js"></script>
<script src="{{ constant('cmstheme') }}plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ constant('cmstheme') }}plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="{{ constant('cmstheme') }}plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="{{ constant('cmstheme') }}plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="{{ constant('cmstheme') }}dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ constant('cmstheme') }}dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ constant('cmstheme') }}dist/js/demo.js"></script>

<!-- Sweet-Alert  -->
<script type="text/javascript" src="{{ constant('cmstheme') }}plugins/sweetalert2/sweetalert2.all.min.js"></script>

<script src="{{ constant('cmstheme') }}js/jquery.validate.min.js"></script>
  <script src="{{ constant('cmstheme') }}js/additional-methods.min.js"></script>
  <script src="{{ constant('cmstheme') }}js/modernizr_pricing.js"></script>
   <script type="text/javascript" src="{{ constant('cmstheme') }}js/noty.min.js"></script>
   <!-- DataTables -->
<script src="{{ constant('cmstheme') }}plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ constant('cmstheme') }}plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ constant('cmstheme') }}plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ constant('cmstheme') }}plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

<!-- InputMask -->
<script src="{{ constant('cmstheme') }}plugins/moment/moment.min.js"></script>

<!-- Select2 -->
<script src="{{ constant('cmstheme') }}plugins/select2/js/select2.full.min.js"></script>

<script src="{{ constant('cmstheme') }}plugins/sweetalert2/sweetalert2.all.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>

<script src="{{ constant('cmstheme') }}js/app.js"></script> 


<!-- <script src="{{ constant('cmstheme') }}js/attribute.js"></script> -->

<!-- 
<script type="text/javascript" src="{{ constant('cmstheme') }}js/dropzone/dropzone.min.js"></script> -->

<!-- bs-custom-file-input -->
<script src="{{ constant('cmstheme') }}plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>

     








 