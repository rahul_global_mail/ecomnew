<!-- jQuery -->
<script src="{{ constant('cmstheme') }}plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{ constant('cmstheme') }}plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="{{ constant('cmstheme') }}dist/js/adminlte.min.js"></script>
<script src="{{ constant('cmstheme') }}js/jquery.validate.min.js"></script>
<script src="{{ constant('cmstheme') }}js/additional-methods.min.js"></script>
<script type="text/javascript" src="{{ constant('cmstheme') }}js/noty.min.js"></script>
<script src="{{ constant('cmstheme') }}js/app.js"></script>