<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<html lang="en">
<head>
	<base id="myBase" href="{{ base_url() }}">
	<!-- <meta name="_token" content="{{ this.security.get_csrf_hash() }}" /> -->

	{% include 'admin/app/log_header.php' %}
	
		   {% if pagename == 'Home' %}
		<title>Next-Gen of Free Bitcoin Cloud Mining</title>
	   {% else %}
		<title>Ecommerce : {{ pagename }}</title>
	   {% endif %}
</head>
<body class="hold-transition login-page">
	
	<div class="content_wrapper">
		{% block content %}{% endblock %}
	</div>

	{% include 'admin/app/log_footer.php' %}
	
	
	{% block scripts %}{% endblock %}
</body>
</html>