{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    	<!-- Content Header (Page header) -->
    	<section class="content-header">
      		<div class="container-fluid">
			<div class="row mb-2">
	          		<div class="col-sm-6">
	             			<h1>{{pagetitle}}</h1> 
	          		</div>
		          	<div class="col-sm-6">
			            	<ol class="breadcrumb float-sm-right">
			              		<li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
			              		<li class="breadcrumb-item active">category</li>
			            	</ol>
		          	</div>
        		</div>
      		</div><!-- /.container-fluid -->
    	</section>

      <!-- Main content -->
    	<section class="content">
      		<div class="row">
        		<div class="col-12">
          			<div class="card">
            				<div class="card-body">
               					<div class="row">
              						<div class="col-md-12">              
              							<form class="form-horizontal"  id="attribute_form" name="attribute_form" method="post">
                						{% if editid is not defined %}
						                   	<div class="form-group row">
						                    		<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Name</label>
						                    		<div class="col-sm-11">
						                    			<input type="text"  class="form-control" id="name" name="name" placeholder="Enter Name" >
						                    		</div>
						                  	</div>
                   							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Values</label>
							                    	<div class="col-sm-11">
							                    		<input type="text"  class="form-control" id="values" name="values" placeholder="Enter Values" placeholder="Comma Seperated Values">
							                    	</div>
                  							</div>
                  							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Type</label>
							                    	<div class="col-sm-11">
								                    	<select name="type" class="form-control">
								                    		<option value="">Select Types</option>
								                    		{% for key, type in types %}
											        	<option value="{{key}}">{{type}}</option>
											    	{% endfor %}
								                    	</select>
							                    	</div>
                  							</div>

                  							 <div class="form-group row">
								                <label class="col-sm-1 col-form-label" for="exampleInputEmail1">category</label>
								                <div class="col-sm-11">
								                <select name="category" id="category" class="form-control">
								                  <option value="">--Please select--</option>
<<<<<<< HEAD
								                  {% if catinfo|length > 0 %}
=======
								                  {% if count(catinfo)> 0 %}
>>>>>>> 1387377960807901e00344fd01a10caa7cc72e84
								                    {% for cdata in catinfo %}
								                      <option value="{{cdata._ID}}">{{cdata._Name}}</option>
								                    {% endfor %}
								                  {% endif %} 
								                </select>
								            </div>
								            
								              </div>
                  							<input type="submit" class="btn btn-success" value="Submit" style="margin-top: 10px;">
							        {% else %}  
<<<<<<< HEAD
							        {% if attribute|length > 0 %}
=======
							        {% if count(attribute) > 0 %}
>>>>>>> 1387377960807901e00344fd01a10caa7cc72e84
          								<input type="hidden" id="id" name="id" value="{{attribute._ID}}">
                 							<div class="form-group row">
						                    		<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Name</label>
						                    		<div class="col-sm-11">
						                    			<input type="text"  class="form-control" id="name" name="name" placeholder="Enter Name" value="{{attribute._Name}}" >
						                    		</div>
						                  	</div>
                   							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Values</label>
							                    	<div class="col-sm-11">
							                    		<input type="text"  class="form-control" id="values" name="values" placeholder="Enter Values" placeholder="Comma Seperated Values" value="{{attribute._Values}}">
							                    	</div>
                  							</div>
                  							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Type</label>
							                    	<div class="col-sm-11">
								                    	<select name="type" class="form-control">
								                    		<option value="">Select Types</option>
								                    		{% for key, type in types %}
								                    			{% set selected = '' %}
												        {% if key == attribute._Type %}
												           {% set selected = 'selected' %}
												        {% endif %}
											        	<option value="{{key}}" {{ selected }}>{{type}}</option>
											    	{% endfor %}
								                    	</select>
							                    	</div>
                  							</div>
                  							  <div class="form-group row">
								                <label class="col-sm-1 col-form-label" for="exampleInputEmail1">category</label>
								                <div class="col-sm-11">
								                <select name="category" id="category" class="form-control">
								                  <option value="">--Please select--</option>
<<<<<<< HEAD
								                  {% if catinfo|length > 0 %}
=======
								                  {% if count(catinfo)> 0 %}
>>>>>>> 1387377960807901e00344fd01a10caa7cc72e84
								                    {% for cdata in catinfo %}
								                      <option value="{{cdata._ID}}">{{cdata._Name}}</option>
								                    {% endfor %}
								                  {% endif %} 
								                </select>
								            </div>
								            
								              </div>
          								<input type="submit" class="btn btn-success" value="Update" style="margin-top: 10px;">
	                  					{% endif %}
                  						{% endif %}
      								</form>
                  					</div>
                  				</div>
                  			</div>
                  		</div>
                  	</div>
		</div>                  	
  	</section>
</div>
  <!-- /.content-wrapper -->

  {% endblock %}

{% block scripts %}
	<script src="{{ constant('cmstheme') }}js/attribute.js"></script>
  {% endblock %}