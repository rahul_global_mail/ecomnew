{% extends 'admin/app/index.php' %}
 
{% block content %} 

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1>Product Attributes</h1> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li> 
              <li class="breadcrumb-item active">Product Attributes</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card  card-default">
            <div class="card-header">
	    	<div class="card-tools">
		      	<a href="{{base_url('createattribute')}}" class="btn  btn-block bg-gradient-primary pull-right"><i class="fas fa-plus"></i> Add Attribute</a>
	    	</div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="user_tab" class="table table-bordered table-hover">
                <thead>
                <tr>
<<<<<<< HEAD
                  <th>{% if attributes|length > 0 %}
                    <button class="btn btn-danger" name="delete_all" id="delete_all" class="glyphicon glyphicon-pencil"><i class="fas fa-user-slash"></i></button>{% endif %}</th>
=======
>>>>>>> 1387377960807901e00344fd01a10caa7cc72e84
                  <th>No</th>
                  <th>Name</th>
                  <th>Values</th>
                  <th>Type</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
<<<<<<< HEAD
                  {% if attributes|length > 0 %}
=======
                  {% if count(attributes) > 0 %}
>>>>>>> 1387377960807901e00344fd01a10caa7cc72e84
                
                  {% set i = 1 %}
                    {% for attribute in attributes %}
                    
                   
                <tr>
<<<<<<< HEAD
                   <td><input type="checkbox" class="delete_checkbox" name="checkuser[]" id="checkuser" value="{{attribute._ID}}"></td>
=======
>>>>>>> 1387377960807901e00344fd01a10caa7cc72e84
                  <td>{{ i }}</td>
                  <td>{{attribute._Name}}</td>
                  <td>{{attribute._Values}}</td>
                  <td>{{attribute._Type}}</td>
                  <td> 
<<<<<<< HEAD
                    <a href="{{ base_url('editattribute/'~attribute._ID) }}" class="btn btn-warning"><i class="fas fa-edit"></i></a>
=======
                    <a href="{{ base_url('editattribute/'~attribute._ID) }}" class="glyphicon glyphicon-pencil"><i class="fa fa-pen"></i></a>
                    <a onclick="return attribute._sweetalert('{{attribute._ID}}')" class="glyphicon glyphicon-pencil"><i class="fa fa-trash"></i></a>
>>>>>>> 1387377960807901e00344fd01a10caa7cc72e84
                  </td>
                </tr>
                
                  {% set i = i + 1 %}
                 {% endfor %}
                
                             {% endif %}
                </tbody>
              
            </table>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

  
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  {% endblock %}

{% block scripts %}
	<script src="{{ constant('cmstheme') }}js/attribute.js"></script>
  {% endblock %}