{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1>Category Create</h1> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
              <li class="breadcrumb-item active">category</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
               <div class="row">
              <div class="col-md-12">
              
              <form  id="category_form" name="category_form" method="post" enctype="multipart/form-data">
                {% if editid is not defined %}
                   <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text"  class="form-control" id="catname" name="catname" placeholder="Enter Name" >
                  </div>
                   <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" id="catdes" name="catdes" placeholder="Enter Description" cols="3"></textarea>
                    
                  </div>
                
                   <div class="form-group">
                       <label for="image">Image</label>
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" id="catimage" name="catimage">
                      <label class="custom-file-label" for="catimage">Choose file</label>
                    </div>
                  </div>
                  <input type="submit" class="btn btn-success" value="create" style="margin-top: 10px;">
                {% else %}  
                {% if editcat|length > 0 %}

                  <input type="hidden" id="hcatid" name="hcatid" value="{{editcat._ID}}">
                  <input type="hidden" id="_catname" name="_catname" value="{{editcat._Name}}">
                 <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text"  class="form-control" id="catname" name="catname" placeholder="Enter Name" value="{{editcat._Name}}" >
                  </div>
                   <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" id="catdes" name="catdes" placeholder="Enter Description" cols="3">{{editcat._Description}}</textarea>
                  </div>
                
                   <div class="form-group">
                    <label for="image">Image</label>
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" id="catimage" name="catimage" >
                      <label class="custom-file-label" for="catimage">Choose file</label>
                    </div>
                    
                    {% if(editcat._Image)!=""%}
                    <img src="{{base_url()}}assets/uploads/category/{{editcat._Image}}" style="width: 80px;height: 80px;border-radius: 50px;">
                    {% endif %}

                  </div>
                  <input type="submit" class="btn btn-success" value="save" style="margin-top: 10px;">

                  {% endif %}
                  
                  {% endif %}
              </form>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  {% endblock %}
   {% block scripts %}
  <script src="{{ constant('cmstheme') }}js/category.js"></script>
   {% endblock %}

