{% extends 'admin/app/index.php' %}

{% block content %} 

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    	<!-- Content Header (Page header) -->
    	<section class="content-header">
      		<div class="container-fluid">
			<div class="row mb-2">
	          		<div class="col-sm-6">
	             		<h1>Edit Gallery</h1> 
	          		</div>
		          	<div class="col-sm-6">
			            	<ol class="breadcrumb float-sm-right">
			              		<li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
			              		<li class="breadcrumb-item active">Edit Gallery</li>
			            	</ol>
		          	</div>
        		</div>
      		</div><!-- /.container-fluid -->
    	</section>
      <!-- Main content -->
    	<section class="content">
      	  <div class="container-fluid">

       	   <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Gallery Details</h3>
          </div>


          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">

  <div class="col-sm-12">
                        <div class="form-group">
                          <div class="panel-body">
                                <form action="./" method="POST" enctype="multipart/form-data" >
                                    <input type="hidden" value="{{data._ID}}" name="gcid" id="gcid" />
                                    <div class="fallback">
                                          <div id="myDropzone" class="dropzone"></div>
                                    </div>
                                </form>
                          </div>
                          <div class="panel-footer">
                            <div class="row" align="center"> 
                              <div class="col-sm-6 pull-right">
                                <button name="ucstsubmit" class="btn btn-primary mr5" id="dropzoneSubmit">Upload</button> 
                              </div>
                              <div class="col-sm-6">
                                <a href="product_view.php" class="btn btn-dark">Cancel</a>
                              </div>
                            </div>
                        </div>
                        </div><!-- form-group -->
              </div>
         <!-- /.form -->
          </div>
          <!-- /.card-body -->
        </div>
      </div>
        <!-- /.card -->    

        <!-- /.row -->            	
  	</section>
</div>
  <!-- /.content-wrapper -->

  {% endblock %}

  {% block scripts %}
  <script src="{{ constant('cmstheme') }}js/gallery.js"></script>
  <script src="{{ constant('cmstheme') }}js/dropzone.min.js"></script>

  <script>

    Dropzone.autoDiscover = false;
    // init dropzone on id (form or div)
    $(document).ready(function() {
       
        var myDropzone = new Dropzone("#myDropzone", {
            url:base_url+"admin/gallery/editImage",
            method: "POST",
            paramName: "proImage",
            autoProcessQueue : false,
            acceptedFiles: "image/*",
            maxFiles: 10,
            maxFilesize: 128, // MB
            uploadMultiple: true,
            parallelUploads: 100, // use it with uploadMultiple
            createImageThumbnails: true,
            thumbnailWidth: 120,
            thumbnailHeight: 120,
            addRemoveLinks: true,
            timeout: 180000,
            dictRemoveFileConfirmation: "Are you Sure?",
            dictFileTooBig: "File is to big ({{filesize}}mb). Max allowed file size is {{maxFilesize}}mb",
            dictInvalidFileType: "Invalid File Type",
            dictCancelUpload: "Cancel",
            dictRemoveFile: "Remove",
            dictMaxFilesExceeded: "Only {{maxFiles}} files are allowed",
            dictDefaultMessage: "Drop files here to upload",
        });
      
  {% if data._Image != ''%}
              {% for edit_img in data._Image %}
                {% set name= "assets/uploads/gallery/"~edit_img.name %}
                     {% set thumburl =  base_url()~'assets/uploads/gallery/'~edit_img.name %}
            
                var mockFile = { 
                  name: '{{ name }}', 
                  mainname: '{{edit_img.name }}'  
                  
                };
            myDropzone.emit('addedfile', mockFile);
                myDropzone.emit('thumbnail', mockFile, "{{ thumburl }}");
            myDropzone.emit("complete", mockFile);

              {% endfor %}
    {% endif %}

          
      });
 
    Dropzone.options.myDropzone = {
        // The setting up of the dropzone
        init: function() {
            var myDropzone = this;
           // save(myDropzone);
            $("#dropzoneSubmit").on("click", function(e) {
                e.preventDefault();
                e.stopPropagation();
                
                if (myDropzone.files != "") {
                    myDropzone.processQueue();
                } else {
                    $("#myDropzone").submit();
                }
            });
            
            this.on("addedfile", function(file) {
            });
            this.on("error", function(file, response) {
            });

      myDropzone.on("removedfile", function(file) {
                $.ajax({
                      url:base_url+"admin/gallery/deleteImage",
                      method:"POST",
                      data:{mainname : file.mainname ,name: file.name, gallery_id:"{{data._ID}}", action: 'removefile'},
                      success: function(response){
                        ecommerce.notifyWithtEle(response.msg , response.type ,'topRight', 2000);
                      }
                });
                return false;
      });

            this.on("sendingmultiple", function(file, xhr, formData) {
                formData.append("gcid", $("#gcid").val());
            });

            this.on("successmultiple", function(file, response) {
              ecommerce.notifyWithtEle(response.msg , response.type ,'topRight', 2000);
            });
        }
    };
   
  /*  });*/

</script>
  
   {% endblock %}


