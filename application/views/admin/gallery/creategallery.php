{% extends 'admin/app/index.php' %}

{% block content %} 

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    	<!-- Content Header (Page header) -->
    	<section class="content-header">
      		<div class="container-fluid">
			<div class="row mb-2">
	          		<div class="col-sm-6">
	             		<h1>Edit Gallery</h1> 
	          		</div>
		          	<div class="col-sm-6">
			            	<ol class="breadcrumb float-sm-right">
			              		<li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
			              		<li class="breadcrumb-item active">Edit Gallery</li>
			            	</ol>
		          	</div>
        		</div>
      		</div><!-- /.container-fluid -->
    	</section>
      <!-- Main content -->
    	<section class="content">
      	  <div class="container-fluid">

       	   <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Gallery Details</h3>
          </div>


          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">

		<div class="col-sm-12">
            <div class="form-group">
                <div class="panel-body">
                    <form method="POST" enctype="multipart/form-data" name="gallery_form" id="gallery_form">
                        <p><button class="add_fields btn btn-info">Add More Fields</button></p>
                         <div class="form-group">
                            <label>Title</label>
                                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title">     
                         </div>
   
                        <div class="wrapper">
                         <div class="form-group">
                          <label>Name</label>
                            <input type="text" class="form-control" id="url[]" name="url[]" placeholder="Enter Name" >     
                            <a href="javascript:void(0);" class="remove_field">Remove</a>
                         </div>
                        </div>

                        <input type="submit" name="ucstsubmit" class="btn btn-primary mr5" id="dropzoneSubmit" value="Add">
                    </form>
                </div>
            </div>
        </div>
         <!-- /.form -->
          </div>
          <!-- /.card-body -->
        </div>
      </div>
        <!-- /.card -->    

        <!-- /.row -->            	
  	</section>
</div>
  <!-- /.content-wrapper -->

  {% endblock %}

  {% block scripts %}
  <script src="{{ constant('cmstheme') }}js/gallery.js"></script>
  

    <script type="text/javascript">

        $(document).ready(function() {
            var wrapper    = $(".wrapper"); 
            var add_button = $(".add_fields"); 
            var x = 1; 

            $(add_button).click(function(e){
                e.preventDefault();
     
                $('div.wrapper:last').append('<div><div class="col-sm-12"><div class="form-group"><input type="text" name="url[]" id="url" class="form-control"/></div></div>  <a href="javascript:void(0);" class="remove_field">Remove</a></div>');
            });

                $(wrapper).on("click",".remove_field", function(e){ 
                    e.preventDefault();
                    $(this).parent('div').remove(); 
                    x--; 
                })
            });

    </script>
   {% endblock %}


