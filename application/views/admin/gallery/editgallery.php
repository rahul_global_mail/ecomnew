{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                    <div class="col-sm-6">
                            <h1>Edit Gallery</h1> 
                    </div>
                    <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
                                <li class="breadcrumb-item active">Edit Gallery</li>
                            </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

      <!-- Main content -->
        <section class="content">
          <div class="container-fluid">

           <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div style="margin:20px;">
              <a class="btn btn-info" href="{{base_url('catgallery/')~galedit._ID}}">Gallery</a>
              <a class="add_fields btn btn-primary">Add More</a>
              <hr>
          </div>
          <!-- <div class="card-header">
            <h3 class="card-title">Basic Details</h3>
          </div> -->
          <!-- /.card-header -->
          <div class="card-body">
            <form id="editgallery_form" name="editgallery_form" method="post">

            <input type="hidden" value="{{galedit._ID}}" name="gid" id="gid" /> 
            
              <div class="col-md-12">
                <div class="form-group">

                    <label>Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="{{galedit._Title}}">                                                                   
                </div>
             </div>

             <div class="col-md-12">
                 {% if galedit._Url != ''%}
                    {% for edit_url in galedit._Url %}
                             
                <div class="wrapper">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" id="url[]" name="url[]" placeholder="Enter Name" value="{{edit_url}}" >  
                    <a href="javascript:void(0);" class="remove_field">Remove</a>                                                                    
                </div>
             </div>

             {% endfor %}
    {% endif %}
                <!-- /.form-group -->
              
              </div>
              <!-- /.col -->
              
              <!-- /.col -->
            <!-- /.row -->

            <!-- /.row -->
           <input type="submit" class="btn btn-success" value="save" style="margin-top: 10px;">
           
        </form>
         <!-- /.form -->
          </div>
          <!-- /.card-body -->
        </div>
      </div>
        <!-- /.card -->    

        <!-- /.row -->              
    </section>
</div>
  <!-- /.content-wrapper -->

  {% endblock %}

  {% block scripts %}
  <script src="{{ constant('cmstheme') }}js/gallery.js"></script>
  <script type="text/javascript">

        $(document).ready(function() {
            var wrapper    = $(".wrapper"); 
            var add_button = $(".add_fields"); 
            var x = 1; 

            $(add_button).click(function(e){
                e.preventDefault();
     
                $('div.wrapper:last').append('<div><div class="col-sm-12"><div class="form-group"><input type="text" name="url[]" id="url" class="form-control"/></div></div>  <a href="javascript:void(0);" class="remove_field">Remove</a></div>');
            });

                $(wrapper).on("click",".remove_field", function(e){ 
                    e.preventDefault();
                    $(this).parent('div').remove(); 
                    x--; 
                })
            });

    </script>
   {% endblock %}


