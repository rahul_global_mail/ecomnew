{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1>Gallery</h1> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
              <li class="breadcrumb-item active">Gallery</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card card-default">
               <div class="card-header">
                <div class="card-tools">
                  <a href="{{base_url('creategallery')}}" class="btn  btn-block bg-gradient-primary pull-right"><i class="fas fa-plus"></i>Add More</a>
                </div>
              </div>
           
            <div class="card-body">   
              <table id="coupon_tab" class="table table-bordered table-hover" >
                <thead>
                <tr>

                   <th>{% if galldetail|length > 0 %}
              
            <button class="btn btn-danger" name="delete_all" id="delete_all" class="glyphicon glyphicon-pencil"><i class="fas fa-user-slash"></i></button>
            {% endif %}</th>

                  <th>No</th>
                  <th>Title</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                  {% if galldetail|length > 0 %}

                
                  {% set i = 1 %}
                    {% for gdata in galldetail %}
                    
                <tr>
                  <td><input type="checkbox" class="delete_checkbox" name="checkuser[]" id="checkuser" value="{{gdata._ID}}"></td>
                  <td>{{ i }}</td>
                  <td>{{gdata._Title}}</td>
                 
                  <td> 
                    <a href="{{ base_url('editgallery/'~gdata._ID) }}" class="btn btn-warning""><i class="fas fa-edit"></i></a>
                  </td>
                </tr>
                
                  {% set i = i + 1 %}
                 {% endfor %}
                
               {% endif %}
                </tbody>
              
            </table>
        
            </div>
            <!-- /.card-body -->
          </div>
        
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  {% endblock %}
  {% block scripts %}
  <script src="{{ constant('cmstheme') }}js/gallery.js"></script>
  {% endblock %}
