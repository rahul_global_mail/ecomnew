{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

      <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Order</h1>
          </div>
           <div class="col-sm-6">
           <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
           <li class="breadcrumb-item active">Order</li>
         </ol>
       </div> 
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card  card-default">
            
            <div class="card-body">
              <table id="order_tab" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>User Name</th> 
                  <th>Order ID</th>
                  <th>Order Status</th>
                  <th>Payment Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  {% if count(data) > 0 %}
                
                  {% set i = 1 %}
                  {% for odata in data %}
                    
                    {% if odata._Paymentstatus == '0' %}
                      {% set pstatus = 'Pending' %}
                    {% elseif odata._Paymentstatus == '1' %}
                      {% set pstatus = 'Paid' %}
                    {% elseif odata._Paymentstatus == '2' %}
                      {% set pstatus = 'Refund' %}
                    {% endif %}

                    {% if odata._Orderstatus == '0' %}
                      {% set ostatus = 'Pending' %}
                    {% elseif odata._Orderstatus == '1' %}
                      {% set ostatus = 'Approved' %}
                    {% elseif odata._Orderstatus == '2' %}
                      {% set ostatus = 'Shipping' %}
                    {% elseif odata._Orderstatus == '3' %}
                      {% set ostatus = 'Delieverd' %}
                    {% elseif odata._Orderstatus == '4' %}
                      {% set ostatus = 'Cancelled' %}
                    {% endif %}

                <tr>
                  <td>{{i}}</td>
                  <td>{{odata.users._Firstname}}</td>
                  <td>{{odata._Orderid}}</td>
                  <td>{{pstatus}}</td>
                  <td>{{ostatus}}</td>
                  <th>
                    <a href="{{ base_url('vieworderdetails/'~odata._ID) }}" class="btn btn-primary"><i class="fas fa-eye"></i></a>
                    <a href="{{ base_url('invoice/'~odata._ID) }}" class="btn btn-info">Invoice</a>
                  </th>
                </tr>
                
                  {% set i = i + 1 %}
                 {% endfor %}
                
               {% endif %}
                </tbody>
              
            </table>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

  
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
</section>

  </div>
  <!-- /.content-wrapper -->

  {% endblock %}

  {% block scripts %}
    <script type="text/javascript">
        ecommerce._payment();
        ecommerce._delpayment();
    </script>
    {% endblock %}
