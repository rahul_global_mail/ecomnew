{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.js"></script>
  <div class="content-wrapper">

      <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Invoice</h1>
          </div>
           <div class="col-sm-6">
           <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
           <li class="breadcrumb-item active">Invoice</li>
         </ol>
       </div> 
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="callout callout-info">
              <h5><i class="fas fa-info"></i> Note:</h5>
              This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
            </div>


            <!-- Main content -->
            <div class="invoice p-3 mb-3" id="invoice11">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <img class="img-size-50 mr-3 img-circle" src="assets/uploads/store/{{admin_data._Logo}}"> {{admin_data._Name}}, Inc.
                    
                    <small class="float-right">Date: {{ "now"|date("m/d/Y") }}</small>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  From
                  <address>
                    <strong>{{admin_data._Name}}</strong><br>
                    {{admin_data._Address}}<br>
                    Phone: {{admin_data._mobile}}<br>
                    Email: {{admin_data._Email}}
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  To
                  <address>
                    <strong>{{order_data[0].users._Firstname~" "~order_data[0].users._Lastname}}</strong><br>
                    {% for useraddr in order_data[0].users._Address %}
                        {{useraddr}}
                        <br>
                    {% endfor %}
                    Phone: {{order_data[0].users._Mobile}}<br>
                    Email: {{order_data[0].users._Email}}
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  <b>Invoice {{order_data[0]._Invoiceid}}</b><br>
                  <br>
                  <b>Order ID:</b> {{order_data[0]._Orderid}}<br>
                  <b>Payment Date:</b> {{order_data[0]._Orderdate}}<br>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>Product</th>
                      <th>Qty</th>
                      <th>Price</th>
                      <th>Subtotal</th>
                    </tr>
                    </thead>
                    <tbody>
                    {% if count(order_data[0]._Productdetail) > 0 %}
                      {% set i = 1 %}
                      {% set j = 0 %}
                      {% for key,odata in order_data[0]._Productdetail %}
                        <tr>
                          <td>{{i}}</td>
                          <td>{{order_data[0].product[j]._Name}}</td>
                          <td>{{odata.quantity}}</td>
                          <td>{{odata.price}}</td>
                          <td>{{odata.total}}</td>
                        </tr>
                         {% set i = i + 1 %}
                         {% set j = j + 1 %}
                         {% endfor %}
                       {% endif %}
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                <!-- /.col -->
                <div class="col-12">
                  <p class="lead">Amount </p>

                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <th style="width:50%">Subtotal:</th>
                        <td>{{order_data[0]._Total}}</td>
                      </tr>
                      <tr>
                        <th>Tax (10%) Fix</th>
                        <td>
                          {% set tax = order_data[0]._Total * 0.1 %}
                          {{tax}}
                        </td>
                      </tr>
                      <tr>
                        <th>Shipping:</th>
                        <td>5</td>
                      </tr>
                      <tr>
                        <th>Total:</th>
                        <td>
                          {% set finaltotal = order_data[0]._Total + tax + 5  %}
                          {{finaltotal}}
                        </td>
                      </tr>
                    </table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

           
            </div>
            <!-- /.invoice -->
               <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-12">
                  <button type="button" class="btn btn-primary float-right" id="generate_pdf" style="margin-right: 5px;">
                    <i class="fas fa-download"></i> Generate PDF
                  </button>
                </div>
              </div>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

  </div>
  <!-- /.content-wrapper -->

  {% endblock %}

  {% block scripts %}

    <script type="text/javascript">
        ecommerce._payment();
        ecommerce._delpayment();

        $('#generate_pdf').click(function() {
            const invoice = document.getElementById("invoice11");
            console.log(invoice);
            console.log(window);
            var opt = {
                margin: 1,
                filename: 'myfile.pdf',
                image: { type: 'jpeg', quality: 0.98 },
                html2canvas: { scale: 2 },
                jsPDF: { unit: 'in', format: 'letter', orientation: 'portrait' }
            };
            html2pdf().from(invoice).set(opt).save();

        });
    </script>
    {% endblock %}
