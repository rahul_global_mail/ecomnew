{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

      <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Order Details</h1>
          </div>
           <div class="col-sm-6">
           <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
           <li class="breadcrumb-item active">Order Details</li>
         </ol>
       </div> 
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card  card-default">

            <div class="card-body">
              <form  id="order_form" method="post">
              <table class="table">
                <tr>
                  <td>Order ID : </td>
                  <td>{{data[0]._Orderid}}</td>
                </tr>
                <tr>
                  <td>User Name : </td>
                  <td>{{data[0].users._Firstname}}</td>
                </tr>
                <tr>
                  <td>Payment Getway : </td>
                  <td>{{data[0].payment._Name}}</td>
                </tr>
                <tr>
                  <td>Payment Status : </td>
                  <td>
                    <input type="hidden" name="id" value="{{data[0]._ID}}">
                    <select id="payment_status" class="form-control" name="payment">
                      <option value="0" {% if data[0]._Orderstatus == 0 %}selected="selected"{% endif %}>Pending</option>
                      <option value="1" {% if data[0]._Orderstatus == 1 %}selected="selected"{% endif %}>Paid</option>
                      <option value="2" {% if data[0]._Orderstatus == 2 %}selected="selected"{% endif %}>Refund</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>Order Status : </td>
                  <td>
                     <select id="order_status" class="form-control" name="order" >
                      <option value="0" {% if data[0]._Paymentstatus == 0 %}selected="selected"{% endif %}>Pending</option>
                      <option value="1" {% if data[0]._Paymentstatus == 1 %}selected="selected"{% endif %}>Approved</option>
                      <option value="2" {% if data[0]._Paymentstatus == 2 %}selected="selected"{% endif %}>Shipping</option>
                      <option value="3" {% if data[0]._Paymentstatus == 3 %}selected="selected"{% endif %}>Delieverd</option>
                      <option value="4" {% if data[0]._Paymentstatus == 4 %}selected="selected"{% endif %}>Cancelled</option>
                    </select>
                  </td>

                </tr>
                <tr>
                  <td>Date Time : </td>
                  <td>{{data[0]._Created}}</td>
                </tr>
                <tr>
                  <td>Order Date Time : </td>
                  <td>{{data[0]._Orderdate}}</td>
                </tr>
              </table>
              <input type="submit" class="btn btn-success save_status" value="Save" style="margin-top: 10px;">
                 </form>
            </div>
            
            <div class="card-body">
              <h4>Products : </h4>
              <table class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>Product Name</th> 
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Total</th>
                </tr>
                </thead>
                <tbody>

                  {% if data[0]._Productdetail|length > 0 %}

                  {% set i = 1 %}
                  {% set j = 0 %}
                  {% for key,odata in data[0]._Productdetail %}
                    
                <tr>
                  <td>{{i}}</td>
                  <td>{{data[0].product[j]._Name}}</td>
                  <td>{{odata.price}}</td>
                  <td>{{odata.quantity}}</td>
                  <td>{{odata.total}}</td>
                </tr>
                
                  {% set i = i + 1 %}
                  {% set j = j + 1 %}
                 {% endfor %}
                
               {% endif %}
                </tbody>
              
            </table>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

  
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
</section>
 
  </div>
  <!-- /.content-wrapper -->

  {% endblock %}

  {% block scripts %}
  <script src="{{ constant('cmstheme') }}js/order.js"></script>
    {% endblock %}
