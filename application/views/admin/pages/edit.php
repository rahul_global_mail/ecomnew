{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    	<!-- Content Header (Page header) -->
    	<section class="content-header">
      		<div class="container-fluid">
			<div class="row mb-2">
	          		<div class="col-sm-6">
	             			<h1>{{pagetitle}}</h1> 
	          		</div>
		          	<div class="col-sm-6">
			            	<ol class="breadcrumb float-sm-right">
			              		<li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
			              		<li class="breadcrumb-item active">category</li>
			            	</ol>
		          	</div>
        		</div>
      		</div><!-- /.container-fluid -->
    	</section>

      <!-- Main content -->
    	<section class="content">
      		<div class="row">
        		<div class="col-12">
          			<div class="card">
            				<div class="card-body">
               					<div class="row">
              						<div class="col-md-12">              
              							<form class="form-horizontal"  id="pages_form" name="pages_form" method="post">
                						{% if editid is not defined %}
						                   	<div class="form-group row">
						                    		<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Title</label>
						                    		<div class="col-sm-11">
						                    			<input type="text"  class="form-control" id="title" name="title" placeholder="Enter Name" >
						                    		</div>
						                  	</div>
                   							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Content</label>
							                    	<div class="col-sm-11">
                										<textarea class="textarea" placeholder="Place some text here" name="content" id="content" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
             										 </div>
                  							</div>
                  							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Status</label>
							                    	<div class="col-sm-11">
								                    	 <div class="custom-control custom-switch">
										                      <input type="checkbox" class="custom-control-input" id="status" name="status">
										                      <label class="custom-control-label" for="status"></label>
                    									</div>
							                    	</div>
                  							</div>

                  							<input type="submit" class="btn btn-success" value="Submit" style="margin-top: 10px;">
							        {% else %}  

							        {% if editpages|length > 0 %}

          								<input type="hidden" id="hpid" name="hpid" value="{{editpages._ID}}">
                 							  	<div class="form-group row">
						                    		<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Title</label>
						                    		<div class="col-sm-11">
						                    			<input type="text"  class="form-control" id="title" name="title" placeholder="Enter Name" value="{{editpages._Title}}" >
						                    		</div>
						                  	</div>
                   							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Content</label>
							                    	<div class="col-sm-11">
                										<textarea class="textarea" placeholder="Place some text here" name="content" id="content" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{editpages._Content}}</textarea>
             										 </div>
                  							</div>
                  							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Status</label>
							                    	<div class="col-sm-11">
								                    	 <div class="custom-control custom-switch">
										                      <input type="checkbox" class="custom-control-input" id="status" name="status" {% if editpages._Status == 1 %} checked {% endif %}>
										                      <label class="custom-control-label" for="status"></label>
                    									</div>
							                    	</div>
                  							</div>
          								<input type="submit" class="btn btn-success" value="Update" style="margin-top: 10px;">
	                  					{% endif %}
                  						{% endif %}
      								</form>
                  					</div>
                  				</div>
                  			</div>
                  		</div>
                  	</div>
		</div>                  	
  	</section>
</div>
  <!-- /.content-wrapper -->

  {% endblock %}

{% block scripts %}
	<script src="{{ constant('cmstheme') }}js/pages.js"></script>
  {% endblock %}