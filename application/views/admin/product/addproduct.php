{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    	<!-- Content Header (Page header) -->
    	<section class="content-header">
    		<div class="container-fluid">
		       <div class="row mb-2">
          		<div class="col-sm-6">
             			<h1>Add Product</h1> 
          		</div>
	          	<div class="col-sm-6">
		            	<ol class="breadcrumb float-sm-right">
		              		<li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
		              		<li class="breadcrumb-item active">Add Product</li>
		            	</ol>
	          	</div>
      		</div>
    		</div><!-- /.container-fluid -->
    	</section>

      <!-- Main content -->
    	<section class="content">
        <form id="addproduct_form" name="addproduct_form" method="post">
      	  <div class="container-fluid">
       	   <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Basic Details</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6 float-left">
                <div class="form-group">
                  <label>Name</label>
             			<input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" >                    		
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Description</label>
                  <textarea class="form-control" rows="3" placeholder="Enter Description" name="description" id="description"></textarea>
                </div>

                <!-- /.form-group -->
                 <div class="form-group"> 
                  <label>Status</label>
                    <div class="custom-control custom-switch">
                      <input type="checkbox" class="custom-control-input" id="status" name="status">
                      <label class="custom-control-label" for="status"></label>
                    </div>
                 
                </div>
              
              </div>
              <!-- /.col -->
              <div class="col-md-6 float-left">
                <div class="form-group">
                  <label>Category</label>
                  <select class="form-control" name="category" id="category" style="width: 100%;">
                    <option value="">Select Category</option>
                      {% if count(catdata) > 0 %}
              					{% for cdata in catdata %}
              						<option value="{{cdata._ID}}">{{cdata._Name}}</option>
              					{% endfor %}
                        {% endif %} 
                  </select>
                </div>
                <!-- /.form-group -->
                <div class="form-group" style="display: none;"   id="subcategory" >
                </div>
                  <div class="form-group">
                  <label>Special</label>
                  <select class="form-control" name="special" id="special" stysle="width: 100%;">
                    <option value="">Please select</option>
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                  </select>
                </div>
         </div>
          </div>
        </div>

        <div class="card card-default" id="attrdetail">
          <div class="card-header">
            <h3 class="card-title">Attribute Details</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row" >
              <div class="col-md-6 float-left">
                <div class="form-group">
                  <label>Size</label>
                  <input type="text" class="form-control" id="size" name="size[]" placeholder="Enter Size" >
                </div>
                <div class="form-group">
                  <label>Color</label>
                   <input type="text" class="form-control" id="color" name="color[]" placeholder="Enter Color" >
                </div>
                <div class="form-group">
                  <label>Quantity</label>
                   <input type="text" class="form-control" id="quantity" name="quantity[]" placeholder="Enter quantity" >
                </div>
              </div>
              <div class="col-md-6 float-right">
                <div class="form-group">
                  <label>Price</label>
                  <input type="text" class="form-control" id="price" name="price[]" placeholder="Enter Price" >
                </div>
                <div class="form-group">
                  <label>Sell Price</label>
                  <input type="text" class="form-control" id="sellprice" name="sellprice[]" placeholder="Enter Price" >                       
                </div>
                <div class="form-group">
                  <label>Default Product</label>
                  <select name="default[]" class="form-control">
                    <option value="">Select Default Product</option>
                    <option value="1" >Yes</option>
                    <option value="0" >No</option>
                  </select>                    
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="att-body"></div>
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-body">
                <div class="form-group">
                  <button type="button" class="btn btn-primary" id="addmore">Add More Attribute</button>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-body">
                <div class="form-group">
                  <input type="submit" class="btn btn-success" value="Submit"/>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>   
        </div>   
      </form>
        <!-- /.row -->            	
  	</section>
</div>
  <!-- /.content-wrapper -->

  {% endblock %}

  {% block scripts %}
  <script src="{{ constant('cmstheme') }}js/product.js"></script>
   {% endblock %}


