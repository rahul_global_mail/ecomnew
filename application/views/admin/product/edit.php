{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    	<!-- Content Header (Page header) -->
    	<section class="content-header">
      		<div class="container-fluid">
			<div class="row mb-2">
	          		<div class="col-sm-6">
	             			<h1>{{pagetitle}}</h1> 
	          		</div>
		          	<div class="col-sm-6">
			            	<ol class="breadcrumb float-sm-right">
			              		<li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
			              		<li class="breadcrumb-item active">Product</li>
			            	</ol>
		          	</div>
        		</div>
      		</div><!-- /.container-fluid -->
    	</section>

      <!-- Main content -->
    	<section class="content">
      	  <div class="container-fluid">
       	   <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Basic Details</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
              	<form class="form-horizontal"  id="product_form" name="product_form" method="post">
                  {% if editid is not defined %}
                <div class="form-group">
                  <label>Name</label>
             			<input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" >                    		
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Description</label>
                  <textarea class="form-control" rows="3" placeholder="Enter Description" name="description" id="description"></textarea>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>Category</label>
                  <select class="form-control" name="category" id="category" style="width: 100%;">
                    <option value="">Select Category</option>
                      {% if count(catdata) > 0 %}
              					{% for cdata in catdata %}
              						<option value="{{cdata._ID}}">{{cdata._Name}}</option>
              					{% endfor %}
                        {% endif %} 
                  </select>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Sub category</label>
                  <select class="form-control" name="subcategory" id="subcategory" style="width: 100%;">
                    <option value="">Select Sub Category</option>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <div class="col-12 col-sm-6">
                <div class="form-group">
                  <label>Special</label>
                  <select class="form-control" name="special" id="special" stysle="width: 100%;">
                    <option value="">Please select</option>
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-12 col-sm-6">
                <div class="form-group">
                  <label>Status</label>
                    <div class="custom-control custom-switch">
                      <input type="checkbox" class="custom-control-input" id="status" name="status">
                      <label class="custom-control-label" for="status"></label>
                    </div>
                 
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
           <input type="submit" class="btn btn-success" value="Submit" style="margin-top: 10px;">
           {% else %}  
            {% if count(product) > 0 %}
                <input type="hidden" id="productedit" value="1" />
                 <input type="hidden" id="subcatedit" value= "{{product._SubcatID}}"/>
                  <input type="hidden" id="hpid" name="hpid" value= "{{product._ID}}"/>
                  <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="{{product._Name}}" >                        
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Description</label>
                  <textarea class="form-control" rows="3" placeholder="Enter Description" name="description" id="description">{{product._Des}}
                  </textarea>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>Category</label>
                  <select class="form-control" name="category" id="category" style="width: 100%;">
                    <option value="">Select Category</option>
                      {% if count(catdata) > 0 %}
                        {% for cdata in catdata %}
                        {% set selected = '' %}
                          {% if cdata._ID == product._CatID %}
                                   {% set selected = 'selected' %}
                          {% endif %}
                          <option value="{{cdata._ID}}" {{ selected }}>{{cdata._Name}}</option>
                        {% endfor %}
                        {% endif %} 
                  </select>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Sub category</label>
                  <select class="form-control" name="subcategory" id="subcategory" style="width: 100%;">
                    <option value="">Select Sub Category</option>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <div class="col-12 col-sm-6">
                <div class="form-group">
                  <label>Special</label>
                  <select class="form-control" name="special" id="special" stysle="width: 100%;">
                    <option value="">Please select</option>
                    <option value="0" {% if product._Special == 0 %} selected {% endif %}>No</option>
                    <option value="1" {% if product._Special == 1 %} selected {% endif %}>Yes</option>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-12 col-sm-6">
                <div class="form-group">
                  <label>Status</label>
                    <div class="custom-control custom-switch">
                      <input type="checkbox" class="custom-control-input" id="status" name="status" {% if product._Status == 1 %} checked {% endif %}>
                      <label class="custom-control-label" for="status"></label>
                    </div>
                 
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
           <input type="submit" class="btn btn-success" value="Submit" style="margin-top: 10px;">
           {% endif %}


           {% endif %}

        </form>
         <!-- /.form -->
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->     

         <!-- SELECT2 EXAMPLE -->
         {% if editid is defined %}
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Attribute Details</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <form class="form-horizontal"  id="proattribute_form" name="proattribute_form" method="post">
                  
                  {% if count(attdetail) > 0 %}
                  <div class="form-group">
                  <label>Attribute:</label>
                   <select class="form-control" name="proattribute" id="proattribute" style="width: 100%;">
                    <option value="">Select Attribute</option>
                      {% if count(attribute) > 0 %}
                        {% for data in attribute %}
                          <option value="{{data._ID}}"{% if attdetail._AttID == data._ID %} selected {% endif %}>{{data._Name}}</option>
                        {% endfor %}
                        {% endif %} 
                  </select>
                </div>
                <input type="hidden" id="hpid" name="hpid" value= "{{product._ID}}"/>
                <!-- /.form-group -->
                <div class="wrapper1">
                 <div class="row"> 
                      
                   <div class="col-md-4 form-group">
                  <div id="attval">
                    
                  </div> 
                   </div> 
                   <div class="col-md-4 form-group">
                     
                    <div id="price"></div>
                 </div>
                   <div class="col-md-4 form-group">
                     
                    <div id="quantity"></div>
                   </div>
                 </div>
                 
               </div>
            
             <input type="submit" class="btn btn-success" value="Submit" style="margin-top: 10px;">


                  {% else %}
                  <h2>welcome</h2>

                   <div class="form-group">
                  <label>Attribute:</label>
                   <select class="form-control" name="proattribute" id="proattribute" style="width: 100%;">
                    <option value="">Select Attribute</option>
                      {% if count(attribute) > 0 %}
                        {% for data in attribute %}
                          <option value="{{data._ID}}">{{data._Name}}</option>
                        {% endfor %}
                        {% endif %} 
                  </select>
                </div>
                <input type="hidden" id="hpid" name="hpid" value= "{{product._ID}}"/>
                <!-- /.form-group -->
                 <div class="wrapper1">
                 <div class="row"> 
                   <div class="col-md-4">
                  <div id="attval"></div> 
                   </div> 
                   <div class="col-md-4 form-group">
                    <div id="price"></div>
                 </div>
                   <div class="col-md-4 form-group">
                    <div id="quantity"></div>
                   </div>
                 </div>
               </div>
             </div>
             <input type="submit" class="btn btn-success" value="Submit" style="margin-top: 10px;">
                   {% endif %}
                     
                  
               
              <!--      <select class="form-control" name="proattribute" id="proattribute" style="width: 100%;">
               <option value="">Select Attribute</option>
                 {% if count(attribute) > 0 %}
                   {% for data in attribute %}
                     <option value="{{data._ID}}">{{data._Name}}</option>
                   {% endfor %}
                   {% endif %} 
                                </select> -->
                <!-- </div> -->
                   
              </div>
                
              </div>
              <!-- /.col -->
           
            </form>
             <!-- /.form -->

            </div>
            <!-- /.row -->
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
        {% endif %}

         <!-- SELECT2 EXAMPLE -->
         {% if editid is defined %}
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Product Details</h3>
          </div>

           
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
                <button class="add_fields btn btn-info pull-right"><i class="fas fa-plus"></i> Add More</button>
              <div class="col-md-12">
                <form name="speci_form" id="speci_form" method="post">
                <input type=hidden id="hpid" name="hpid" value= "{{product._ID}}"/>
                <div style="margin-top: 10px;" class="wrapper">
                            <div class="col-md-6 float-left">
                              <div class="form-group">
                                <input type="text" class="form-control " name="property[]"  placeholder="Enter Property" >
                              </div>
                            </div>
                            <div class="col-md-6 float-left">
                              <div class="form-group">
                                <input type="text" class="form-control " name="value[]" placeholder="Enter Values" >
                              </div>
                            </div>
                          <a href="javascript:void(0);" class="remove_field">Remove</a>
                        
              
            </div>
                 <input type="submit" class="btn btn-success" value="Submit" style="margin-top: 10px;">
              </form>
             <!-- /.form -->

            </div>
            <!-- /.row -->
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->



        </div>
        <!-- /.row -->   
         {% endif %}

        {% if editid is defined %}
         <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Product Gallery</h3>
          </div>

           
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                 <form  method="post" enctype="multipart/form-data" id="gallery_form">
                <input type = "hidden" id="hpid" name="hpid" value= "{{product._ID}}"/>   
                  <div class="form-group">
                    <label for="image">Upload Image</label>
                    <input type="file" name="imageURL[]" id="imageURL" class="filestyle" value="" data-icon="false" data-buttontext="Choose image" multiple="multiple">
                  </div>
                
              </div>
                 <input type="submit" id="gallerysubmit" class="btn btn-success" value="Submit" style="margin-top: 10px;">
              </form>
             <!-- /.form -->

            </div>
            <!-- /.row -->
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

        

        </div>
        {% endif %}
        <!-- /.row -->             	
  	</section>
</div>
  <!-- /.content-wrapper -->

  {% endblock %}

  {% block scripts %}
  <script src="{{ constant('cmstheme') }}js/product.js"></script>
   {% endblock %}
