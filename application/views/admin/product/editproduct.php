{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    	<!-- Content Header (Page header) -->
    	<section class="content-header">
      		<div class="container-fluid">
			<div class="row mb-2">
	          		<div class="col-sm-6">
	             			<h1>Edit Product</h1> 
	          		</div>
		          	<div class="col-sm-6">
			            	<ol class="breadcrumb float-sm-right">
			              		<li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
			              		<li class="breadcrumb-item active">Edit Product</li>
			            	</ol>
		          	</div>
        		</div>
      		</div><!-- /.container-fluid -->
    	</section>
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card ">
                <div class="card-body">
                  <div class="form-group">
                    <a class="btn btn-info" href="{{base_url('gallery/')~product_data._ID}}">Gallery</a>
                    <a class="btn btn-primary" href="{{base_url('product_adddetail/')~product_data._ID}}">Additional Details</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- Main content -->
    	<section class="content">
      	  <div class="container-fluid">
            <form id="productedit" name="productedit" method="post">
            <input type="hidden" name="pid" id="pid" value="{{product_data._ID}}" />
            <input type="hidden" id="subcatedit" value= "{{product_data._SubcatID}}"/>
            <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Basic Details</h3>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6 float-left">
                    <div class="form-group">
                      <label>Name</label>
                      <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name"  value="{{product_data._Name}}">
                    </div>
                    <div class="form-group">
                      <label>Description</label>
                      <textarea class="form-control" rows="3" placeholder="Enter Description" name="description" id="description">{{product_data._Des}}</textarea>
                    </div>
                     <div class="form-group"> 
                      <label>Status</label>
                        <div class="custom-control custom-switch">
                          <input type="checkbox" class="custom-control-input" id="status" name="status" {% if product_data._Status == 1 %} checked {% endif %}>
                          <label class="custom-control-label" for="status"></label>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-6 float-left">
                    <div class="form-group">
                      <label>Category</label>
                      <select class="form-control" name="category" id="category" style="width: 100%;">
                        <option value="">Select Category</option>
                          {% if count(catdata) > 0 %}
                            {% for cdata in catdata %}
                              <option value="{{cdata._ID}}" {% if product_data._CatID == cdata._ID %} selected="selected" {% endif %}>{{cdata._Name}}</option>
                            {% endfor %}
                            {% endif %} 
                      </select>
                    </div>
                    <div class="form-group" style="display: none;"   id="subcategory" >
                    </div>
                      <div class="form-group">
                      <label>Special</label>
                      <select class="form-control" name="special" id="special" stysle="width: 100%;">
                        <option value="">Please select</option>
                        <option value="0" {% if product_data._Special == 0 %} selected="selected" {% endif %}>No</option>
                        <option value="1" {% if product_data._Special == 1 %} selected="selected" {% endif %}>Yes</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>


          {% if attr_detail | length > 0 %}
          {% for attr in attr_detail %}
          <div class="card card-default" id="attrdetail">
          <div class="card-header">
            <h3 class="card-title">Attribute Details</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row" >
              <div class="col-md-6 float-left">
                <div class="form-group">
                  <label>Size</label>
                  <input type="text" value="{{attr['_Size']}}" class="form-control" id="size" name="size[]" placeholder="Enter Size" >
                </div>
                <div class="form-group">
                  <label>Color</label>
                   <input type="text" value="{{attr['_Color']}}" class="form-control" id="color" name="color[]" placeholder="Enter Color" >
                </div>
                <div class="form-group">
                  <label>Quantity</label>
                   <input type="text" value="{{attr['_Quantity']}}" class="form-control" id="quantity" name="quantity[]" placeholder="Enter quantity" >
                </div>
              </div>
              <div class="col-md-6 float-right">
                <div class="form-group">
                  <label>Price</label>
                  <input type="text" value="{{attr['_Price']}}"  class="form-control" id="price" name="price[]" placeholder="Enter Price" >
                </div>
                <div class="form-group">
                  <label>Sell Price</label>
                  <input type="text" value="{{attr['_Sellprice']}}"  class="form-control" id="sellprice" name="sellprice[]" placeholder="Enter Price" >                       
                </div>
                <div class="form-group">
                  <label>Default Product</label>
                  {% set selected = (attr['_Default'])  %}
                  <select name="default[]" class="form-control">
                    <option value="">Select Default Product</option>
                    <option value="1" {{ (attr['_Default'] == '1') ? 'selected' : '' }} >Yes</option>
                    <option value="0" {{ (attr['_Default'] == '0') ? 'selected' : '' }} >No</option>
                  </select>                    
                </div>
              </div>
              
            </div>
          </div>
        </div>
          {% endfor %}
          {% endif %}
          <div id="att-body"></div>
          <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-body">
                <div class="form-group">
                  <button type="button" class="btn btn-primary" id="addmore">Add More Attribute</button>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-body">
                <div class="form-group">
                  <input type="submit" class="btn btn-success" value="Submit"/>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        </form>
        </div>
        <!-- /.row -->            	
  	</section>
</div>
  <!-- /.content-wrapper -->

  {% endblock %}

  {% block scripts %}
  <script src="{{ constant('cmstheme') }}js/product.js"></script>
   {% endblock %}


