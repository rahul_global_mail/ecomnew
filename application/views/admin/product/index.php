{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1>Product Detail</h1> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
              <li class="breadcrumb-item active">Product</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
             <div class="card-header">
                <div class="card-tools">
                  <a href="{{base_url('addproduct')}}" class="btn  btn-block bg-gradient-primary pull-right"><i class="fas fa-plus"></i> Add Product</a>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              
            
              <table id="product_tab" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>{% if catresult|length > 0 %}
              
                  <button class="btn btn-danger" name="delete_all" id="delete_all" class="glyphicon glyphicon-pencil"><i class="fas fa-user-slash"></i></button>
                  {% endif %}</th>

                  <th>No</th>
                  <th>Name</th>
                  <th>Category</th>
                  <th>Sub Category</th> 
                  <th>Action</th>
                  <th>Review</th>
                </tr>
                </thead>
                <tbody>

                  {% if catresult|length > 0 %}
                  {% set i = 1 %}
                    {% for pdata in catresult %}
                    
                <tr>
                   <td><input type="checkbox" class="delete_checkbox" name="checkuser[]" id="checkuser" value="{{pdata._ID}}"></td>
                  <td>{{ i }}</td>
                  <td>{{pdata._Name}}</td>
                  <td>{{pdata['categories']._Name}}</td>
                  <td>{{pdata['subcategory']._Name}}</td>
                  <td> 
                    <a href="{{ base_url('editproduct/'~pdata._ID) }}" class="btn btn-warning"><i class="fas fa-edit"></i>
                  </td>
                   <td> 
                   <a href="{{ base_url('showreview/'~pdata._ID)}}" class="btn btn-success btn-sm">Review</a> 

                  </td>
                </tr>
                
                  {% set i = i + 1 %}
                 {% endfor %}
                
               {% endif %}
                </tbody>
              
            </table>

            </div>
            <!-- /.card-body -->
          </div>
        
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  {% endblock %}


  {% block scripts %}
  <script src="{{ constant('cmstheme') }}js/product.js"></script>
   {% endblock %}


