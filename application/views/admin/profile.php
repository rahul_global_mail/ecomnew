{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

      <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Profile</h1>
          </div>
           <div class="col-sm-6">
           <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
           <li class="breadcrumb-item active">profile</li>
         </ol>
       </div> 
        </div>
      </div><!-- /.container-fluid -->
    </section>

     <section class="content">
          <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                          <div class="col-md-12">              
                            <form class="form-horizontal"  id="profile_form" name="profile_form" method="post">
                            
                            {% if count(profiledata) > 0 %}
                            <input type="hidden" id="phid" name="phid" value="{{profiledata._ID}}">
                                <div class="form-group row">
                                    <label class="col-sm-1 col-form-label" for="exampleInputEmail1">First Name</label>
                                    <div class="col-sm-11">
                                      <input type="text"  class="form-control" id="fname" name="fname" placeholder="Enter FirstName" value="{{profiledata._Firstname}}" >
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-1 col-form-label" for="exampleInputEmail1">Last Name</label>
                                    <div class="col-sm-11">
                                      <input type="text"  class="form-control" id="lname" name="lname" placeholder="Enter LastName" value="{{profiledata._Lastname}}" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-1 col-form-label" for="exampleInputEmail1">Email</label>
                                    <div class="col-sm-11">
                                      <input type="text"  class="form-control" id="email" name="email" placeholder="Enter Email" value="{{profiledata._Email}}">
                                    </div>
                                </div>
                                 <div class="form-group row">
                                    <label class="col-sm-1 col-form-label" for="exampleInputEmail1">Address</label>
                                    <div class="col-sm-11">
                                      <textarea name="address" class="form-control" id="address" placeholder="Enter Address">{{profiledata._Address}}</textarea>
                                    </div>
                                </div>
                                  <div class="form-group row">
                                    <label class="col-sm-1 col-form-label" for="exampleInputEmail1">Mobile</label>
                                    <div class="col-sm-11">
                                      <input type="text"  class="form-control" id="mobile" name="mobile" placeholder="Enter Email" value="{{profiledata._Mobile}}">
                                    </div>
                                </div>
                                 <div class="form-group row">
                                    <label class="col-sm-1 col-form-label" for="exampleInputEmail1">Username</label>
                                    <div class="col-sm-11">
                                      <input type="text"  class="form-control" id="username" name="username" placeholder="Enter Username" value="{{profiledata._Username}}">
                                    </div>
                                </div>
                                 <div class="form-group row">
                                    <label class="col-sm-1 col-form-label" for="exampleInputEmail1">Current Password</label>
                                    <div class="col-sm-11">
                                      <input type="Password"  class="form-control" id="curpass" name="curpass" placeholder="Enter Password" value="{{profiledata._Password}}">
                                    </div>
                                </div>

                                 <div class="form-group row">
                                    <label class="col-sm-1 col-form-label" for="exampleInputEmail1">New Password</label>
                                    <div class="col-sm-11">
                                      <input type="Password"  class="form-control" id="newpass" name="newpass" placeholder="Enter Password">
                                    </div>
                                </div>
                                <input type="submit" class="btn btn-success" value="Submit" style="margin-top: 10px;">
                      
                              {% endif %}
                      </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
    </div>                    
    </section>

  </div>
  <!-- /.content-wrapper -->

  {% endblock %}
