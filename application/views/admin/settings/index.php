{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1>Settings</h1> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
              <li class="breadcrumb-item active">Settings</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
                  Genderal Settings
            </div>
            <div class="card-body">
               <div class="row">
              <div class="col-md-12">
              <form  id="general_form" name="general_form" method="post" enctype="multipart/form-data">
                  {% if general_data[0] != ''  %}
                      {% set i = 0 %}
                      {% for gdata in general_data %}
                        <div class="wrapper">
                            <div class="col-md-6 float-left">
                              <div class="form-group">
                                <input type="text" class="form-control  float-left" name="key[]"   placeholder="Input key Here" value="{{gdata['_Key']}}">
                              </div>
                            </div>
                            <div class="col-md-6 float-left">
                              <div class="form-group">
                                <input type="text" class="form-control "  name="value[]" placeholder="Input value Here" value="{{gdata['_Value']}}">
                            </div>
                          </div>
                          <a href="javascript:void(0);" class="remove_field">Remove</a>
                        </div>
                      {% set i = i + 1 %}
                      {% endfor %}
                  {% else %}
                  
                      <div class="wrapper">
                            <div class="col-md-6 float-left">
                              <div class="form-group">
                                <input type="text" class="form-control col-md-6 float-left" name="key[]" placeholder="Input key Here" />
                                </div>
                            </div>
                            <div class="col-md-6 float-left">
                              <div class="form-group">
                                <input type="text" class="form-control col-md-6" name="value[]" placeholder="Input value Here" />
                              </div>
                            </div>
                      </div>

                  {% endif %}
                  <br>
                  <p><button class="add_fields btn btn-info">Add More Fields</button></p>

                  <input type="submit" class="btn btn-success" value="save" style="margin-top: 10px;">

              </form>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
       <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
                  Social Settings
            </div>
            <div class="card-body">
               <div class="row">
              <div class="col-md-12">
              <form  id="social_form" name="social_form" method="post" enctype="multipart/form-data">
           
              {% if social_data != ''  %}
                {% set i = 0 %}
                {% for sdata in social_data %}
                
                <div class="form-group">
                    <label>{{sdata._Key}}</label>
                    <input type="text" class="form-control" id="{{sdata._Key}}" name="{{sdata._Key}}" placeholder="Enter {{sdata._Key}} Link" value="{{sdata._Value}}">   
                </div>

                  {% set i = i + 1 %}
                {% endfor %}
              {% endif %}

              <input type="submit" class="btn btn-success" value="save" style="margin-top: 10px;">

              </form>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  {% endblock %}

    {% block scripts  %}
  <script src="{{ constant('cmstheme') }}js/settings.js"></script>
    <script type="text/javascript">

        $(document).ready(function() {
            var wrapper    = $(".wrapper"); 
            var add_button = $(".add_fields"); 
            var x = 1; 

            $(add_button).click(function(e){
                e.preventDefault();
      
                $('div.wrapper:last').append('<div> <div class="col-md-6 float-left"><div class="form-group"><input type="text" name="key[]" placeholder="Input key Here" class="form-control float-left"/></div></div> <div class="col-md-6 float-left"><div class="form-group"><input type="text" name="value[]" placeholder="Input value Here" class="form-control  "/></div></div> <a href="javascript:void(0);" class="remove_field">Remove</a></div>');
            });

                $(wrapper).on("click",".remove_field", function(e){ 
                    e.preventDefault();
                    $(this).parent('div').remove(); 
                    x--; 
                })
            });

    </script>
    {% endblock %}
