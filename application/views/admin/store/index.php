{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1>Store Detail</h1> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
              <li class="breadcrumb-item active">Store</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              
              <form  id="store_form" name="store_form" method="post" enctype="multipart/form-data">
                {% if allstore|length >0 %}

               <input type="hidden" id="hsid" name="hsid" value="{{allstore._ID}}">
                <div class="row">
                <div class="col-md-12">
                   <div class="form-group">
                    <label for="code">Name</label>
                    <input type="text"  class="form-control" id="stname" name="stname" placeholder="Enter Name" value="{{allstore._Name}}">
                  </div>
                   <div class="form-group">
                    <label for="description">Description</label>
                     <textarea  name="des" class="form-control" id="des"  placeholder="Enter Description">{{allstore._Des}}</textarea>
                  </div>
                   <div class="form-group">
                    <label for="amount">Email</label>
                    <input type="text" name="email" class="form-control" id="email"  placeholder="Enter Email" value="{{allstore._Email}}">
                   
                  </div>
                   <div class="form-group">
                    <label for="amount">Mobile</label>
                    <input type="text" name="mno" class="form-control" id="mno"  placeholder="Enter Mobile" value="{{allstore._mobile}}" >
                  </div>
                  <div class="form-group">
                    <label for="amount">Address</label>
                     <textarea  name="add" class="form-control" id="add" placeholder="Enter Address">{{allstore._Address}}</textarea>
                  </div>
                    <div class="form-group">
                    <label for="reg_input_logo" >Logo</label>
                      <div class="custom-file">
                      <input type="file" class="custom-file-input" id="storeimag" name="storeimag">
                      <label class="custom-file-label" for="storeimag">Choose file</label>
                    </div> 

                    {% if (allstore._Logo) != "" %}
                    <img src="{{base_url()}}assets/uploads/store/{{allstore._Logo}}" width="100px" height="100px"></img> <sapn class="undo" data-param="{{allstore._ID}}"><i class="fa fa-times" aria-hidden="true"></i></span> 
                    {% endif %}       

                  </div>
                </div>
                <input type="submit" class="btn btn-success" value="save" style="margin-top: 10px;">
              </div>
               
              {% endif %}
              </form>
          
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  {% endblock %}
  {% block scripts %}
  <script src="{{ constant('cmstheme') }}js/store.js"></script>
  {% endblock %}