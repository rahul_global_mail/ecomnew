{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1>Sub Category Create</h1> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
              <li class="breadcrumb-item active">category</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
               <div class="row">
              <div class="col-md-12">
              
              <form  id="subcategory_form" name="subcategory_form" method="post" enctype="multipart/form-data">
                {% if editsubcatid is not defined %}
                   <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text"  class="form-control" id="subcatname" name="subcatname" placeholder="Enter Name" >
                  </div>

                  <div class="form-group">
                    <label for="category">Parent category</label>
                    <select name="parent_id" id="parent_id" class="form-control">
                      <option value="">--Please select--</option>
                      {% if allcatedata|length > 0 %}

                        {% for cdata in allcatedata %}
                          <option value="{{cdata._ID}}">{{cdata._Name}}</option>
                        {% endfor %}
                      {% endif %} 
                    </select>
                
                  </div>

                   <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" id="subcatdes" name="subcatdes" placeholder="Enter Description" cols="3"></textarea>
                    
                  </div>
                
                   <div class="form-group">
                    <label for="image">Image</label>
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" id="subcatimage" name="subcatimage">
                      <label class="custom-file-label" for="subcatimage">Choose file</label>
                    </div>
                  </div>
                  <input type="submit" class="btn btn-success" value="create" style="margin-top: 10px;">
                 {% else %}  
          {% if editsubcat|length > 0 %}

           <input type="hidden" id="hsubcatid" name="hsubcatid" value="{{editsubcat._ID}}">
           <input type="hidden" id="_subcatname" name="_subcatname" value="{{editsubcat._Name}}">
          <div class="form-group">
             <label for="name">Name</label>
             <input type="text"  class="form-control" id="subcatname" name="subcatname" placeholder="Enter Name" value="{{editsubcat._Name}}" >
           </div>
         
            <div class="form-group">
                    <label for="caegory">Parent category</label>
                    <select name="parent_id" id="parent_id" class="form-control">
                      <option value="">--Please select--</option>

                      {% if allcatedata|length> 0 %}

                        {% for cdata in allcatedata %}
                          <option value="{{cdata._ID}}"{% if editsubcat._Main_id == cdata._ID %} selected {% endif %}>{{cdata._Name}}</option>
                        {% endfor %}
                      {% endif %} 
                    </select>
                
                  </div>
         
            <div class="form-group">
             <label for="description">Description</label>
             <textarea class="form-control" id="subcatdes" name="subcatdes" placeholder="Enter Description" cols="3">{{editsubcat._Description}}</textarea>
             
           </div>
         
            <div class="form-group">
              <label for="image">Image</label>
             <div class="custom-file">
               <input type="file" class="custom-file-input" id="subcatimage" name="subcatimage" >
               <label class="custom-file-label" for="subcatimage">Choose file</label>
             </div>

             {% if(editsubcat._Image)!="" %}
             <img src="{{base_url()}}assets/uploads/subcategory/{{editsubcat._Image}}" style="width: 80px;height: 80px;border-radius: 50px;">
             {% endif %}

           </div>
           <input type="submit" class="btn btn-success" value="save" style="margin-top: 10px;">
         
          {% endif %} 
                  
            {% endif %}
              </form>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  {% endblock %}
  {% block scripts %}
  <script src="{{ constant('cmstheme') }}js/subcategory.js"></script>
   {% endblock %}
