{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    	<!-- Content Header (Page header) -->
    	<section class="content-header">
      		<div class="container-fluid">
			<div class="row mb-2">
	          		<div class="col-sm-6">
	             			<h1>{{pagetitle}}</h1> 
	          		</div>
		          	<div class="col-sm-6">
			            	<ol class="breadcrumb float-sm-right">
			              		<li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
			              		<li class="breadcrumb-item active">Template</li>
			            	</ol>
		          	</div>
        		</div>
      		</div><!-- /.container-fluid -->
    	</section>

      <!-- Main content -->
    	<section class="content">
      		<div class="row">
        		<div class="col-12">
          			<div class="card">
            				<div class="card-body">
               					<div class="row">
              						<div class="col-md-12">              
              							<form class="form-horizontal"  id="template_form" name="template_form" method="post">
                						{% if editid is not defined %}
						                   	<div class="form-group row">
						                    		<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Name</label>
						                    		<div class="col-sm-11">
						                    			<input type="text"  class="form-control" id="name" name="name" placeholder="Enter Name" >
						                    		</div>
						                  	</div>
                   							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Slug</label>
							                    	<div class="col-sm-11">
							                    		<input type="text"  class="form-control" id="slug" name="slug" >
							                    	</div>
                  							</div>
                  							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Subject</label>
							                    	<div class="col-sm-11">
							                    		<input type="text"  class="form-control" id="subject" name="subject" >
							                    	</div>
                  							</div>
                  							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Content</label>
							                    	<div class="col-sm-11">
							                    		<textarea  name="content" class="form-control" id="content" placeholder="Enter Content"></textarea>
							                    	</div>
                  							</div>
                  							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Mail To</label>
							                    	<div class="col-sm-11">
							                    		<input type="text"  class="form-control" id="mailto" name="mailto" placeholder="Enter mail" >
							                    	</div>
                  							</div>
                  							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Type</label>
							                    	<div class="col-sm-11">
							                    	<select class=" form-control select2 " multiple="multiple" data-placeholder="Select a type" name="type[]" id="type">
							                    		<option value="0">admin</option>
							                    		<option value="1">user</option>
							                    	</select>
							                    </div>
							                    	
                  							</div>
                  							<input type="submit" class="btn btn-success" value="Submit" style="margin-top: 10px;">
							        {% else %}  

							        {% if edittemp|length > 0 %}

          								<input type="hidden" id="htid" name="htid" value="{{edittemp._ID}}">
          									<div class="form-group row">
						                    		<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Name</label>
						                    		<div class="col-sm-11">
						                    			<input type="text"  class="form-control" id="name" name="name" placeholder="Enter Name" value="{{edittemp._Name}}">
						                    		</div>
						                  	</div>
                   							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Slug</label>
							                    	<div class="col-sm-11">
							                    		<input type="text"  class="form-control" id="slug" name="slug" value="{{edittemp._Slug}}" >
							                    	</div>
                  							</div>
                  							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Subject</label>
							                    	<div class="col-sm-11">
							                    		<input type="text"  class="form-control" id="subject" name="subject"value="{{edittemp._Subject}}">
							                    	</div>
                  							</div>
                  							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Content</label>
							                    	<div class="col-sm-11">
							                    		<textarea  name="content" class="form-control" id="content" placeholder="Enter Content">{{edittemp._Content}}</textarea>
							                    	</div>
                  							</div>
                  							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Mail To</label>
							                    	<div class="col-sm-11">
							                    		<input type="text"  class="form-control" id="mailto" name="mailto" placeholder="Enter mail" value="{{edittemp._Mail_to}}" >
							                    	</div>
                  							</div>
                  							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Type</label>
							                    	<div class="col-sm-11">
							                    	<select class="select2 form-control" multiple="multiple" data-placeholder="Select a type" name="type[]" id="type">
							                    		<option value="0"{% if 0 in edittemp._Type %} selected {% endif %}>admin</option>
							                    		<option value="1" {% if 1 in edittemp._Type %} selected {% endif %}>user</option>
							                    	</select>
							                    </div>
							                    	
                  							</div>
                  							<input type="submit" class="btn btn-success" value="Save" style="margin-top: 10px;">
	                  					{% endif %}
                  						{% endif %}
      								</form>
                  					</div>
                  				</div>
                  			</div>
                  		</div>
                  	</div>
		</div>                  	
  	</section>
</div>
  <!-- /.content-wrapper -->

  {% endblock %}

{% block scripts %}
	<script src="{{ constant('cmstheme') }}js/template.js"></script>
  {% endblock %}