{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1>Customers</h1> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
              <li class="breadcrumb-item active">Customers</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <!-- <div class="card-header">
            </div> -->
            <!-- /.card-header -->
            <div class="card-body">
              <table id="user_tab" class="table table-bordered table-hover">
                <thead>
                <tr>
                 

            
              <table id="user_tab" class="table table-bordered table-hover">
                <thead>
                <tr>
                   <th>{% if userdata|length > 0 %}
              
            <button class="btn btn-danger" name="delete_all" id="delete_all" class="glyphicon glyphicon-pencil"><i class="fas fa-user-slash"></i></button>
            {% endif %}</th>
                  <th>No</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Mobile No</th> 
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  
                  {% if userdata|length > 0 %}
                  {% set i = 1 %}
                    {% for udata in userdata %}
                    
                <tr>
                    <td><input type="checkbox" class="delete_checkbox" name="checkuser[]" id="checkuser" value="{{udata._ID}}"></td>
                  	<td>{{ i }}</td>
                  	<td>{{udata.fullname}}</td>
                  	<td>{{udata._Email}}</td>
                  	<td>{{udata._Mobile}}</td>
                  	
                  	<td> 
					<div class="btn-group">
                    	<a  href="{{ base_url('address/'~udata._ID) }}" class="btn btn-primary"><i class="fa fa-address-card"></i></a>
                    	<a href="{{ base_url('edituser/'~udata._ID) }}" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                    	<button type="button" class="btn btn-info" onclick="ecommerce.showAjaxModal('admin/user/show/{{udata._ID}}','Profile');"><i class="fa fa-user-edit"></i></button>
                  	</div>
                  	</td>
                </tr>
                
                  {% set i = i + 1 %}
                 {% endfor %}
                
               {% endif %}
                </tbody>
              
            </table>

            </div>
            <!-- /.card-body -->
          </div>
        
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  {% endblock %}
  {% block scripts %}
  <script src="{{ constant('cmstheme') }}js/user.js"></script>
   {% endblock %}