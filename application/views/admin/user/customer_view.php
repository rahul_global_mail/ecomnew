<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table">
                <tbody>
                    <tr>
                        <td><b>Name</b></td>
                        <td>{{ customer.full_name }}</td>
                    </tr>
                    <tr>
                        <td><b>Email</b></td>
                        <td>{{ customer._Email }}</td>
                    </tr>
                    <tr>
                        <td><b>Mobile</b></td>
                        <td>{{ customer._Mobile }}</td>
                    </tr>
                    <tr>
                        <td><b>Password</b></td>
                        <td>{{ customer._Password }}</td>
                    </tr>
                    <tr>
                        <td><b>Gender</b></td>
                        <td>{{ customer._Gender }}</td>
                    </tr>
                        <td><b>Register at</b></td>
                        <td>{{ customer._Created|date("d-m-Y") }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>