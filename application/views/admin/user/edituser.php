{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1>Customers</h1> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
              <li class="breadcrumb-item active">Customer</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
               <div class="row">
              <div class="col-md-12">
              <div class="col-md-6">
              
              <form  id="edituser_form" name="edituser_form" method="post" enctype="multipart/form-data">
                 <input type="hidden" id="useredit" value="1" />
                 <input type="hidden" id="cityedit" value= "{{userinfo._City}}"/>
               <!--  <div class="card-body"> -->
                 {% if userinfo|length > 0 %}
                 {% if count(userinfo) > 0 %}
                 <input type="hidden" id="huserid" name="huserid" value= "{{userinfo._ID}}"/>
                   <div class="form-group">
                    <label for="exampleInputEmail1">Firstname</label>
                    <input type="text"  class="form-control" id="fname" name="fname" placeholder="Enter Firstname" value="{{userinfo._Firstname}}" >
                  </div>
                   <div class="form-group">
                    <label for="exampleInputEmail1">Lastname</label>
                    <input type="text" class="form-control" id="lname" name="lname" placeholder="Enter Lastname" value="{{userinfo._Lastname}}">
                  </div>
                   <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" name="email" class="form-control" id="email"  placeholder="Enter email" value="{{userinfo._Email}}">
                  </div>
                   <div class="form-group">
                    <label for="exampleInputEmail1">Mobile Number</label>
                    <input type="text" name="mno" class="form-control" id="mno"  placeholder="Enter Mobile Number" value="{{userinfo._Mobile}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" name="password" class="form-control" id="password" placeholder=" Enter Password" value="{{userinfo._Password}}">
                  </div>

                    <div class="form-group">
                    <label for="exampleInputPassword1">Gender</label>
                    
                      <select name="gender" id="gender" class="form-control">
                      <option value="">--select--</option>
                      <option value="Male" {% if userinfo._Gender == "Male" %} selected {% endif %}>Male</option>
                      <option value="Female" {% if userinfo._Gender =="Female" %} selected {% endif %}>Female</option>
                    </select>
                   
                  </div>

                    <div class="form-group">
                    <label for="exampleInputEmail1">zipcode</label>
                    <input type="text" name="zipcode" class="form-control" id="zipcode" placeholder="Enter zipcode" value="{{userinfo._Zipcode}}">
                  </div>

                  <div class="form-group">
                    <label for="reg_input_logo" >Upload Photo</label>
                     <input name="user_photo" id="user_photo" type="file" /><img src="{{base_url()}}assets/uploads/user/{{userinfo._Profilepic}}" style="width: 100px;height: 100px;border-radius: 50px;">                    
                  </div>
                </div>   
                </div>
                 <div class="col-md-6">

                   <div class="form-group">
                    <label for="exampleInputEmail1">address1</label>
                    <textarea  name="address1" class="form-control" id="address1" rows="3" placeholder="Enter address">{{userinfo._Address.address1}}</textarea> 
                  </div>
                    <div class="form-group">
                    <label for="exampleInputEmail1">address2</label>
                    <textarea  name="address2" class="form-control" id="address2" rows="3" placeholder="Enter address">{{userinfo._Address.address2}}</textarea> 
                  </div>
                    <div class="form-group">
                    <label for="exampleInputEmail1">address3</label>
                    <textarea  name="address3" class="form-control" id="address3" rows="3" placeholder="Enter address">{{userinfo._Address.address3}}</textarea> 
                  </div>
                   <div class="form-group">
                    <label for="exampleInputEmail1" >state</label>
                    <select name="state" id="state" class="form-control">
                      <option value="">--select--</option>
                      {% if count(allstate) > 0 %}
                      {% for statedata in allstate %} 
                      <option value="{{statedata.id}}"{% if userinfo._State == statedata.id %} selected {% endif %}>{{statedata.name}}</option>
                      
                      {% endfor %}
                      {% endif %} 
                    </select>
                  </div>
                   <div class="form-group">
                    <label for="exampleInputEmail1">city</label>
                    <select name="city" id="city" class="form-control">
                     <option value="">--select--</option>
                    </select>
                  </div>
                </div>

                  <input type="submit" class="btn btn-success" value="submit" style="margin-top: 10px;">
              
                {% endif %} 
              </form>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  {% endblock %}
   {% block scripts %}
  <script src="{{ constant('cmstheme') }}js/user.js"></script>
   {% endblock %}
  
