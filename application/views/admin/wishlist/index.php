{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

      <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Wishlist</h1>
          </div>
           <div class="col-sm-6">
           <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
           <li class="breadcrumb-item active">Wishlist</li>
         </ol>
       </div> 
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card  card-default">
            
            <div class="card-body">
              <table id="wish_tab" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>User Name</th>
                  <th>Product Name</th> 
                </tr>
                </thead>
                <tbody>
                  {% if count(data) > 0 %}
                
                  {% set i = 1 %}
                    {% for wdata in data %}
                    
                <tr>
                  <td>{{i}}</td>
                  <td>{{wdata.users._Firstname}}</td>
                  <td>{{wdata.products._Name}}</td>
                </tr>
                
                  {% set i = i + 1 %}
                 {% endfor %}
                
               {% endif %}
                </tbody>
              
            </table>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

  
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
</section>

  </div>
  <!-- /.content-wrapper -->

  {% endblock %}

  {% block scripts %}
    <script type="text/javascript">
        ecommerce._payment();
        ecommerce._delpayment();
    </script>
    {% endblock %}
