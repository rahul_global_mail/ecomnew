{% extends 'front/app/index.php' %}

{% block content %} 
<main class="page-main">
				<div class="block">
					<div class="container">
						<ul class="breadcrumbs">
							<li><a href="index.html"><i class="fa fa-building"></i></a></li>
							<li>/<span>My Address</span></li>
						</ul>
					</div>
				</div>
				<div class="block">
					<div class="address-top">
						<div class="container">
							<button type="button" class="btn btn-primary" onclick="ecommerce.showAjaxModal('front/account/edit/','Address Create', 'modal-lg');"><span class="m-r-5"><i class="fa fa-plus"></i></span>Add New address</button>
						</div>
					</div>
						
				<div class="address-middle">
					<div class="container">
                  <div class="blog-grid-4">
                  	{% if count(custadd) > 0 %}
								{% for caddress in custadd %}
						<div class="blog-post">
							<div class="blog-content">
								<h2 class="blog-title"><a href="#">{{ caddress._Name }}</a></h2>
								<div class="blog-text">
									<!-- <div class="pull-left"> -->
									<ul class="simple-list">
                        				<li class="small"><i class="fa fa-building"></i> Address: {{ caddress._Line1 }}&nbsp;{{ caddress._Line2 }}<br></li>
		                     			<li class="small"><i class="icon icon-location"></i>{{ caddress.cities.name }}&nbsp;{{ caddress.states.name }}
		                        		<br>
		                        		India&nbsp;{{ caddress._Postcode }}</li>
                        				<li class="small"><i class="icon icon-phone"></i> Phone #: {{ caddress.users._Mobile }}</li>
                      				</ul>
									<!-- </div> -->
								</div>
								<div align="right">
								<a onclick="ecommerce.showAjaxModal('front/account/edit/{{caddress._ID}}','Address Edit', 'modal-lg');" class="btn">Edit</a>
								<a onclick="_sweetalert('{{caddress._ID}}')" class="btn">Delete</a>
							</div>
							</div>
						</div>
						  {% endfor %}
                  {% endif %}
						
					</div>
				</div>
                </div>
						
					</div>
				<!-- </div>
				</div> -->
			</main>
{% endblock %}
{% block scripts %}
<script>
	function _sweetalert(id) {
		const del_url = base_url+'front/account/adddelete/'+id;
		Swal.fire({
		  	title: 'Are you sure?',
		  	text: "You won't be able to revert this!",
		  	icon: 'warning',
		  	showCancelButton: true,
		  	confirmButtonColor: '#3085d6',
		  	cancelButtonColor: '#d33',
		  	confirmButtonText: 'Yes, delete it!',
		  	showLoaderOnConfirm: true,
		  	preConfirm: () => {
		        $.ajax({
	                type: 'POST',
	                url: del_url,
	                success: function(response){
	                	console.log(response)
	                    var temp = JSON.parse(response);
	                    if(temp.type == 'success'){
	                        Swal.fire({
	                            title: temp.msg,
	                            type: "success",
	                            confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
	                            confirmButtonText: 'Ok',
	                            closeOnConfirm: false,
	                            onAfterClose: () => {
									location.reload();    
							  	}
	                        })
	                    }else {
	                        Swal.fire("No records found", '', "error");
	                    }
	                }
	            });
		  	}
		})
	}
	function reloadPage() {
		location.reload();
	}
	{% if this.session.flashdata.type %}
	ecommerce.notifyWithtEle('{{this.session.flashdata.message}}' , '{{this.session.flashdata.type}}' ,'topRight', 2000);
	{% endif %}
</script>
{% endblock %}