{% extends 'front/app/index.php' %}

{% block content %} 
<main class="page-main">
	<div class="block">
		<div class="container">
			<ul class="breadcrumbs">
				<li><a href="index.html"><i class="icon icon-home"></i></a></li>
				<li>/<span>Profile Detail</span></li>
			</ul>
		</div>
	</div>
	<div class="block">
		<div class="container">
			<div class="form-card">
				<div class="panel panel-default">
					<div class="panel-heading"><h4>Basic Detail</h4></div>
					<div class="panel-body">
			
				<form class="account-create" id="profile_form" name="profile_form" method="post" enctype="multipart/form-data">
					 {% if count(userdetail) > 0 %}
				
					<label>First Name<span class="required">*</span></label>
					<input type="text" class="form-control" name="fname" value="{{userdetail._Firstname}}">
					<label>Last Name<span class="required">*</span></label>
					<input type="text" class="form-control" name="lname" value="{{userdetail._Lastname}}">
					<label>E-mail<span class="required">*</span></label>
					<input type="text" class="form-control" name="email" id="email" value="{{userdetail._Email}}">
					<input type="hidden" id="_email" name="_email" value= "{{userdetail._Email}}"/>
					<input type="hidden" name="prohid" id="prohid" value="{{userdetail._ID}}">
					<input type="hidden" id="useredit" value="1" />
                 	<input type="hidden" id="cityedit" value= "{{userdetail._City}}"/>
                 </div>
             </div>

<div class="panel panel-default">
	<div class="panel-heading"><h4>Personal Detail</h4></div>
					<div class="panel-body">
					
					<div class="row">
					<div class="col-md-12">
					<label>Mobile<span class="required">*</span></label>
					<input type="text" class="form-control input-lg" name="mno" id="mno" value="{{userdetail._Mobile}}">
					<label>Gender<span class="required">*</span></label>
					<div class="select-wrapper-sm">
					 <select name="gender" id="gender" class="form-control">
                      <option value="">--select--</option>
                      <option value="Male" {% if userdetail._Gender == "Male" %} selected {% endif %}>Male</option>
                      <option value="Female" {% if userdetail._Gender =="Female" %} selected {% endif %}>Female</option>
                    </select>
                    <label>Profile<span class="required">*</span></label>
					<input type="file" class="form-control input-lg" name="user_photo" id="user_photo">
					{% if(userdetail._Profilepic !='')%}
					<img src="{{base_url()}}assets/uploads/user/{{userdetail ._Profilepic}}" style="width: 100px;height: 100px;border-radius: 50px;">
					{% endif %}
                </div>
					
					
				</div>
				<div style="margin-left: 10px;">
						<input type="submit" class="btn btn-lg" value="Save"></div>
			</div>
					
				</div>
				</div>
				
				</form>

				<div class="panel panel-default">
				<div class="panel-heading"><h4>Change Password</h4></div>
					<div class="panel-body">

					
					<form class="account-create" id="profilepass_form" name="profilepass_form" method="post" enctype="multipart/form-data">
					<div class="row">
					<div class="col-md-6">
					<input type="hidden" name="prohid" id="prohid" value="{{userdetail._ID}}">
					<label>Current Password<span class="required">*</span></label>
					<input type="password" class="form-control" name="cpass" id="cpass">
					</div>
					<div class="col-md-6">
					<label>New Password<span class="required">*</span></label>
					<input type="password" class="form-control" name="newpass" id="newpass">
					</div>
					</div>
					<div>
						<input type="submit" class="btn btn-lg" value="Save"><span class="required-text">* Required Fields</span></div>
					{% endif %}
				</form>
			</div>
		</div>
			</div>
		</div>
	</div>
</main>
{% endblock %}