	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SEIKO eCommerce HTML 5 Template</title>
	<meta name="author" content="BigSteps">
	<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
	<!-- Vendor -->
	<link href="{{ constant('fronttheme') }}js/vendor/slick/slick.css" rel="stylesheet">
	<link href="{{ constant('fronttheme') }}js/vendor/swiper/swiper.min.css" rel="stylesheet">
	<link href="{{ constant('fronttheme') }}js/vendor/magnificpopup/dist/magnific-popup.css" rel="stylesheet">
	<link href="{{ constant('fronttheme') }}js/vendor/darktooltip/dist/darktooltip.css" rel="stylesheet">
	<link href="{{ constant('fronttheme') }}js/vendor/bootstrap/bootstrap.min.css" rel="stylesheet">
	<link href="{{ constant('fronttheme') }}js/vendor/nouislider/nouislider.css" rel="stylesheet">
	
	<link rel="stylesheet" href="{{ constant('cmstheme') }}css/noty.css">
	<link href="{{ constant('fronttheme') }}css/animate.css" rel="stylesheet">

	<!-- Custom -->
	<link href="{{ constant('fronttheme') }}css/style.css" rel="stylesheet">
	<link href="{{ constant('fronttheme') }}css/megamenu.css" rel="stylesheet">
	<link href="{{ constant('fronttheme') }}css/tools.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Color Schemes -->
	<link href="{{ constant('fronttheme') }}css/style-color-orange.css" rel="stylesheet">
	<!-- your style-color.css here  -->

	<!-- Icon Font -->
	<link href="{{ constant('fronttheme') }}fonts/icomoon-reg/style.css" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="{{ constant('cmstheme') }}plugins/sweetalert2/sweetalert2.min.css"/>

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
	<!-- <script src="{{ constant('fronttheme') }}js/vendor/jquery/jquery.js"></script> -->
	