<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<html lang="en">
<head>
		<base id="myBase" href="{{ base_url() }}">
		<meta name="_token" content="{{ this.security.get_csrf_hash() }}" />
		<script type="text/javascript">
			window.base_url =  document.getElementById('myBase').getAttribute('href');
		</script>
		{% include 'front/app/header.php' %}
	</head>
	<body class="color-orange boxed">

		<div class="wrapper">
		<div id="wrapper">
			<!-- Page -->
			<div class="page-wrapper">
			{% include 'front/app/top.php' %}

		{% include 'front/app/sidebar.php' %}

	{% block content %}{% endblock %}

		{% include 'front/app/footer.php' %}

</div>
		</div>
				{% include 'front/app/bottom.php' %}
				{% include 'front/app/modal.php' %}
			</div>
		
		{% block scripts %}{% endblock %}
	</body>
</html>