<div class="modal fade" id="modal_ajax" role="dialog">
  <div class="modal-dialog">
   <!--  Modal content -->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modal-header">Modal Header</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn button light" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

<div class="modal quick-view zoom" id="quickView">
	<div class="modal-dialog">
		<div class="modalLoader-wrapper">
			<div class="modalLoader bg-striped"></div>
		</div>
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">&#10006;</button>
		</div>
		<div class="modal-content">
			<iframe></iframe>
		</div>
	</div>
</div>
