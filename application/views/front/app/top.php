<!-- Header -->
      <header class="page-header fullboxed variant-9 sticky always">
        <!-- Header Top Row -->
        <div class="header-top-row">
          <div class="container">
            <div class="header-top-left">
              <div class="header-custom-text">
                <ul class="social-list-simple small">
                  <li>
                    <a href="#" class="icon icon-google google"></a>
                  </li>
                  <li>
                    <a href="#" class="icon icon-twitter-logo twitter"></a>
                  </li>
                  <li>
                    <a href="#" class="icon icon-facebook-logo facebook"></a>
                  </li>
                </ul>
              </div>
              <div class="header-custom-text">
                <span><i class="icon icon-phone"></i> +{{addetail._mobile}}</span>
                <span class="hidden-xs"><i class="icon icon-location"></i> {{addetail._Address}}</span>
              </div>
            </div>
           
          </div>
        </div>
        <!-- /Header Top Row -->
        <div class="navbar">
          <div class="container">
            <!-- Menu Toggle -->
            <div class="menu-toggle"><a href="#" class="mobilemenu-toggle"><i class="icon icon-menu"></i></a></div>
            <!-- /Menu Toggle -->
            <div class="header-right-links">
              <!-- <div class="collapsed-links-wrapper">
                <div class="collapsed-links"> -->
                  <!-- Header Links -->
                <!--   <div class="header-links"> -->
                    <!-- Header WishList -->
                    <!-- {% if get_cookie('userid') != '' %}
                    <div class="header-link">
                      <a href="{{ base_url('register') }}"><i class="fa fa-registered"></i><span class="link-text">Register</span></a>
                       <a href="{{ base_url('login') }}"><i class="icon icon-user"></i><span class="link-text">Login</span></a>
                    </div>
                    {% else %}
                    <div class="header-link">
                      <a href="{{ base_url('register') }}"><i class="fa fa-registered"></i><span class="link-text">Register</span></a>
                       <a href="{{ base_url('login') }}"><i class="icon icon-user"></i><span class="link-text">Login</span></a>
                    </div>
                    {% endif %} -->
                    <!-- Header WishList -->
                    <!-- Header Account -->
                    <!-- <div class="header-link ">
                    
                      <a href="{{ base_url('login') }}"><i class="icon icon-user"></i><span class="link-text">Login</span></a>
                    </div> -->
                    <!-- /Header Account -->
                 <!--  </div> -->
                  <!-- /Header Links -->
                  <!-- Header Cart -->

                   
                  <div class="header-link dropdown-link header-cart variant-1">
                    <a href="{{base_url('cart_view')}}"> <i class="icon icon-cart"></i> <span class="badge">{{ cart_view_count() }}</span><span class="link-text">My Cart</span></a>
                    <!-- minicart wrapper -->
                    <div class="dropdown-container right">
                      <!-- minicart content -->
                  	<div class="block block-minicart">
                      	<div class="minicart-content-wrapper">
                      		<div class="block-title">
								<span>Recently added item(s)</span>
							</div>
							<a class="btn-minicart-close" title="Close">❌</a>
							<div class="block-content">
								<div class="minicart-items-wrapper overflowed">
									<ol class="minicart-items">
                     					{{ cart_view_header() | raw }}
                     				</ol>
                     			</div>
                 				<div class="actions">
									<div class="secondary">
										<a href="{{base_url('cart_view')}}" class="btn btn-alt">
											<i class="icon icon-cart"></i><span>View and edit cart</span>
										</a>
									</div>
									<div class="primary">
                    {% if get_cookie('userid') == '' %}
                    <a class="btn" data-toggle="modal" data-target="#modal3">
                      <i class="icon icon-external-link"></i><span>Go to Checkout</span>
                    </a>
										
                      {% else %} 
                      <a class="btn" href="{{base_url('checkout')}}">
                      <i class="icon icon-external-link"></i><span>Go to Checkout</span>
                    </a>
                       {% endif %}
									</div>
								</div>
                     		</div>
                     	</div>
                 	</div>
                      <!-- /minicart content -->
                    </div>
                    <!-- /minicart wrapper -->
                  </div>
                  <!-- /Header Cart -->
               <!--  </div>
                             </div> -->
              <!-- Header Search -->
              <div class="header-link header-search header-search">
                <div class="exp-search">
                  <form name="all_search" id="all_search" method="get" action="productlist">
                    <input class="exp-search-input " placeholder="Search here ..." type="text"  name="search" id="search_id">
                    <input class="exp-search-submit" type="submit" value="">
                    <span class="exp-icon-search"><i class="icon icon-magnify"></i></span>
                    <span class="exp-search-close"><i class="icon icon-close"></i></span>
                  </form>
                </div>
              </div>
              <!-- /Header Search -->
            </div>
            <!-- Logo -->
            <div class="header-logo">
              <a href="{{base_url()}}" title="Logo"><img src="{{ constant('fronttheme') }}images/logo.png" alt="Logo" /></a>
            </div>
            <!-- /Logo -->
            <!-- Mobile Menu -->
            <div class="mobilemenu dblclick">
              <div class="mobilemenu-header">
                <div class="title">MENU</div>
                <a href="#" class="mobilemenu-toggle"></a>
              </div>
              <div class="mobilemenu-content">
                <ul class="nav">
                  <li><a href="{{base_url('front')}}">HOME</a><span class="arrow"></span>
                   <!--  <ul>
                     <li> <a href="index.html" title="">Default</a> </li>
                     <li> <a href="index-bg-white.html" title="">White Background</a> </li>
                     <li> <a href="index-layout-6.html" title="">Wide + Side Panel</a> </li>
                     <li> <a href="index-layout-1.html" title="">Classic</a> </li>
                     <li> <a href="index-layout-2.html" title="">Journal<span class="menu-label">new look</span></a> </li>
                     <li> <a href="index-layout-3.html" title="">Banners Boom</a> </li>
                     <li> <a href="index-fullscreen-slider.html" title="">Fullscreen Slider</a> </li>
                     <li> <a href="index-layout-4.html" title="">Amason</a> </li>
                     <li> <a href="index-layout-5.html" title="">Lookbook</a> </li>
                     <li> <a href="index-rtl.html" title="">RTL</a> </li>
                     <li> <a href="index-popup.html" title="">Popup on Load</a> </li>
                   </ul>  -->
                  </li>
                  <li class="mega-dropdown">
                  <a href="{{base_url('productlist')}}">Shop</a><span class="arrow"></span>
                  <div class="sub-menu">
                    <div class="container">
                      <div class="megamenu-categories column-4">
                        <!-- megamenu column -->
                        {{ product_view(1) | raw }}
                        <!-- /megamenu column -->
                      </div>
                    </div>
                  </div>
                </li>


                {% if get_cookie('userid') != '' %}
                  <li class="simple-dropdown"><a href="#">My Account</a>
                    <div class="sub-menu">
                      <ul class="category-links">
                      <li> <a href="{{ base_url('userprofile') }}" title=""> <i class="fa fa-user"></i> My Profile </a> </li>
                      <li> <a href="{{ base_url('customeraddress') }}" title=""> <i class="fa fa-user"></i> My Address </a> </li>
                      <li> <a href="{{ base_url('showwishlist') }}" title=""><i class="fa fa-heart" aria-hidden="true"></i> My Wishlist </a> </li>
                      <li><a href="{{ base_url('showorders') }}" title=""> <i class="fa fa-money"></i> My Orders</a> </li>
                      <li> <a href="{{base_url('cart_view')}}" title=""><i class="fa fa-shopping-cart"></i> My Cart</a> </li>
                    </ul>
                  </div>
                  </li>
                <li><a href="{{ base_url('logout_front') }}">Logout</a></li>
                {% else %}
                <li><a href="{{ base_url('login') }}">Login</a></li>
                <li><a href="{{ base_url('register') }}">Register</a></li>
                 {% endif %}
                  
                </ul>
              </div>
            </div>
            <!-- Mobile Menu -->
            <!-- Mega Menu -->
            <div class="megamenu fadein blackout">
              <ul class="nav">
                <li class="simple-dropdown">
                  <a href="{{base_url('front')}}">HOME</a>
                <!--   <div class="sub-menu">
                                 <ul class="category-links">
                 <li> <a href="index.html" title="">Default</a> </li>
                 <li> <a href="index-bg-white.html" title="">White Background</a> </li>
                 <li> <a href="index-layout-6.html" title="">Wide + Side Panel</a> </li>
                 <li> <a href="index-layout-1.html" title="">Classic</a> </li>
                 <li> <a href="index-layout-2.html" title="">Journal<span class="menu-label">new look</span></a> </li>
                 <li> <a href="index-layout-3.html" title="">Banners Boom</a> </li>
                 <li> <a href="index-fullscreen-slider.html" title="">Fullscreen Slider</a> </li>
                 <li> <a href="index-layout-4.html" title="">Amason</a> </li>
                 <li> <a href="index-layout-5.html" title="">Lookbook</a> </li>
                 <li> <a href="index-rtl.html" title="">RTL</a> </li>
                 <li> <a href="index-popup.html" title="">Popup on Load</a> </li>
                                 </ul>
                </div> -->
                </li>
               
                <li class="mega-dropdown">
                  <a href="{{base_url('productlist')}}">Shop</a>
                  <div class="sub-menu">
                    <div class="container">
                      <div class="megamenu-categories column-4">
                       {{ product_view(1) | raw }}
                      </div>
                    </div>
                  </div>
                </li>

                <li class="mega-dropdown">
                  <a href="{{base_url('gallerylist')}}">Gallery</a>
                  <div class="sub-menu">
                    <div class="container">
                      <div class="megamenu-categories column-4">
                       {{ product_view(1) | raw }}
                      </div>
                    </div>
                  </div>
                </li>
                

               
                {% if get_cookie('userid') != '' %}
                  <li class="simple-dropdown"><a href="#">My Account</a>
                  	<div class="sub-menu">
                     	<ul class="category-links">
                      <li> <a href="{{ base_url('userprofile') }}" title=""> <i class="fa fa-user"></i> My Profile </a> </li>
                      <li> <a href="{{ base_url('customeraddress') }}" title=""> <i class="fa fa-user"></i> My Address </a> </li>
                      <li> <a href="{{ base_url('showwishlist') }}" title=""><i class="fa fa-heart" aria-hidden="true"></i> My Wishlist </a> </li>
                      <li><a href="{{ base_url('showorders') }}" title=""> <i class="fa fa-money"></i> My Orders</a> </li>
                      <li> <a href="{{base_url('cart_view')}}" title=""><i class="fa fa-shopping-cart"></i> My Cart</a> </li>
                    </ul>
                  </div>
                  </li>
                <li><a href="{{ base_url('logout_front') }}">Logout</a></li>
                {% else %}
                <li><a href="{{ base_url('login') }}">Login</a></li>
                <li><a href="{{ base_url('register') }}">Register</a></li>
                 {% endif %}
              </ul>
            </div>
            <!-- /Mega Menu -->
          </div>
        </div>
      </header>

        <div class="modal modal-countdown fade zoom info success" id="modal3">
    <div class="modal-dialog">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">&#10006;</button> -->
      </div>
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-center">
            <div class="icon-info"><i class="icon icon-alert"></i></div>
            <p><a href="{{base_url('login')}}">Please must be login</a></p>
          </div>
         
        </div>
      </div>
    </div>
  </div>
      <!-- /Header -->