{% extends 'front/app/index.php' %}

{% block content %} 
  <!-- Loader -->
  <div id="loader-wrapper" class="off">
    <div class="cube-wrapper">
      <div class="cube-folding">
        <span class="leaf1"></span>
        <span class="leaf2"></span>
        <span class="leaf3"></span>
        <span class="leaf4"></span>
      </div>
    </div>
  </div>
  <!-- /Loader -->
  <div class="fixed-btns">
    <!-- Back To Top -->
    <a href="#" class="top-fixed-btn back-to-top"><i class="icon icon-arrow-up"></i></a>
    <!-- /Back To Top -->
  </div>

    <!-- Page Content -->
        <main class="page-main">
        <div class="block">
          <div class="container">
            <ul class="breadcrumbs">
              <li><a href="index.html"><i class="icon icon-home"></i></a></li>
              <li>/<span>Shopping Cart</span></li>
            </ul>
          </div>
        </div>
        <div class="block">
          <div class="container">
            <div class="cart-table">
              <div class="table-header">
                <div class="photo">
                  Product Image
                </div>
                <div class="name">
                  Product Name
                </div>
                <div class="price">
                  Unit Price
                </div>
                <div class="qty">
                  Qty
                </div>
                <div class="subtotal">
                  Subtotal
                </div>
                <div class="remove">
                  <span class="hidden-sm hidden-xs">Remove</span>
                </div>
              </div>



              {% if count(cart_data) > 0 %}
             
              <form class="form-horizontal"  id="cart_update_form" name="cart_update_form" method="post">
              {% set price = 0 %}
              {% for cdata in cart_data %}
              {% set price = price + cdata._Subtotal %}
              <div class="table-row">
                <div class="photo"> 
                 
                  {% if cdata['product']['_Image'] != '' %}
                    <a href="product.html"><img src="{{base_url()}}assets/uploads/product/{{cdata['product']._Image[0]['thumb'] }}" alt=""></a>
                  {% else %}
                    <a href="product.html"><img src="{{ constant('fronttheme') }}images/products/product-1.jpg" alt=""></a>
                  {% endif %}
                </div>
                <div class="name">
                  <a href="{{base_url('product_detail/')~cdata._ProductID}}"> {{cdata['product']['_Name']}}</a>
                </div>
                <div id="price{{cdata._ID}}" class="price" >
                 {{cdata._Unitprice}}
                </div>
                <div class="qty qty-changer">
                  <fieldset>
                    
                        <input type="hidden" name="cart_id[]" class="cart_id" data-id="{{cdata._ID}}" value="{{cdata._ID}}">
                        <input type="button" data-id="{{cdata._ID}}" value="&#8210;" class="decrease qty_change_dec">
                        <input type="text" class="qty-input" value="{{cdata._Quantity}}" data-min="0" data-max="5000" name="qty_value[]" id="qty_value{{cdata._ID}}" inc_val="{{cdata._Quantity}}">
                        <input type="button" value="+" data-id="{{cdata._ID}}" class="increase qty_change_inc">
                        <input type="hidden" name="sub_total[]" id="sub_total{{cdata._ID}}" class="sub_total" value="{{cdata._Subtotal}}">
                   
                  </fieldset>
                </div>
                <div class="subtotal cart_subtotal" id="main_price{{cdata._ID}}" subtotal="{{cdata._Subtotal}}" unit="{{cdata._Unitprice}}" price_val="{{cdata._Subtotal}}" orig_val="{{cdata._Unitprice}}">
                  {{cdata._Subtotal}}
                </div>
                <div class="remove">
                  <a href="{{base_url('remove_cart/'~cdata._ID)}}" class="icon icon-close-2"></a>
                </div>
              </div>
         
              {% endfor %}
              {% else %}
                <p>No Data.</p>
              {% endif %}
               </form>


              {% set i = 0 %}
              {% set j = 0 %}
              {% set discount = 0 %}
              {% set couponname = '' %}
              {% set cat_yes = 0 %}
              {% set ccat = coupon_data[i]._CatID %}
 
                {% for cdata in cart_data %}
                  {% if cdata['product']['_CatID'] == ccat %}
                    {% set scct = cdata['product']['_SubcatID'] %}

                   <!--  {% set cat_yes1 = cat_yes + 1 %} -->
                    <!-- {{scct}} -->
                    {{print_R(coupon_data[i]['_Subcatid'])}}
                    {% for subcat in coupon_data[i]['_Subcatid'] %}
                      {% if subcat[j] == scct %}
                        {% set cat_yes = cat_yes + 1 %}
                      {% endif %}
                    {% endfor %}
   
                {% for cdata in cart_data %}
                  {% if cdata['product']['_CatID'] == ccat %}
                    {% set scct = cdata['product']['_SubcatID'] %}
              
                 {% for scct in coupon_data[i]['_Subcatid'] %}
                    {% set cat_yes = cat_yes + 1 %}
                  {% endfor %} 
              
                    {% if cat_yes == count(coupon_data[i]['_Subcatid']) %}
                        {% set discount = price - coupon_data[i]['_Amount']  %}
                        {% set couponname = coupon_data[i]['_Code'] %}
                    {% endif %}
              
                  {% endif %}
                {% set i = i + 1 %}
                {% endfor %}
                {{cat_yes}}

                {% endfor %} 

              <div class="table-footer">
                <a href="{{base_url('productlist')}}" class="btn btn-alt">CONTINUE SHOPPING</a>
                <a href="{{base_url('emptyshopcart')}}"class="btn btn-alt pull-right"><i class="icon icon-bin"></i><span>Clear Shopping Cart</span></a>
                <a  class="update_cart">
                  <button class="btn btn-alt pull-right"><i class="icon icon-sync"></i><span>UPDATE</span></button>
                </a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 total-wrapper">
                <table class="total-price">
                  <tr>
                    <td>Subtotal</td>
                    <td>${{ price }}</td>
                  </tr>
                  <tr>
                    <td>Discount</td>
                    <td>{{ discount }}<br>({{couponname}})</td>
                  </tr>
                  {% set grandtotal = price - discount %}
                  <tr class="total">
                    <td>Grand Total</td>
                    <td id="main_cart_total" data-price="{{ price }}">${{ grandtotal }}</td>
                  </tr>
                </table>

                <div class="cart-action">
                  <div> 
                    {% if get_cookie('userid') == '' %}
                     
                      
                       <a data-toggle="modal" data-target="#modal3" style="color: #ffffff" class="btn">Proceed To Checkout</a>
                      {% else %} 
                       <a href="{{base_url('checkout')}}" class="btn" style="color: #ffffff" class="btn">Proceed To Checkout</a>
                      {% endif %} 
                  
                  </div>
                  <a href="#">Checkout with Multiple Addresses</a>
                </div>
              </div>
              <div class="col-sm-6 col-md-5">
              </div>
              <div class="col-sm-6 col-md-4">
                <h2>Discount Codes</h2>
                <form class="white" action="#">
                  <label>Enter your coupon code if you have one.</label>
                  <input type="text" class="form-control dashed" value="{{couponname}}">
                  <div>
                    <button class="btn btn-alt">Apply Coupon</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </main>
      <!-- /Page Content -->

      <div class="modal modal-countdown fade zoom info success" data-interval="10000" id="modal3">
    <div class="modal-dialog">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">&#10006;</button> -->
      </div>
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-center">
            <div class="icon-info"><i class="icon icon-alert"></i></div>
            <p><a href="{{base_url('login')}}">Please must be login</a></p>
          </div>
         
        </div>
      </div>
    </div>
  </div>

{% endblock %}