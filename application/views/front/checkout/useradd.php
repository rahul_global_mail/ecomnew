{% extends 'front/app/index.php' %}

{% block content %} 
<main class="page-main">
				<div class="block">
					<div class="container">
						<ul class="breadcrumbs">
							<li><a href="index.html"><i class="fa fa-building"></i></a></li>
							<li>/<span>My Address</span></li>
						</ul>
					</div>
				</div>
				<div class="block">
						
				<div class="address-middle">
					<div class="container">
                  <div class="blog-grid-4">
                  	{% if count(custadd) > 0 %}
								{% for caddress in custadd %}
						<div class="blog-post" data-param="{{caddress._ID}}">
							<div class="blog-content">
								<h2 class="blog-title"><a href="#">{{ caddress._Name }}</a></h2>
								<div class="blog-text">
									<!-- <div class="pull-left"> -->
									<ul class="simple-list">
                        				<li class="small"><i class="fa fa-building"></i> Address: {{ caddress._Line1 }}&nbsp;{{ caddress._Line2 }}<br></li>
		                     			<li class="small"><i class="icon icon-location"></i>{{ caddress.cities.name }}&nbsp;{{ caddress.states.name }}
		                        		<br>
		                        		India&nbsp;{{ caddress._Postcode }}</li>
                        				<li class="small"><i class="icon icon-phone"></i> Phone #: {{ caddress.users._Mobile }}</li>
                      				</ul>
									<!-- </div> -->
								</div>
							
							</div>
						</div>
						  {% endfor %}
                  {% endif %}
						
					</div>
				</div>
                </div>
<div class="block">
	<div class="container">
        <form method="POST" id="checkadd">
                <div class="row">
                <div class="col-md-6">
            
            <div class="form-group">
                <label for="addr1" class="control-label">Address Name *</label>
                <input type="text" name="aname" id="aname" class="form-control" placeholder="Address Name"  maxlength="50" required/>
            </div>
            <div class="form-group">
                <label for="addr1" class="control-label">Address Line 1</label>
                <input type="text" name="addr1" id="addr1" class="form-control" placeholder="Address Line 1"  required/>
            </div>
            <div class="form-group">
                <label for="addr2" class="control-label">Address Line 2</label>
                <input type="text" name="addr2" id="addr2" class="form-control" placeholder="Address Line 2"  required/>
            </div>
            <div class="form-group">
                <label for="zip" class="control-label">Zipcode</label>
                <input type="number" name="zip" id="zipcode" class="form-control" min="0" placeholder="Zipcode"  required/>

                <input type="hidden" name="addr_id" id="addr_id" value="">

            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="country" class="control-label">Country</label>
                <input type="text" name="country" class="form-control" placeholder="Country" value="India" required/>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1" >state</label>
                <div class="select-wrapper-sm">
                <select name="state" id="state" class="form-control">
                    <option value="">Select State</option>
                    {% if count(allstate) > 0 %}
                    {% for statedata in allstate %} 
                    <option value="{{statedata.id}}">{{statedata.name}}</option>
                    {% endfor %}
                    {% endif %} 
                </select>
            </div>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">city</label>
                <div class="select-wrapper-sm">
                <select name="city" id="city" class="form-control">
                    <option value="">Select City</option>
                    {% if count(allcity) > 0 %}
                    {% for citydata in allcity %} 
                    <option value="{{citydata.id}}">{{citydata.name}}</option>
                    {% endfor %}
                    {% endif %} 
                </select>
            </div>
                
            </div>
        </div>
       
        <div class="form-check">
        <input type="checkbox" name="other" value="save" class="form-check-input">
        <label class="form-check-label" for="exampleCheck1">save for other</label>
        
    </div>
    <input type="submit" class="btn btn-lg" value="checkout"></div>
        <!-- <a href="{{base_url('otheradd')}}" class="btn">checkout</a> -->
</form>
    </div>
</div>
</div>


						
					</div>
				<!-- </div>
				</div> -->
			</main>
{% endblock %}
{% block scripts %}
<script>
	function _sweetalert(id) {
		const del_url = base_url+'front/account/adddelete/'+id;
		Swal.fire({
		  	title: 'Are you sure?',
		  	text: "You won't be able to revert this!",
		  	icon: 'warning',
		  	showCancelButton: true,
		  	confirmButtonColor: '#3085d6',
		  	cancelButtonColor: '#d33',
		  	confirmButtonText: 'Yes, delete it!',
		  	showLoaderOnConfirm: true,
		  	preConfirm: () => {
		        $.ajax({
	                type: 'POST',
	                url: del_url,
	                success: function(response){
	                	console.log(response)
	                    var temp = JSON.parse(response);
	                    if(temp.type == 'success'){
	                        Swal.fire({
	                            title: temp.msg,
	                            type: "success",
	                            confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
	                            confirmButtonText: 'Ok',
	                            closeOnConfirm: false,
	                            onAfterClose: () => {
									location.reload();    
							  	}
	                        })
	                    }else {
	                        Swal.fire("No records found", '', "error");
	                    }
	                }
	            });
		  	}
		})
	}
	function reloadPage() {
		location.reload();
	}
	{% if this.session.flashdata.type %}
	ecommerce.notifyWithtEle('{{this.session.flashdata.message}}' , '{{this.session.flashdata.type}}' ,'topRight', 2000);
	{% endif %}
</script>
{% endblock %}