{% extends 'front/app/index.php' %}

{% block content %} 
  <!-- Loader -->
  <div id="loader-wrapper" class="off">
    <div class="cube-wrapper">
      <div class="cube-folding">
        <span class="leaf1"></span>
        <span class="leaf2"></span>
        <span class="leaf3"></span>
        <span class="leaf4"></span>
      </div>
    </div>
  </div>
  <!-- /Loader -->
  <div class="fixed-btns">
    <!-- Back To Top -->
    <a href="#" class="top-fixed-btn back-to-top"><i class="icon icon-arrow-up"></i></a>
    <!-- /Back To Top -->
  </div>

    <!-- Page Content -->
     <main class="page-main">
        <div class="block">
          <div class="container">
            <ul class="breadcrumbs">
              <li><a href="index.html"><i class="icon icon-home"></i></a></li>
              <li>/<span>Gallery</span></li>
            </ul>
          </div>
        </div>
        <div class="block fullwidth full-nopad">
          <div class="container">
            <div class="title center">
              <h1 class="size-lg">Gallery</h1>
            </div>
           <!--  <ul class="filters filters-gallery">
             <li><a href="#" class="filter-label">All</a><span>/</span></li>
             <li><a href="#" class="filter-label" data-filter=".category1">Category1</a><span>/</span></li>
             <li><a href="#" class="filter-label" data-filter=".category2">Category2</a><span>/</span></li>
             <li><a href="#" class="filter-label" data-filter=".category3">Category3</a><span>/</span></li>
           </ul> -->
            <div class="row">
              <div class="gallery-wrapper">
                <div class="gallery isotope">
                   {% if count(galldetail) > 0 %}
                 

                     {% for gdata in galldetail %} 
                      {% for imgdetail in gdata['_Image'] %} 
                  <div class="col-xs-4 gallery-item effect category3">
                   
                    <div class="image"><img class="img-responsive" src="{{base_url()}}assets/uploads/gallery/{{imgdetail.name}}" alt=""></div>
                    
                    <div class="caption">
                      <div class="vert-wrapper">
                        <div class="vert">
                          <div class="title"><span style="padding: 0.05em 0.2em; background-color: rgba(26,28,36,0.7); font-weight: 100;">{{gdata._Title}}</span></div>
                          <div class="text">
                            <p>Nam libero tempore, cum soluta nobis est eligendi optio cumque </p>
                            <p>Nompedit quo minus id quod maxime</p>
                          </div>
                        </div>
                      </div>
                      <div class="link">
                        <a href="{{base_url()}}assets/uploads/gallery/{{imgdetail.name}}" class="zoomimage"><i class="icon icon-plus"></i></a>
                        <a href="#"><i class="icon icon-arrow-right"></i></a>
                      </div>
                    </div>
                  </div>
                
                  {% endfor %}
               {% endfor %}
                    {% endif %}

                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
      <!-- /Page Content -->

{% endblock %}