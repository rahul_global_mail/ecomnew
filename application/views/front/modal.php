<!-- SIMPLE AJAX MODAL -->
<script type="text/javascript">
    function showAjaxModal(url,title)
    {
        // SHOWING AJAX PRELOADER IMAGE
        jQuery('#modal_ajax .modal-body').html('<div style="text-align:center;"><img src="<?php echo IMG_PATH;?>loading.gif" style="max-width: 150px;"/></div>');

        // LOADING THE AJAX MODAL
        jQuery('#modal_ajax').modal('show', {backdrop: 'true'});
        
        // SHOW AJAX RESPONSE ON REQUEST SUCCESS
        $.ajax({
            url: url,
            success: function(response)
            {
                jQuery('#modal_ajax .modal-title').html("");
                jQuery('#modal_ajax .modal-body').html("");
                jQuery('#modal_ajax .modal-title').html('<strong>' + title + '</strong>');
                jQuery('#modal_ajax .modal-body').html(response);
            }
        });
    }
    function swadel(module,param,func) {
        var url = module;
        var params = param;
        var func = func;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "error",
            showCancelButton: true,
            cancelButtonClass: 'btn-white btn-md waves-effect',
            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
            confirmButtonText: 'Delete!',
            closeOnConfirm: false
        },
        function(){
            var del_url = "admin/"+url+"/";
            if(func){
                del_url += func;
            }else{
                del_url += "delete/";
            }
            var data = params.split('/');
            $.ajax({
                type: 'POST',
                url: del_url,
                data: {
                    prms:data[0],
                    rtrn_id:data[1]
                },
                success: function(response){
                    var temp = JSON.parse(response);
                    console.log(temp);
                    if(temp.status == 'success'){
                        swal({
                            title: url.charAt(0).toUpperCase() + url.substr(1) + " deleted!",
                            type: "success",
                            confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                            confirmButtonText: 'Ok',
                            closeOnConfirm: false
                        },
                        function(){
                            window.location.href = temp.redirect_url;
                        });
                    }else {
                        swal("No records found", '', "error");
                    }
                }
            });
        });
    }
</script>
 <div class="modal fade" id="modal_ajax" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div> 


