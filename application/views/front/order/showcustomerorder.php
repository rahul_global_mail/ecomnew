{% extends 'front/app/index.php' %}

{% block content %} 
<main class="page-main">
        <div class="block">
          <div class="container">
            <ul class="breadcrumbs">
              <li><a href="index.html"><i class="icon icon-home"></i></a></li>
              <li>/<span>Order Details</span></li>
            </ul>
          </div>          
        </div>
        <div class="panel">
        <div><h2><p style="font-size: 30px;font-weight: bold;text-align: center;">Thank you !!</p></h2></div>
        <!-- <div class="block"> -->

          <div class="row">
            <div class="panel-body">
         <!-- <div class="col-xs-6 col-sm-3">   -->
          <ul class="simple-list">
                          <br>
                         <li> <b>Order ID:</b>{{orderinfo._Orderid}}<br></li>
                          <li><b>Payment Status:</b> {% if (orderinfo._Paymentstatus)==0 %}Pending
                          {% elseif(orderinfo._Paymentstatus)==1 %}Paid

        </div>

        <div>
            <h2>
                <p style="font-size: 30px;font-weight: bold;text-align: center; color:#f82e56;">Thank you !!</p>
            </h2>
        </div>

      <div class="block">
          <div class="container">
            <div class="row">
              <div class="col-md-12 col-sm-12">

                <div class="panel panel-default">
                  <div class="panel-heading">
                         <ul class="simple-list">
                        <br>
                         <li> <b>Order ID:</b>{{orderinfo[0]._Orderid}}<br></li>
                          <li><b>Payment Status:</b> {% if (orderinfo[0]._Paymentstatus)==0 %}Pending
                          {% elseif(orderinfo[0]._Paymentstatus)==1 %}Paid
                          {% else %}Refund
                          {% endif %}</li>

                          <li><b>Order Status:</b> {% if (orderinfo._Orderstatus)==0 %}Pending
                          {% elseif(orderinfo._Orderstatus)==1 %}Approved
                          {% elseif(orderinfo._Orderstatus)==2 %}Shipping
                          {% elseif(orderinfo._Orderstatus)==3 %}Deliverd
                          {% else %}Cancelled
                          {% endif %}<br></li>
                          <li><b>Order Date:</b> {{orderinfo._Orderdate|date('d/m/Y')}}
                        </li>
                         <!-- </div>  -->
                       </ul>
                       </div>
         </div> 
         </div>
        </div>
      <!-- </div> -->
      
        <div class="block">
          <div class="container">
            <div class="cart-table">
                          <li><b>Order Status:</b> {% if (orderinfo[0]._Orderstatus)==0 %}Pending
                          {% elseif(orderinfo[0]._Orderstatus)==1 %}Approved
                          {% elseif(orderinfo[0]._Orderstatus)==2 %}Shipping
                          {% elseif(orderinfo[0]._Orderstatus)==3 %}Deliverd
                          {% else %}Cancelled
                          {% endif %}<br></li>
                          <li><b>Order Date:</b> {{orderinfo[0]._Orderdate|date('d/m/Y')}}
                        </li>
                         <!-- </div>  -->
                       </ul>
                  </div>

                  <!-- List group -->
                  
                  <div class="panel-body">
                    <div class="cart-table">

              <div class="table-header">
                <div class="photo">
                  Product Image
                </div>
                <div class="name">
                   Name
                </div>
                <div class="qty">
                   Quantity
                </div>
                <div class="price">
                  Price
                </div>
                <div class="subtotal">
                  Total
                </div>
              </div>
              {% if orderinfo|length > 0 %}
              {% set total = 0 %}
              {% set subtotal = 0 %}
              {% set discount = 12.00 %}

              {% for rowdata in orderinfo['product'] %}

              <div class="table-row">
              
                <div class="photo">
                  <a href="">
                    <img src="{{base_url()}}assets/uploads/product/{{rowdata._Image[0]['thumb']}}" alt="">
                  </a>
                </div>
                <div class="name">
                  <a href="{{base_url('product_detail/')~rowdata._ID}}">{{rowdata._Name}}</a>
                </div>
              
                <div class="price">
                  {{rowdata['pivot'].quantity}}
                </div>
              
                <div class="price">
                  ${{rowdata['pivot'].price}}
                </div>
                
                <div class="subtotal">
                  {% set total = rowdata['pivot'].quantity * rowdata['pivot'].price %}
                  {% set subtotal = total + subtotal %}
                  {{total}}

              <!-- {{print_r(orderinfo[0]['orderdetail'][0]['product'])}} -->
              {% for rowdata in orderinfo[0].orderdetail %}

              <div class="table-row">
                <div class="photo">
                  <a href="">
                    <img src="{{base_url()}}assets/uploads/product/{{rowdata['product']._Image[0]['thumb']}}" alt="">
                  </a>
                </div>
                <div class="name">
                  <a href="{{base_url('product_detail/')~rowdata['product']._ID}}">{{rowdata['product']._Name}}</a>
                </div>
              
                <div class="price">
                  {{rowdata._Quantity}}
                </div>
              
                <div class="price">
                  ${{rowdata._Unitprice}}
                </div>
                
                <div class="subtotal">
                  ${{rowdata._Subtotal}}

                </div>
              </div>
              {% endfor %}
           {% endif %}
              
            </div>

             {% set i = 0 %}
              {% set discount = 0 %}
              {% set couponname = '' %}
              {% set cat_yes = 0 %}
              {% set ccat = coupon_data[i]._CatID %}
 
                {% for cdata in orderinfo['product'] %}
                  {% if cdata['_CatID'] == ccat %}
                    {% set scct = cdata['_SubcatID'] %}

                  
                    
                    {% for scct in coupon_data[i]['_Subcatid'] %}
                      {% set cat_yes = cat_yes + 1 %}
                    {% endfor %}
   
                    {% if cat_yes == coupon_data[i]['_Subcatid']|length %}
                        {% set discount = subtotal - coupon_data[i]['_Amount']  %}
                        {% set couponname = coupon_data[i]['_Code'] %}
                   
                       
                    {% endif %}

                  {% endif %}
                {% set i = i + 1 %}
                {% endfor %}

                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      
        <div class="block">
          <div class="container">
            
            <div class="row">
              <div class="col-md-3 total-wrapper">
                <table class="total-price">
                  <tr>
                    <td>Subtotal</td>
                    <td>${{subtotal}}</td>
                  </tr>
                  <tr>
                    <td>Discount</td>
                     <td>{{ discount }}<br>({{couponname}})</td>
                  </tr>
                   {% set grandtotal = subtotal - discount %}
                  <tr class="total">
                    <td>Grand Total</td>
                    <td>${{grandtotal}}</td>
                    <td>${{orderinfo[0]._Subtotal}}</td>
                  </tr>
                  <tr>
                    {% if orderinfo[0]._Discount != 0 %}
                     <td>Discount</td>
                       <td><br>
                       ({{orderinfo[0].coupon._Code}})
                      ({{orderinfo[0]._Discount}})</td>
                      {% endif %}
                  </tr>
                  <tr class="total">
                    <td>Grand Total</td>
                    <td>${{orderinfo[0]._Grandtotal}}</td>
                  </tr>
                  <tr>

                    <td colspan="2">
                     <a href="{{base_url('showorders')}}" class="btn">View order</a>
                   </td>
                  </tr>
                </table>
              </div>
             <!--  <div class="col-md-6">
             
             </div> -->
              </div>
            </div>
          </div>
        </div>
      </main>
{% endblock %}