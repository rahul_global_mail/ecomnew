{% extends 'front/app/index.php' %}

{% block content %} 
  <main class="page-main">
        <div class="block">
          <div class="container">
            <ul class="breadcrumbs">
              <li><a href="index.html"><i class="icon icon-home"></i></a></li>

              <li>/<span>{{page._Title}}</span></li>
            </ul>
          </div>
        </div>
        <div class="block">
          <div class="container">
            <div class="title center">
              <h1>{{page._Title}}</h1>
            </div>
          </div>
        </div>
        <div class="block fullboxed parallax" data-parallax="scroll" data-image-src="images/block-bg-1.jpg">
          <div class="container">
            <div class="row">
              <div class="col-sm-12 text-wrapper text-lg">
                 <p>{{page._Content|raw }}</p>
              </div>

            <!--   <div class="col-sm-6">
              <img src="images/logo-big.png" alt class="img-responsive" />
            </div>
            <div class="col-sm-6">
              <div class="text-wrapper text-lg">
                <p>{{page._Content|raw }}</p>
               
              </div>
            </div> -->
            </div>
          </div>
        </div>
      
      </main>
{% endblock %}