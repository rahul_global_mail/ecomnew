{% extends 'front/app/index.php' %}

{% block content %} 
  <!-- Loader -->
  <div id="loader-wrapper" class="off">
    <div class="cube-wrapper">
      <div class="cube-folding">
        <span class="leaf1"></span>
        <span class="leaf2"></span>
        <span class="leaf3"></span>
        <span class="leaf4"></span>
      </div>
    </div>
  </div>
  <!-- /Loader -->
  <div class="fixed-btns">
    <!-- Back To Top -->
    <a href="#" class="top-fixed-btn back-to-top"><i class="icon icon-arrow-up"></i></a>
    <!-- /Back To Top -->
  </div>

    <!-- Page Content -->
      <main class="page-main">
        <div class="block">
          <div class="container">
            <ul class="breadcrumbs">
              <li><a href="index.html"><i class="fa fa-building"></i></a></li>
              <li>/<span>Payment</span></li>
            </ul>
          </div>
        </div>
         <div class="block">
         <div class="panel">
          <div class="row">
     
          <div><label class="col-form-label"><i class='fa fa-credit-card'></i><h4><span style="margin-left:23px;">Select your mode of payment</span></h4></label></div>
          <div class="panel-body">
            <form id="payment_form" method="post">
            <div class="row">
             
            {% if paydata|length > 0 %}
            {% for pdata in paydata %}
             <div class="col-md-9">
            <div class="form-check">
              <ul style="list-style-type:none">
                <li>
            <input type="radio" value="{{pdata._ID}}" name="pay" class="form-check-input pay">
            <label class="form-check-label">{{pdata._Name}}</label></li>
            <br>
            <ul>
          </div>
        </div>

         
            {% endfor %}
            {% endif %}



            <div class="col-md-3" style="display: none;" id="tprice">
                <table class="total-price">
                  <tr class="total">
                    <td>SubTotal</td>
                    <td id="main_cart_total" data-price="{{ allProd.total }}">${{ new_order_data[0]['_Subtotal'] }}</td>
                  </tr>
                  <tr>
                    {% if new_order_data[0]['_Discount'] != 0 %}
                    <td>Discount</td>
                    <td>

                        <br>(${{new_order_data[0]['_Discount']}})

                        <br>({{new_order_data[0]['coupon']['_Code']}})(${{new_order_data[0]['_Discount']}})

                    </td>
                    {% endif %}
                  </tr>

                  <tr class="total">
                    <td>Grand Total</td>
                    <td id="main_cart_total" data-price="{{ price }}">${{ new_order_data[0]['_Grandtotal'] }}</td>
                  </tr>
                </table>
                <input type="hidden" name="total" id="total" value="{{grandtotal}}">
                <input type="hidden" name="logid" id="logid" value="{{loguser_id}}">
                <input type="hidden" name="orderid" id="orderid" value="{{new_order_data[0]['_Orderid']}}">
                <div class="cart-action">
                  <div>
                     <input type="submit" class="btn btn-lg" value="Place Order">
                  
                  </div>
                  
                </div>
              </div>
            </div>
          </form>
             </div>
            
           
         </div>
             </div> 
        
      </main>
      <!-- /Page Content -->

{% endblock %}