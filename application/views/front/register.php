{% extends 'front/app/index.php' %}

{% block content %} 
<main class="page-main">
	<div class="block">
		<div class="container">
			<ul class="breadcrumbs">
				<li><a href="index.html"><i class="icon icon-home"></i></a></li>
				<li>/<span>Register</span></li>
			</ul>
		</div>
	</div>
	<div class="block">
		<div class="container">
			<div class="form-card">
				<h3>Personal Information</h3>
				<form class="account-create" id="register_form" method="post">
					<label>First Name<span class="required">*</span></label>
					<input type="text" class="form-control input-lg" name="fname">
					<label>Last Name<span class="required">*</span></label>
					<input type="text" class="form-control input-lg" name="lname">
					<label>Mobile No<span class="required">*</span></label>
					<input type="text" class="form-control input-lg" name="mno" id="mno">
					<label>E-mail<span class="required">*</span></label>
					<input type="text" class="form-control input-lg" name="email" id="email">
					<label>Password<span class="required">*</span></label>
					<input type="password" class="form-control input-lg" name="pass">
					<div>
						<input type="submit" class="btn btn-lg" value="Register"><span class="required-text">* Required Fields</span></div>
					<div class="back">or <a href="#">Return to Store <i class="icon icon-undo"></i></a></div>
				</form>
			</div>
		</div>
	</div>
</main>
{% endblock %}