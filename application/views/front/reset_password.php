{% extends 'front/app/index.php' %}

{% block content %} 
<main class="page-main">
	<div class="block">
		<div class="container">
			<ul class="breadcrumbs">
				<li><a href="index.html"><i class="icon icon-home"></i></a></li>
				<li>/<span>Reset Password</span></li>
			</ul>
		</div>
	</div>
	<div class="block">
		<div class="container">
			<div class="form-card">
				<h3>Reset Password</h3>
				<form class="account-create" id="reset_form" method="post" >
					<input type="hidden" name="reset_token" value="{{reset_token}}">
					<label>Password<span class="required">*</span></label>
					<input type="password" class="form-control input-lg" name="pass" id="password">
					<label>Confirm Password<span class="required">*</span></label>
					<input type="password" class="form-control input-lg" name="cpass">
					<div>
						<input type="submit" class="btn btn-lg" value="Change Password"><span class="required-text">* Required Fields</span></div>
					<div class="back">or <a href="#">Return to Store <i class="icon icon-undo"></i></a></div>
				</form>
			</div>
		</div>
	</div>
</main>
{% endblock %}