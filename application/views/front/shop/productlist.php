{% extends 'front/app/index.php' %}

{% block content %} 
  <!-- Loader -->
  <div id="loader-wrapper" class="off">
    <div class="cube-wrapper">
      <div class="cube-folding">
        <span class="leaf1"></span>
        <span class="leaf2"></span>
        <span class="leaf3"></span>
        <span class="leaf4"></span>
      </div>
    </div>
  </div>
  <!-- /Loader -->
  <div class="fixed-btns">
    <!-- Back To Top -->
    <a href="#" class="top-fixed-btn back-to-top"><i class="icon icon-arrow-up"></i></a>
    <!-- /Back To Top -->
  </div> 
  <main class="page-main">
        <div class="block">
          <div class="container">
            <ul class="breadcrumbs">
              <li><a href="index.html"><i class="icon icon-home"></i></a></li>
              <li>/<span>MEN’S</span></li>
            </ul>
          </div>
        </div>
        <div class="container">
          <!-- Two columns -->
          <div class="row">
            <!-- Left column -->
            <div class="col-md-3 filter-col aside">
              <div class="fixed-wrapper">
                <div class="fixed-scroll">
                  <div class="filter-col-header">
                    <div class="title">Filters</div>
                    <a href="#" class="filter-col-toggle"></a>
                  </div>
                  <h2>Shoping By</h2>
                  <div class="filter-col-content">
                    {% if count(category) > 0 %}
                     {% for cdata in cat_data %} 
                    <div class="sidebar-block " id="category_change">
                      <div class="block-title">
                       <!--  <span>{{cdata._Name}}</span> -->
                        <div class="toggle-arrow"></div>
                      </div>
                      <div class="block-content">
                        <ul class="category-list">
                          {% for cat in category %}
                          {% if cat._Main_id == cdata._ID %} 
                          <li ><a href="#" class="sli_category" id="scategory" sub_cat='{{cat._ID}}'>{{cat._Name}}</a>
                            <a href="#" class="clear remove_scategory" rm_sub_cat='{{cat._ID}}'></a>
                          </li>
                          {% endif %} 
                          {% endfor %}
                        </ul>
                        <div class="bg-striped"></div>
                      </div>
                    </div>
                    {% endfor %}
                    {% endif %}
                    
                      <input name="hcatid" id="hcatid" type="hidden"> 
                    <div class="sidebar-block collapsed" id="priceblock">
                      <div class="block-title">
                        <span>Price</span>
                        <div class="toggle-arrow"></div>
                      </div> 
                      <div class="block-content">
                        <div class="price-slider-wrapper">
                          <div class="price-values">
                            <div class="pull-left">$<span id="priceMin"></span></div>
                            <div class="pull-right">$<span id="priceMax"></span></div>
                          </div>
                          <div id="priceSlider" class="price-slider"></div>
                        </div>
                        <div class="bg-striped"></div>
                      </div>
                    </div>

                      <div class="sidebar-block collapsed" id="sizeblock">
                     <div class="block-title">
                       <span >Size</span>
                       <div class="toggle-arrow"></div>
                     </div>
                     <div class="block-content">
                       <ul class="size-list">
                        <div class="size"></div>
                       </ul>
                       <div class="bg-striped"></div>
                     </div>
                                       </div> 
                   <div id="size_fil" style="display:none" ></div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /Left column -->
            <!-- Center column -->
            <div class="col-md-9 aside">
              <!-- Page Title -->
              <div class="page-title">
                <div class="title center">
                  <h1>MEN’S</h1>
                </div>
              </div>
              <!-- /Page Title -->
            
              <!-- /Categories -->
              <!-- Filter Row -->
              <div class="filter-row">
                <div class="row">
                  <div class="col-xs-8 col-sm-7 col-lg-5 col-left">
                  	<div class="view-mode">
          						<a href="javascript:void(0);" class="a-z-short" data-tooltip="Sort By Name"><i class="fa fa-sort-alpha-asc"></i></a>
          						<a href="javascript:void(0);" class="z-a-short" data-tooltip="Sort By Name"><i class="fa fa-sort-alpha-desc"></i></a>
          						<a href="javascript:void(0);" class="l-h-short" data-tooltip="Sort By Price"><i class="fa fa-sort-amount-desc"></i></a>
          						<a href="javascript:void(0);" class="h-l-short" data-tooltip="Sort By Price"><i class="fa fa-sort-amount-asc"></i></a>
				           </div>
                  </div>
                  	<div class="col-xs-4 col-sm-5 col-lg-7 col-right">
						<div class="view-mode">
							<a href="#" class="grid-view" data-tooltip="Grid View"><i class="icon icon-th"></i></a>
							<a href="#" class="list-view" data-tooltip="List View"><i class="icon icon-th-list"></i></a>
						</div>
					</div>
                </div>
                <div class="bg-striped"></div>
              </div>
              <!-- /Filter Row -->
              
              <!-- Products Grid -->
              <div class="products-grid three-in-row product-variant-5 items" >
                <div id="before_search">
                  {% if products | length > 0 %}

              {% for product in products %}
              {% if product.product | length > 0 %}
                <!-- Product Item -->
                <div class="product-item large category1" >
                  <div class="product-item-inside">
                    <div class="product-item-info">
                      <!-- Product Photo -->
                        <div class="product-item-photo">
                          {% if product.product.is_sale == 1 %}
                          <div class="product-item-label label-sale"><span>Sale</span></div>
                          {% endif %}
                          {% if product.product.is_new == 1 %}
                          <div class="product-item-label label-new"><span>New</span></div>
                          {% endif %}
                          
                          
                          {% if product.product['_Image'] | length == 0 %}
                           
                            <div class="product-item-gallery-main">
                              <a href="{{base_url('product_detail/')~product.product._ID}}">
                                <img class="product-image-photo" src="assets/front/images/products/product-1.jpg" alt="">
                              </a>
                            </div>
                             {% endif %}
                           {% if product.product['_Image'] | length == 1 %}
            								<div class="product-item-gallery-main">
                          		<a href="{{base_url('product_detail/')~product.product._ID}}">
                          			<img class="product-image-photo" src="assets/uploads/product/{{ product.product['_Image'][0]['cat'] }}" alt="">
                          		</a>
                        	  </div>
                             {% endif %}
                               {% if product.product['_Image'] | length > 1 %}
                    <div class="carousel-inside slide" data-ride="carousel">
                      <div class="carousel-inner" role="listbox">
                        {% set i = 0%}
                        {% for image in product.product['_Image'] %}

                        {% if(i == 0) %}
                          <div class="item active">
                            <a href="{{base_url('product_detail/')~product.product._ID}}">
                              <img class="product-image-photo" src="assets/uploads/product/{{ image['cat'] }}" alt="" />
                            </a>
                          </div>
                        {% else %}
                          <div class="item">
                            <a href="{{base_url('product_detail/')~product.product._ID}}">
                              <img class="product-image-photo" src="assets/uploads/product/{{ image['cat'] }}" alt="" />
                            </a>
                          </div>
                        {% endif %}
                        {% set i = i + 1 %}
                        {% endfor %}
                      </div>
                      <a class="carousel-control next"></a>
                      <a class="carousel-control prev"></a>
                    </div>
                  {% endif %}
              		<a href="{{base_url('quick_view/')~product.product._ID}}" title="Quick View" class="quick-view-link quick-view-btn">
              			<i class="icon icon-eye"></i><span>Quick View</span>
              		</a>

                  {% if get_cookie('userid') == '' %}

                  <a onclick ="ecommerce.notifyWithtEle('Please login to add product in your wishlist' , 'error' ,'topRight', 2000);" title="Add to Wishlist" class="no_wishlist add"> <i class="icon icon-heart"></i><span>Add to Wishlist</span> </a>
                  
                  {% else %}

                  {% if product.product.wishlists | length > 0 %}
                    <a onclick ="ecommerce.notifyWithtEle('This product is already exist in wishlist' , 'success' ,'topRight', 2000);"  title="In Wishlist" class="no_wishlist" style="color:#7e57c2 !important;"> <i class="icon icon-heart"></i><span>In Wishlist</span> </a>
                  {% else %} 
                    <a href="{{base_url('addwishlist/')~product.product._ID}}" title="Add to Wishlist" class="no_wishlist add" data-id="{{product.product._ID}}"> <i class="icon icon-heart"></i><span>Add to Wishlist</span> </a>
                   {% endif %} 
                  {% endif %}
						              </div>

                      <!-- /Product Photo -->
                      <!-- Product Details -->
                          <div class="product-item-details">
                            <div class="product-item-name"> <a title="{{product.product._Name}}" href="{{base_url('product_detail/')~product.product._ID}}" class="product-item-link">{{product.product._Name}}</a></div>
                            <div class="product-item-description">{{product.product._Description}}</div>
                            <div class="price-box"> <span class="price-container"> 

                        <span class="price-wrapper">
                        {% if product._Sellprice > 0 %}
                        <span class="price-wrapper">
                          <span class="old-price">${{ product._Price }}</span>
                          <span class="special-price">${{ product._Sellprice }}</span>
                        </span>
                        {% else %}
                        <span class="price">
                          ${{ product._Price }}
                        </span>
                        {% endif %}
                      </span>
                              </span>
                            </div>
                            <div class="product-item-rating"> <i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i></div>
                            <a href="{{base_url('addtocart_shop/')~product.product._ID}}">
                            <button class="btn add-to-cart" data-product="{{product.product._ID}}"> <i class="icon icon-cart"></i><span>Add to Cart</span> </button></a>
                          </div>
                      <!-- /Product Details -->
                      </div>
                    </div>
                  </div>
                  {% endif %}
                  {% endfor %}
                 {% endif %}
                 </div>
                <!-- /Product Item -->
                <div id="after_search" style="display: none;">
                   
                </div>

              </div>
				

            </div>
            <!-- /Center column -->
          </div>
          <!-- /Two columns -->
        </div>
      </main>
      <div class="modal modal-countdown fade zoom info success" data-interval="10000" id="modal3">
    <div class="modal-dialog">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">&#10006;</button> -->
      </div>
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-center">
            <div class="icon-info"><i class="icon icon-alert"></i></div>
            <p><a href="{{base_url('login')}}">Please must be login</a></p>
          </div>
         
        </div>
      </div>
    </div>
  </div>


{% endblock %}

{% block scripts %}

<script type="text/javascript">
	var $grid_short = $('.items').isotope({
	    itemSelector: '.product-item',
	    getSortData: {
	        name: '.product-item-link',
	        number: '.price parseInt',
	    }
	});
	$('.grid-view').on("click", function (e) {
		$('.items').removeClass('products-listview').addClass('products-grid');
		$grid_short.isotope({layoutMode: "fitRows"});
		e.preventDefault();
	});
	$('.list-view').on("click", function (e) {
		$('.items').removeClass('products-grid').addClass('products-listview');
		$grid_short.isotope({layoutMode: "vertical"});
		e.preventDefault();
	});
	$('.a-z-short').on("click", function (e) {
		$grid_short.isotope({ sortBy: "name",sortAscending: true });
		e.preventDefault();
	});
	$('.z-a-short').on("click", function (e) {
		$grid_short.isotope({ sortBy: "name",sortAscending: false });
		e.preventDefault();
	});
	$('.l-h-short').on("click", function (e) {
		$grid_short.isotope({ sortBy: "number",sortAscending: false });
		e.preventDefault();
	});
	$('.h-l-short').on("click", function (e) {
		$grid_short.isotope({ sortBy: "number",sortAscending: true });
		e.preventDefault();
	});
	var per_page = 4;
	var start = 4;
	var url = 'category/load_more/';
	$('#load-data').click(function(e) {
	    e.preventDefault();
	    var l = Ladda.create(this);
	    l.start();
	    $.ajax({
	        type: 'POST',
	        url: url,
	        data: {
	        	param: "2-desktop",
	            start: start
	        },
	        success: function(response) {
	            setTimeout(function() {
	            	if(response == 0){
	            		$('.no-result,#load-data').toggle();
	            	}else {
	            		newItems = $(response).appendTo('.items');
						$grid_short.isotope('appended', newItems );
						start = (start + per_page);
	            	}
	                l.stop();
	            }, 100);
	        }
	    });
	});


</script>
<script src="{{ constant('cmstheme') }}js/product.js"></script>
{% endblock %}