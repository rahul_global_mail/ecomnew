var base_url = $("#myBase").attr('href');
var $document = $('document');

$(function() {
    ecommerce.init();
}); 


ecommerce = {
  	init:function () { 
    	ecommerce._loginvalidate();
    	ecommerce._profilevalidate();
    	ecommerce._extrafunction();
	},
    _extrafunction:function(){  

     $('#review_tab,#user_tab,#category_tab,#scategory_tab,#coupon_tab,#payment_tab,#wish_tab,#order_tab').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });

    bsCustomFileInput.init();
    $('.select2').select2()

    $document.on('change', '#state', function () {
		var state_id = $(this).val();
		alert(state_id);
        ecommerce.ajax_req({id: state_id},"getcity").done(function (response) {
        	var res = $.parseJSON(response);
            $("#city").empty();
            $("#city").append("<option value=''>Please select</option>");
            $.each(res,function (index,value){
                $("#city").append("<option value='"+value.id+"' > "+value.name+" </option>");
            });
         })
    })

  

    },

    _loginvalidate: function () {
        $("#login-form").validate({
            rules: {
                username: {
                    required: true,
                },
                pass: {
                    required:true,
                },
            },
            messages: {
                username: {
                    required: "Please Enter Username",
                },
                pass: {
                    required:"Please Enter Password"
                },
               
            },
            errorClass: "error-class",
            errorElement: "div",
            errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
           unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
            submitHandler: function (form) {
              
        ecommerce.ajax_req(($("#login-form").serialize()), 'do_login').done(function (response) {
                    var res = $.parseJSON(response);

                    if(res.type == 'success'){
                      ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;
                    }else if(res.type == 'warning'){
                      ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                      window.location = base_url+res.url;
                    }else{
                      ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                    }
          });
          return false;
      }
        });
    },

    _profilevalidate: function () {
        $("#profile_form").validate({
            rules: {
                fname: {
                    required: true,
                },
                lname: {
                    required: true,
                },
                email: {
                    required: true,
                    email:true,
                },
                address: {
                    required: true,
                },
                mobile: {
                    required: true,
                    minlength: 10
                },
                username:{
                    required:true,
                },
                curpass: {
                    required:true,
                },
                /*newpass: {
                    required:true,
                    equalTo : "#curpass"
                },*/
            },
            messages: {
                fname: {
                    required: "Please Enter FirstName",
                },
                lname:{
                    required: "Please Enter LastName",
                },
                email:{
                    required: "Please Enter Email",
                    email:"Please Enter valid Email"
                },
                address:{
                    required:"Please Enter Address",
                },
                mobile:{
                    required:"Please Enter mobile",
                    
                },
                username:{
                    required:"Please Enter username",
                },
                curpass: {
                    required:"Please Enter Password"
                }, 
                /*newpass: {
                    required:true,
                    required:"Enter New Password Same as Current Password"
                },*/
               
            },
            errorClass: "error-class",
            errorElement: "div",
            errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
           unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
            submitHandler: function (form) {
              
        ecommerce.ajax_req(($("#profile_form").serialize()), 'makeprofile').done(function (response) {
                    var res = $.parseJSON(response);

                    if(res.type == 'success'){
                      ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;
                    }else if(res.type == 'warning'){
                      ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                      window.location = base_url+res.url;
                    }else{
                      ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                    }
          });
          return false;
      }
        });
    },

   
  notifyWithtEle: function (msg,type,pos,timeout) {
        pos = "";
    timeout = "";
        var noty = new Noty({
      theme:'metroui',
      text: msg,
      type: type,
      layout: (pos != "") ? pos : 'topRight',
      timeout: (timeout != "") ? timeout : 2000,
      closeWith: ['click'],
      animation: {
        open: 'animated slideInRight',
        close: 'animated slideOutRight'
      }

    });
        noty.show();
    },

    ajax_req: function(fields, url) {
      return $.ajax({
          url:base_url+url,
          type:'POST',
          data: fields,
          datatype : "application/json"
      });
    },

    
    _payment:function(){
         $.validator.addMethod("validatepname", function(value, element, jdata) {
            var x = $.ajax({
                type: "POST",
                url: base_url + "verifypayment",
                async: false,
                dataType: "json",
                data: jdata,
            }).responseText;
        return (x === 'false') ? false : true;
        }, "Payment name is already available.");
      

        $("#payment_form").validate({
           onkeyup: false,
           onclick: false,
           onfocusout: false,
            rules: {
                payname: {
                    required: true, 
                    validatepname: {
                    payname: function(){
                        return $("#payname").val();
                    },
                    _payname: function(){
                        return $("#_payname").val();
                    }
                }  
                },   
                paydes: {
                    required: true,
                }
            },
            messages: {
                payname: {
                  required: "Please Enter Category",
                },
                paydes: {
                    required: "Please Enter Description",
                }
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                error.addClass( "help-block" );
                error.insertAfter( element );
                
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
         submitHandler: function (form) {
            
                var form_data =  $('#payment_form').serialize();
                var hpayid = $("#hpayid").val();

                if(hpayid)
                {
                    $.ajax({
                            url: base_url + 'updatepayment', // point to server-side controller method
                            datatype : "application/json", // what to expect back from the server
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                                 var res = $.parseJSON(response);
                                if(res.type == 'success'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else if(res.type == 'warning'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else{
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        }
                            },
                            
                        });
                    return false;
                }
                else
                {
                   
                    $.ajax({
                            url: base_url + 'addpayment', // point to server-side controller method
                            datatype : "application/json", // what to expect back from the server
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                                 var res = $.parseJSON(response);
                                if(res.type == 'success'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else if(res.type == 'warning'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else{
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        }
                            },
                            
                        });
                    return false;
                }
            }
        })

    },

    _delpayment:function(){

        $(".delpayment").click(function() {
        var delid = parseInt($(this).attr('del_payid'));
            const del_url = base_url+'delpayment/'+delid;
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    showLoaderOnConfirm: true,
                    preConfirm: () => {
                    $.ajax({
                            type: 'POST',
                            url: del_url,
                            success: function(response){
                                var temp = JSON.parse(response);
                                    if(temp.type == 'success'){
                                        Swal.fire({
                                                title: temp.msg,
                                                type: "success",
                                                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                                                confirmButtonText: 'Ok',
                                                closeOnConfirm: false,
                                        })
                                    }else {
                                        Swal.fire("No records found", '', "error");
                                    }
                            }
                        });
                    }
        })
          
        });
    },

    _pdetail:function(){
        $("#pdetail_form").validate({
           onkeyup: false,
           onclick: false,
           onfocusout: false,
            rules: {
                key: {
                    required: true, 
                },   
                value: {
                    required: true,
                }
            },
            messages: {
                key: {
                  required: "Please Enter key",
                },
                value: {
                    required: "Please Enter value",
                }
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                error.addClass( "help-block" );
                error.insertAfter( element );
                
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
         submitHandler: function (form) {
            
                var form_data =  $('#pdetail_form').serialize();
                    $.ajax({
                            url: base_url + 'addpdetail_form', // point to server-side controller method
                            datatype : "application/json", // what to expect back from the server
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                                 var res = $.parseJSON(response);
                                if(res.type == 'success'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else if(res.type == 'warning'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else{
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        }
                            },
                            
                        });
                    return false;
               
            }
        })

    },

    showAjaxModal: function (url,title,size) {
        // SHOWING AJAX PRELOADER IMAGE
        jQuery('#modal_ajax .modal-body').html('<div style="text-align:center;"><img src="http://localhost/storeapp/assets/images/loading.gif" style="max-width: 150px;"/></div>');

        // LOADING THE AJAX MODAL
        jQuery('#modal_ajax').modal('show', {backdrop: 'true'});
    	
    	var modelSize = (size) ? size : '';
        // SHOW AJAX RESPONSE ON REQUEST SUCCESS
        $.ajax({
            url: url,
            success: function(response)
            {
                jQuery('#modal_ajax .modal-dialog').addClass(modelSize);
                jQuery('#modal_ajax .modal-title').html("");
                jQuery('#modal_ajax .modal-body').html("");
                jQuery('#modal_ajax .modal-title').html('<strong>' + title + '</strong>');
                jQuery('#modal_ajax .modal-body').html(response);
            }
        });
    }
}