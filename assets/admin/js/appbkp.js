var base_url = $("#myBase").attr('href');

$(function() {
    ecommerce.init();
    
}); 


ecommerce = {
  init:function () {
     
    ecommerce._loginvalidate();
    ecommerce._extrafunction();
    ecommerce._edituser();
    ecommerce._category();
    ecommerce._subcategory();
    ecommerce._couponcode();
  },
    _extrafunction:function(){

    $('#user_tab,#coupon_tab').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
     $('#category_tab,#payment_tab,#wish_tab,#order_tab').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });


  $('#picker1,#picker2').datetimepicker({
        format: "DD-MM-YYYY"
    })
    $(document).ready(function () {
        bsCustomFileInput.init();

    $('#state').change(function () {

      var state_id = $(this).val();
        ecommerce.ajax_req({id: state_id},"getcity").done(function (response) {
            var res = $.parseJSON(response);
                $("#city").empty();
                $("#city").append("<option value=''>Please select</option>");
                $.each(res,function (index,value){
                    $("#city").append("<option value='"+value.id+"' > "+value.name+" </option>");
                });
                 })
    })

    if($("#useredit").length > 0 ){
       var state_id =  $("#state").val();
       if(state_id)
       {
          ecommerce.ajax_req({id:state_id}, 'getcity').done(function (response) {
                    var res = $.parseJSON(response);

                $("#city").empty();
                $("#city").append("<option value=''>Please select</option>");
                var html = '';
                $.each(res,function (index,value){
                    if(value.id == $('#cityedit').val()){
                        html += "<option selected='selected' value='"+value.id+"' > "+value.name+" </option>";
                    }else{
                        html += "<option value='"+value.id+"' > "+value.name+" </option>";
                    }
                });
                $("#city").append(html);
          });
          return false;
       }
    }
  })

    },

    _loginvalidate: function () {
        $("#login-form").validate({
            rules: {
                username: {
                    required: true,
                },
                pass: {
                    required:true,
                },
            },
            messages: {
                username: {
                    required: "Please Enter Username",
                },
                pass: {
                    required:"Please Enter Password"
                },
               
            },
            errorClass: "error-class",
            errorElement: "div",
            errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
           unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
            submitHandler: function (form) {
              
        ecommerce.ajax_req(($("#login-form").serialize()), 'do_login').done(function (response) {
                    var res = $.parseJSON(response);

                    if(res.type == 'success'){
                      ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;
                    }else if(res.type == 'warning'){
                      ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                      window.location = base_url+res.url;
                    }else{
                      ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                    }
          });
          return false;
      }
        });
    },

     _edituser : function () {
        $("#edituser_form").validate({
            rules: {
                fname: {
                    required: true,   
                },   
                lname: {
                    required:true,
                },
                email:{
                    required:true,
                    email:true
                
                },
                mno :{
                    required:true,
                    digits:true,
                    minlength:10,
                    maxlength:12
                },
                password: {
                    required :true,
                },
                gender: {
                    required :true,
                },
                state:{
                    required:true,
                   
                },
                city:{
                    required:true,
                },
                zipcode :{
                    required:true,
                },
                
            },
            messages: {
                fname: {
                  required: "Please Enter Firstname",
                },
                lname: {
                    required :"Please Enter Lastname",
                },
                email:{
                    required:"Please Enter Email Address",
                    email:"Please Enter Valid Email"

                },
                mno: {
                    required:"Please Enter Mobile No",
                    digits:"Please Enter Valid Number",
                    minlength:"Mobile No is too short (minimum is 10 characters).",
                },
                password :{
                    required :"Please Enter Password",
                },
                gender :{
                    required :"Please Select Gender",
                },
                state :{
                    required :"Please Select State",
                    
                },
                city:{
                    required :"Please Select City",
                },
                zipcode :{
                    required:"Please Enter Zipcode",
                },
               
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                error.addClass( "help-block" );

                if ( element.prop( "type" ) === "checkbox" ) {
                    error.insertAfter( element.parent( "label" ) );
                }
                else if(element.prop( "class" ) === "form-control datetimepicker-input"){
                  error.insertAfter( element.parent( "div" ) );
                } 
                else {
                    error.insertAfter( element );
                }
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
         submitHandler: function (form) {
                var file_data = $('#user_photo').prop('files')[0];
                var form_data = new FormData($('#edituser_form')[0]);
                form_data.append('file', file_data);

                $.ajax({
                        url: base_url + 'userupdate', // point to server-side controller method
                       datatype : "application/json", // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                        var res = $.parseJSON(response);
                        if(res.type == 'success'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else if(res.type == 'warning'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else{
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        }
                        },
                        
                    });
                return false;
            }
        })
    },

   /* _deluser:function(){

        $(".deluser").click(function() {
        var delid = parseInt($(this).attr('del_userid'));
              

            const del_url = base_url+'deluser/'+delid;
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    showLoaderOnConfirm: true,
                    preConfirm: () => {
                    $.ajax({
                            type: 'POST',
                            url: del_url,
                            success: function(response){
                                var temp = JSON.parse(response);
                                    if(temp.type == 'success'){
                                        Swal.fire({
                                                title: temp.msg,
                                                type: "success",
                                                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                                                confirmButtonText: 'Ok',
                                                closeOnConfirm: false,
                                        })
                                    }else {
                                        Swal.fire("No records found", '', "error");
                                    }
                            }
                        });
                    }
        })
          
        });
    },*/


  notifyWithtEle: function (msg,type,pos,timeout) {
        pos = "";
    timeout = "";
        var noty = new Noty({
      theme:'metroui',
      text: msg,
      type: type,
      layout: (pos != "") ? pos : 'topRight',
      timeout: (timeout != "") ? timeout : 2000,
      closeWith: ['click'],
      animation: {
        open: 'animated slideInRight',
        close: 'animated slideOutRight'
      }

    });
        noty.show();
    },

    ajax_req: function(fields, url) {
      return $.ajax({
          url:base_url+url,
          type:'POST',
          data: fields,
          datatype : "application/json"
      });
    },

    _category:function(){
         $.validator.addMethod("validatecname", function(value, element, jdata) {
        var x = $.ajax({
            type: "POST",
            url: base_url + "verifycategory",
            async: false,
            dataType: "json",
            data: jdata,
        }).responseText;
    return (x === 'false') ? false : true;
    
    /*setTimeout(function(){
               return (x === 'false') ? false : true;
           }, 3000);*/
    }, "Category name is already available.");
      

$.validator.addMethod('filesize', function (value, element, param) {
  return this.optional(element) || (element.files[0].size <= param)
}, 'File size must be less than {10} bytes.');
     

        $("#category_form").validate({
       onkeyup: false,
       onclick: false,
       onfocusout: false,
            rules: {
                catname: {
                    required: true, 
                    validatecname: {
                    catname: function(){
                        return $("#catname").val();
                    },
                    _catname: function(){
                        return $("#_catname").val();
                    }
                }  
                },   
                catimage:
                {
                    filesize:10000000,
                }
            },
            messages: {
                catname: {
                  required: "Please Enter Category",
                },

            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                error.addClass( "help-block" );
                error.insertAfter( element );
                
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
         submitHandler: function (form) {
                var file_data = $('#catimage').prop('files')[0];
                var form_data = new FormData($('#category_form')[0]);
                form_data.append('file', file_data);
                var hcatid = $("#hcatid").val();
                if(hcatid)
                {
                    $.ajax({
                            url: base_url + 'updatecategory', // point to server-side controller method
                           datatype : "application/json", // what to expect back from the server
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                                 var res = $.parseJSON(response);
                                if(res.type == 'success'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else if(res.type == 'warning'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else{
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        }
                            },
                            
                        });
                    return false;
                }
                else
                {
                   
                    $.ajax({
                            url: base_url + 'addcategory', // point to server-side controller method
                           datatype : "application/json", // what to expect back from the server
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                                 var res = $.parseJSON(response);
                                if(res.type == 'success'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else if(res.type == 'warning'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else{
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        }
                            },
                            
                        });
                    return false;
                }
            }
        })

    },

    _payment:function(){
         $.validator.addMethod("validatepname", function(value, element, jdata) {
            var x = $.ajax({
                type: "POST",
                url: base_url + "verifypayment",
                async: false,
                dataType: "json",
                data: jdata,
            }).responseText;
        return (x === 'false') ? false : true;
        }, "Payment name is already available.");
      

        $("#payment_form").validate({
           onkeyup: false,
           onclick: false,
           onfocusout: false,
            rules: {
                payname: {
                    required: true, 
                    validatepname: {
                    payname: function(){
                        return $("#payname").val();
                    },
                    _payname: function(){
                        return $("#_payname").val();
                    }
                }  
                },   
                paydes: {
                    required: true,
                }
            },
            messages: {
                payname: {
                  required: "Please Enter Category",
                },
                paydes: {
                    required: "Please Enter Description",
                }
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                error.addClass( "help-block" );
                error.insertAfter( element );
                
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
         submitHandler: function (form) {
            
                var form_data =  $('#payment_form').serialize();
                var hpayid = $("#hpayid").val();

                if(hpayid)
                {
                    $.ajax({
                            url: base_url + 'updatepayment', // point to server-side controller method
                            datatype : "application/json", // what to expect back from the server
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                                 var res = $.parseJSON(response);
                                if(res.type == 'success'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else if(res.type == 'warning'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else{
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        }
                            },
                            
                        });
                    return false;
                }
                else
                {
                   
                    $.ajax({
                            url: base_url + 'addpayment', // point to server-side controller method
                            datatype : "application/json", // what to expect back from the server
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                                 var res = $.parseJSON(response);
                                if(res.type == 'success'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else if(res.type == 'warning'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else{
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        }
                            },
                            
                        });
                    return false;
                }
            }
        })

    },

    _delpayment:function(){

        $(".delpayment").click(function() {
        var delid = parseInt($(this).attr('del_payid'));
               /* $.ajax({
                    url: base_url + 'delpayment', // point to server-side controller method
                    datatype : "application/json", // what to expect back from the server
                    data: {delid:delid},
                    type: 'post',
                    success: function (response) {
                         var res = $.parseJSON(response);
                    ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                    window.location = base_url+res.url;
                    },
                    
                });*/

            const del_url = base_url+'delpayment/'+delid;
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    showLoaderOnConfirm: true,
                    preConfirm: () => {
                    $.ajax({
                            type: 'POST',
                            url: del_url,
                            success: function(response){
                                var temp = JSON.parse(response);
                                    if(temp.type == 'success'){
                                        Swal.fire({
                                                title: temp.msg,
                                                type: "success",
                                                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                                                confirmButtonText: 'Ok',
                                                closeOnConfirm: false,
                                        })
                                    }else {
                                        Swal.fire("No records found", '', "error");
                                    }
                            }
                        });
                    }
        })
          
        });
    },

    _pdetail:function(){
        $("#pdetail_form").validate({
           onkeyup: false,
           onclick: false,
           onfocusout: false,
            rules: {
                key: {
                    required: true, 
                },   
                value: {
                    required: true,
                }
            },
            messages: {
                key: {
                  required: "Please Enter key",
                },
                value: {
                    required: "Please Enter value",
                }
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                error.addClass( "help-block" );
                error.insertAfter( element );
                
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
         submitHandler: function (form) {
            
                var form_data =  $('#pdetail_form').serialize();
                    $.ajax({
                            url: base_url + 'addpdetail_form', // point to server-side controller method
                            datatype : "application/json", // what to expect back from the server
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                                 var res = $.parseJSON(response);
                                if(res.type == 'success'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else if(res.type == 'warning'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else{
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        }
                            },
                            
                        });
                    return false;
               
            }
        })

    },

    _subcategory:function(){

         $.validator.addMethod("validatesubcname", function(value, element, jdata) {
        var x = $.ajax({
            type: "POST",
            url: base_url + "verifysubcategory",
            async: false,
            dataType: "json",
            data: jdata,
        }).responseText;
    /* setTimeout(function(){
              return (x === 'false') ? false : true;
           }, 3000);*/
           //alert(x);return false;
     return (x === 'false') ? false : true;
     }, "Category name is already available.");


$.validator.addMethod('filesize', function (value, element, param) {
  return this.optional(element) || (element.files[0].size <= param)
}, 'File size must be less than {10} bytes.');
   
        $("#subcategory_form").validate({
        onkeyup: false,
       onclick: false,
       onfocusout: false,
            rules: {
                subcatname: {
                    required: true,   
                    validatesubcname: {
                    subcatname: function(){
                        return $("#subcatname").val();
                    },
                    _subcatname: function(){
                        return $("#_subcatname").val();
                    }
                }
                },   
                parent_id:
                {
                    required: true,
                },

                subcatimage:
                {
                    filesize:10000000
                }
            },
            messages: {
                subcatname: {
                  required: "Please Enter Category",
                },
                parent_id:{
                  required: "Please Select Parent Category"
                },

            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                error.addClass( "help-block" );
                error.insertAfter( element );
                
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
         submitHandler: function (form) {
                var hsubcatid = $("#hsubcatid").val();
                var file_data = $('#subcatimage').prop('files')[0];
                var form_data = new FormData($('#subcategory_form')[0]);
                form_data.append('file', file_data);
                if(hsubcatid)
                {
                    $.ajax({
                            url: base_url + 'updatesubcategory', // point to server-side controller method
                           datatype : "application/json", // what to expect back from the server
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                                 var res = $.parseJSON(response);
                                if(res.type == 'success'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else if(res.type == 'warning'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else{
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        }
                            },
                            
                        });
                    return false;
                }
                else
                {
                    
                    $.ajax({
                            url: base_url + 'addsubcategory', // point to server-side controller method
                            datatype : "application/json", // what to expect back from the server
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                                 var res = $.parseJSON(response);
                                if(res.type == 'success'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else if(res.type == 'warning'){
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else{
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        }
                            },
                            
                        });
                    return false;
                }
            }
        })

    },

    _couponcode : function () {
       jQuery.validator.addMethod("greaterThan", 
function(value, element, params) {

    if (!/Invalid|NaN/.test(new Date(value))) {
        return new Date(value) > new Date($(params).val());
    }

    return isNaN(value) && isNaN($(params).val()) 
        || (Number(value) > Number($(params).val())); 
},'Must be greater than From Date.');
        $("#coupon_form").validate({
            rules: {
                code: {
                    required: true,   
                },   
                coupondes: {
                    required:true,
                },
                coupontype:{
                    required:true,
                },
                amount :{
                    required:true,
                    digits:true,
                },
                fromdate: {
                    required :true,
                },
                todate: {
                    required :true,
                    greaterThan: "#fromdate"
                },     
            },
            messages: {
                code: {
                  required: "Please Enter Code",
                },
                coupondes: {
                    required :"Please Enter Description",
                },
                coupontype:{
                    required:"Please Enter Coupon Type",
                },
                amount: {
                    required:"Please Enter Amount",
                    digits:"Please Enter Only Number",
                },
                fromdate:{
                    required :"Please Select From Date",
                },
                todate :{
                    required :"Please Select To Date",
                },
               
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                error.addClass( "help-block" );
                console.log(element);

                if(element.prop( "class" ) === "form-control datetimepicker-input"){
                  error.insertAfter( element.parent( "div" ) );
                } 
                else {
                  error.insertAfter( element );
                }
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
         submitHandler: function (form) {
                var file_data = $('#couponimage').prop('files')[0];
                var form_data = new FormData($('#coupon_form')[0]);
                form_data.append('file', file_data);
                var hcouponid = $("#hcouponid").val();
                if(hcouponid)
                {
                    $.ajax({
                            url: base_url + 'updatecoupon', // point to server-side controller method
                           datatype : "application/json", // what to expect back from the server
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                            var res = $.parseJSON(response);
                            if(res.type == 'success'){
                                ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                                window.location = base_url+res.url;
                            }else if(res.type == 'warning'){
                                ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                                window.location = base_url+res.url;
                            }else{
                                ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            }
                            },
                            
                        });
                    return false;
                }
                else
                {
                    $.ajax({
                            url: base_url + 'addcoupon', // point to server-side controller method
                           datatype : "application/json", // what to expect back from the server
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                            var res = $.parseJSON(response);
                            if(res.type == 'success'){
                                ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                                window.location = base_url+res.url;
                            }else if(res.type == 'warning'){
                                ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                                window.location = base_url+res.url;
                            }else{
                                ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            }
                            },
                            
                        });
                    return false;
                }
            }
        })
    },
    _sweetalert: function (id) {
          const del_url = base_url+'deluser/'+id;
          Swal.fire({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!',
              showLoaderOnConfirm: true,
              preConfirm: () => {
                    $.ajax({
                            type: 'POST',
                            url: del_url,
                            success: function(response){
                                var temp = JSON.parse(response);
                                    if(temp.type == 'success'){
                                        Swal.fire({
                                                title: temp.msg,
                                                type: "success",
                                                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                                                confirmButtonText: 'Ok',
                                                closeOnConfirm: false,
                                        })
                                    }else {
                                        Swal.fire("No records found", '', "error");
                                    }
                            }
                        });
              }
        })
      }

}