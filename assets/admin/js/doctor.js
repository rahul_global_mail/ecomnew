var base_url = $("#myBase").attr('href');

$(function() {
    hitmine.init();
    
}); 

hitmine = {
	init:function () {
		
    hitmine._extrafunction();
    hitmine.approve();
    hitmine._addtodo();
	},

	 _extrafunction: function() {
	 	$('#appointment_tab,#staff_tab').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
	 	 $('#timepicker1').datetimepicker({
      format: 'LT'
    });
	 	  $('#picker1').datetimepicker({
        format: 'DD-MM-YYYY'
      })
	 },

	 notifyWithtEle: function (msg,type,pos,timeout) {
        pos = "";
		timeout = "";
        var noty = new Noty({
			theme:'metroui',
			text: msg,
			type: type,
			layout: (pos != "") ? pos : 'topRight',
			timeout: (timeout != "") ? timeout : 2000,
			closeWith: ['click'],
			animation: {
				open: 'animated slideInRight',
				close: 'animated slideOutRight'
			}
		});
        noty.show();
    },

	  ajax_req: function(fields, url) {
    	return $.ajax({
	        url:base_url+url,
	        type:'POST',
	        data: fields,
	        datatype : "application/json"
	    });
    },

	 cancel:function(){
	 	var id = $('#haid').val();
	 	hitmine.ajax_req({id:id}, 'cancelappointment').done(function (response) {
                    var res = $.parseJSON(response);
                    if(res.type == 'success'){
                        hitmine.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;
                    }else if(res.type == 'warning'){
                        hitmine.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;;
                    }else{
                        hitmine.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                    }
                });
                return false;
	 },

	 approve:function(){

jQuery.validator.addMethod("greaterThan", function(value, element, params) {
    return  (value > params); 
},'Appointment date Must be greater than' +new Date().getDate()+"-"+(new Date().getMonth()+1) +"-"+new Date().getFullYear());

var twoDigitMonth = ((new Date().getMonth().length+1) === 1)? (new Date().getMonth()+1) : '0' + (new Date().getMonth()+1);
var currentdate =new Date().getDate()+"-"+twoDigitMonth +"-"+new Date().getFullYear()
   $("#appointform").validate({
            rules: {
                dateofappointment:{
						required:true,
						greaterThan:currentdate
                },
                appointmentstime:
                {
                	required:true,
                }
            	},
            messages: {
                dateofappointment:{
                required:"Appointment date cannot be blank",
                },
                appointmentstime:{
                	required:"Appointment time cannot be blank",
                } 
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                error.addClass( "help-block" );
                if(element.prop( "class" ) === "form-control datetimepicker-input"){
                  error.insertAfter( element.parent( "div" ) );
                }
                else {
                    error.insertAfter( element );
                }
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
            submitHandler: function (form) {
              hitmine.ajax_req(($("#appointform").serialize()), 'approveappointment').done(function (response) {
                    var res = $.parseJSON(response);
                    if(res.type == 'success'){
                        hitmine.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;
                    }else if(res.type == 'warning'){
                        hitmine.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;
                    }else{
                        hitmine.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                    }
                });
                return false;
            }

        })

	 },
     _addtodo:function(){
         $("#todoform").validate({
            rules: {
                todo_subject: {
                    required: true,   
                },
            },
            messages: {
                todo_subject: {
                  required: "Please Enter Subject",
                },
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                error.addClass( "help-block" );
                error.insertAfter( element );
               
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
            submitHandler: function (form) {
                var htodoid = $("#htodoid").val();
                if(htodoid)
                {

                    hitmine.ajax_req(($("#todoform").serialize()), 'todoupdate').done(function (response) {
                    var res = $.parseJSON(response);
                    if(res.type == 'success'){
                        hitmine.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;
                    }
                });
                return false;

                }
                else
                {
                    hitmine.ajax_req(($("#todoform").serialize()), 'addtodo').done(function (response) {
                    var res = $.parseJSON(response);
                    if(res.type == 'success'){
                        hitmine.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;
                    }
                });
                return false;
                }
            }


        })
     }

}