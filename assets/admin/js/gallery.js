var base_url = $("#myBase").attr('href');

$(function() {
      gallery.init();
}); 

gallery = {
  init:function () {
    gallery._addgallery();
    gallery._galleryeditaction();
    gallery._extrafunction();
    },

        _extrafunction:function(){
    $(document).ready(function () {
        $('#delete_all').click(function(){
      var checkbox = $('.delete_checkbox:checked');
  
      if(checkbox.length > 0)
      {
       var checkbox_value = [];
       $(checkbox).each(function(){
        checkbox_value.push($(this).val());
       });
     
       Swal.fire({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!',
              showLoaderOnConfirm: true,
              preConfirm: () => {
                    $.ajax({
                            type: 'POST',
                            url: base_url+'delgallery',
                            data:{id:checkbox_value},
                            success: function(response){
                                var temp = JSON.parse(response);
                                    if(temp.type == 'success'){
                                        Swal.fire({
                                                title: temp.msg,
                                                type: "success",
                                                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                                                confirmButtonText: 'Ok',
                                                closeOnConfirm: false,
                                                onAfterClose:()=>{
                                                  redirecturl(temp.url)
                                                }
                                        })
                                    }else {
                                        Swal.fire("No records found", '', "error");
                                    }

                            }

                        });
              }
        }) ;
   function  redirecturl(url =""){
                 window.location.href = url;
              }

  }
  else
  {
   alert('Select atleast one records');
  }

 });
    })
    },

    _addgallery: function () {
        $("#gallery_form").validate({
          rules: {
                    title: {
                          required: true,   
                    },
                   
                },
                messages: {
                    title: {
                        required: "Please Enter Title",
                    },
                    
                },
          
          submitHandler : function(form){
                gallery.ajax_req(($("#gallery_form").serialize()), 'addgallery').done(function (response) {
                    var res = $.parseJSON(response);
                   gallery.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                    setTimeout(function(){ 
                        window.location = base_url+res.url;
                    }, 3000);
                return false;
                });
          }
        });
    },
 _galleryeditaction: function () {
        $("#editgallery_form").validate({

           rules: {
                    title: {
                          required: true,   
                    },
                   
                },
                messages: {
                    title: {
                        required: "Please Enter Title",
                    },
                    
                },
          
          
          submitHandler : function(form){

                var form_data = $('#editgallery_form').serializeArray();
    
                //Custom data
                gallery.ajax_req(form_data, 'updategallery').done(function (response) {
                    var res = $.parseJSON(response);
                    gallery.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                    setTimeout(function(){ 
                        window.location = base_url+res.url;
                    }, 3000);
                return false;
                });
          }
        });
    },

    ajax_req: function(fields, url) {
      return $.ajax({
          url:base_url+url,
          type:'POST',
          data: fields,
          datatype : "application/json"
      });
    },

    notifyWithtEle: function (msg,type,pos,timeout) {
        pos = "";
    timeout = "";
        var noty = new Noty({
      theme:'metroui',
      text: msg,
      type: type,
      layout: (pos != "") ? pos : 'topRight',
      timeout: (timeout != "") ? timeout : 2000,
      closeWith: ['click'],
      animation: {
        open: 'animated slideInRight',
        close: 'animated slideOutRight'
      }

    });
        noty.show();

    }
},
 