var base_url = $("#myBase").attr('href');

$(function() {
    ecommerce.init();
    
}); 


ecommerce = {
  init:function () {
     
   ecommerce._statusupdate();
  },
    _statusupdate: function(){
         $(".save_status").click(function() {
             var form_data =  $('#order_form').serialize();
               $.ajax({
                    url: base_url + 'statuschange_order', // point to server-side controller method
                    datatype : "application/json", // what to expect back from the server
                    data: form_data,
                    type: 'post',
                    success: function (response) {
                          var res = $.parseJSON(response);
                          ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                          window.location = base_url+res.url;
                       }
              });
        });
    },

    notifyWithtEle: function (msg,type,pos,timeout) {
      pos = "";
      timeout = "";
      var noty = new Noty({
      theme:'metroui',
      text: msg,
      type: type,
      layout: (pos != "") ? pos : 'topRight',
      timeout: (timeout != "") ? timeout : 2000,
      closeWith: ['click'],
      animation: {
        open: 'animated slideInRight',
        close: 'animated slideOutRight'
      }

    });
        noty.show();
    },
}