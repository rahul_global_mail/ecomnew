var base_url = $("#myBase").attr('href');
window.category_list = [];
window.pri_min = '';
window.pri_max = '';
window.att_list= [];


$(function() {
      products.init();
}); 

products = {
  init:function () {
    products._validproduct();
    products._valideditproduct();
   /* products._validattribute();*/
    products._addprodetail();
    products._galleryaction();
    products._extrafunction();
    products.search_prlist();
    
    },

    _extrafunction:function(){


    $(document).ready(function () {
       $('#delete_all').click(function(){
      var checkbox = $('.delete_checkbox:checked');
  
      if(checkbox.length > 0)
      {
       var checkbox_value = [];
       $(checkbox).each(function(){
        checkbox_value.push($(this).val());
       });
     
       Swal.fire({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!',
              showLoaderOnConfirm: true,
              preConfirm: () => {
                    $.ajax({
                            type: 'POST',
                            url: base_url+'delproduct',
                            data:{id:checkbox_value},
                            success: function(response){
                                var temp = JSON.parse(response);
                                    if(temp.type == 'success'){
                                        Swal.fire({
                                                title: temp.msg,
                                                type: "success",
                                                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                                                confirmButtonText: 'Ok',
                                                closeOnConfirm: false,
                                                onAfterClose:()=>{
                                                  redirecturl(temp.url)
                                                }
                                        })
                                    }else {
                                        Swal.fire("No records found", '', "error");
                                    }

                            }

                        });
              }
        }) ;
   function  redirecturl(url =""){
                 window.location.href = url;
              }

  }
  else
  {
   alert('Select atleast one records');
  }

 });

    var number_form_index=0;
    $("#addmore").click(function(){
       number_form_index++;
        var g = $("#attrdetail").clone();
        g.find("input").val("");
        g.appendTo("#att-body");
      });
    


    /*category chnage function start*/
    $('#category').change(function () {

      var cat_id = $(this).val();
        products.ajax_req({id: cat_id},"getsubcategory").done(function (response) {
            var res = $.parseJSON(response);
            
            if(res.length != '0'){
            cat_html = '';
            cat_html += ' <label>Sub category</label>'
                          +'<select class="form-control" name="subcategory" style="width: 100%; ">'
                          +'<option value="">Select Sub Category</option>';
                
                $.each(res,function (index,value){
                  cat_html += '<option value='+value._ID+' > '+value._Name+' </option>';
                });

              cat_html +=   '</select>';
            $('#subcategory').show();
            $("#subcategory").html(cat_html);
          }
          else{
            $('#subcategory').hide();
          }
        })
    })

    /*  $('#product_tab').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });*/

    /*category chnage function complete*/

    /*edit sub category fetch function start*/
    if($("#productedit").length > 0 ){
       var cat_id =  $("#category").val();
           if(cat_id)
           {
              products.ajax_req({id:cat_id}, 'getsubcategory').done(function (response) {
                var res = $.parseJSON(response);
                var html = '';

                html += ' <label>Sub category</label>'
                          +'<select class="form-control" name="subcategory" style="width: 100%; ">'
                          +'<option value="">Select Sub Category</option>';

                if(res.length != '0'){
                  $.each(res,function (index,value){
                      if(value._ID == $('#subcatedit').val()){
                          html += "<option selected='selected' value='"+value._ID+"' > "+value._Name+" </option>";
                      }else{
                          html += "<option value='"+value._ID+"' > "+value._Name+" </option>";
                      }
                  });
                  html +=   '</select>';
                  $('#subcategory').show();
                  $("#subcategory").append(html);
              }
              else{
                  $('#subcategory').hide();
              }
          });

       }
    }

    /*edit sub category fetch function complete*/


      /*attribute change function start*/
      $('#proattribute').change(function(){
      
      var pro_id = $(this).val();
      var attrid = $('#atid').val();
      var prid = $('#prid').val();

      if(attrid){
          products.ajax_req({aid: pro_id , pid : prid},"getattributdetails").done(function (response) {
            var res = $.parseJSON(response);
                
                console.log(res);
                if(res.length != '0'){
                var attr_html = '';
                $('#pro_attr_value').empty();
                $.each(res,function (index,value){
                  $.each(value._Attinfo,function (index,val){
                    attr_html += '<div class="col-md-6 float-left">'
                                    +'<div class="form-group">'
                                      +'<label>'+val.id+' Quantity : </label>'
                                      +'<input type="text" class="form-control" id="quantity"  name="quantity[]" placeholder="Enter Quantity" value="'+val.quantity+'">'
                                    +'</div>'
                                  +'</div>';
                   
                  $('#pro_attr_value').show();
                  $("#pro_attr_value").html(attr_html);
                  });
                });
              }
              else{
                products.ajax_req({id: pro_id},"getattributeval").done(function (response) {
                var res = $.parseJSON(response);
                
                var attr_html = '';
                $('#pro_attr_value').empty();
                $.each(res,function (index,value){
                 var r = value._Values.split(",");
                  $.each(r,function (index,val){
                    attr_html += '<div class="col-md-6 float-left">'
                                    +'<div class="form-group">'
                                      +'<label>'+val+' Quantity : </label>'
                                      +'<input type="text" class="form-control" id="quantity"  name="quantity[]" placeholder="Enter Quantity" >'
                                    +'</div>'
                                  +'</div>';
                   
                  $('#pro_attr_value').show();
                  $("#pro_attr_value").html(attr_html);
                  });
                });
                })
              }
            })
      }
      else{
        products.ajax_req({id: pro_id},"getattributeval").done(function (response) {
            var res = $.parseJSON(response);
                
                var attr_html = '';
                $('#pro_attr_value').empty();
                $.each(res,function (index,value){
                 var r = value._Values.split(",");
                  $.each(r,function (index,val){
                    attr_html += '<div class="col-md-6 float-left">'
                                    +'<div class="form-group">'
                                      +'<label>'+val+' Quantity : </label>'
                                      +'<input type="text" class="form-control" id="quantity"  name="quantity[]" placeholder="Enter Quantity" >'
                                    +'</div>'
                                  +'</div>';
                   
                  $('#pro_attr_value').show();
                  $("#pro_attr_value").html(attr_html);
                  });
                });
            })
          }
        });

      /*attribute change function complete*/

  });


    

  },

 /* getpriceproduct:function(){

    alert('welcome');
     
  },*/

  search_prlist:function() {


    /*search product list function start*/
    $('.sli_category').click(function() {
      $("#priceblock").show();
      $("#sizeblock").show();
        var id = $(this).attr('sub_cat');
        category_list.push(id);
        search_pro(category_list);
         products.ajax_req({catid:category_list}, 'getsize').done(function (response) {
                        var res = $.parseJSON(response);
                        /*window.location=base_url + 'productlist/'+res;*/
                      
                         data = '';
                         size_val ='';
                         var sarr={};
                        
                        for(var i = 0;i < res.length; i++){
                        var id = res[i]._ID;
                          sarr = res[i]._Values.split(',');

                         /* sarr['value'] =  res[i]._Values.split(',');
                          sarr['id'] = res[i]._ID;*/
                          /*var id = res[i]._ID;
                          console.log(id);*/
                          //sarr.push(res[i]._Values.split(','));
                          /*sarr['value'] = res[i]._Values.split(',');
                         sarr['id'] = res[i]._ID;*/
                          /*att_list.push(res[i]._Name);
                          
                      
                             var data = '';*/
                          /*for(var i = 0;i < att_list.length; i++)
                          {
                            
                             data += +'<div class="block-title">'
                            +' <span>'+att_list[i]+'</span>'
                            +'<div class="toggle-arrow"></div>'
                              +'</div>';
                            +'<div class="block-content"><ul class="size-list">'
                           
                            +' </ul>';
                            +'</div>'
                            +'<div class="bg-striped"></div>';
                          }
                          $('#size_fil').addClass('sidebar-block collapsed');
                          $('#size_fil').show();
                          $("#size_fil").html(data);
                          ech
                           */
for(var i = 0;i < sarr.length; i++){
                            size_val += '<li data-param="'+sarr[i]+'" data-id="'+id+'"><a>'
                                          +'<span class="clear"></span>'
                                          +'<span class="value" >'+sarr[i]+'</span></a></li>';
                          }
                           $('.size').html(size_val); 
    

                                
            }
           /* console.log(sarr);
            return false;*/
            
                                  /*$.each(sarr, function( key, value ) {
                            /*console.log(key);
                            console.log(value);*/
                            /*size_val += '<li  data-param="'+value+'"><a>'
                                          +'<span class="clear"></span>'
                                          +'<span class="value" id="'+value+'">'+value+'</span></a></li>';
  
});
                                  console.log(size_val);
                          $('.size').html(size_val);*/
            
                      });
                        return false;
        
    });
      var param = [];
      var id=[]
    $(".size-list").on("click",".size li",function(event){
      //var id = $(this).data('id');
      id.push($(this).data('id'));
       $(this).removeClass('active');
       $(this).addClass('active');
       param.push($(this).data('param'));

       prosize(param,id);
    })

   

    function prosize(param="",id="")
    {
       products.ajax_req({param:param,id:id}, 'search_size_pro').done(function (response) {
        var res = $.parseJSON(response);
            var data_html = '';
           
            for(var i = 0;i < res.length ; i++){
              
                data_html += '<div class="product-item large category1">'
                              +'<div class="product-item-inside">'
                                +'<div class="product-item-info">'
                                  +'<div class="product-item-photo">'
                                    +'<div class="product-item-label label-sale">'
                                      +'<span>-20%</span></div>'
                                      +'<div class="product-item-gallery-main">'
                                      +'<a href="'+base_url+'product_detail/'+res[i]['product']._ID+'">'
                                        +'<img class="product-image-photo" src="assets/uploads/product/'+res[i]['product']['_Image'][0]['cat']+'" alt="">'
                                      +'</a>'
                                    +'</div>'
                                    +'<a href="'+base_url+'quick_view/'+res[i]['product']._ID+'" title="Quick View" class="quick-view-link quick-view-btn">'
                                      +'<i class="icon icon-eye"></i><span>Quick View</span>'
                                    +'</a>'
                                  +'</div>'
                                  +'<div class="product-item-details">'
                                    +'<div class="product-item-name"> <a title="'+res[i]['product']._Name+'" href="'+base_url+'product_detail/'+res[i]['product']._ID+'" class="product-item-link">'+res[i]['product']._Name+'</a></div>'
                                      +'<div class="product-item-description">'+res[i]['product']._Description+'</div>'
                                        +'<div class="price-box"> <span class="price-container"> <span class="price-wrapper"> <span class="old-price">$190.00</span>'
                                          +'<span class="price special-price">'+res[i]['product']._Price+'</span> </span></span>'
                                        +'</div>'
                                      +'<div class="product-item-rating"> <i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i></div>'
                                     +'<a href="'+base_url+'addtocart_shop/'+res[i]['product']._ID+'">'
                                    +'<button class="btn add-to-cart" data-product="'+res[i]['product']._ID+'"> <i class="icon icon-cart"></i><span>Add to Cart</span> </button></a>'
                                  +'</div>'
                                +'</div>'
                              +'</div>'
                            +'</div>';
            }
            
            $('#before_search').hide();
            $('#after_search').show();
            $('#after_search').html(data_html);



      });
    }
   

   if ($('#priceSlider').length) {
        priceSlider();
    }

    

   function priceSlider() {
   
        var priceSlider = document.getElementById('priceSlider');

        noUiSlider.create(priceSlider, {
            start: [100, 10000],
            connect: true,
            step: 1,
            range: {
                'min': 100,
                'max': 3000
            }
        });

        var inputPriceMax = document.getElementById('priceMax');
        var inputPriceMin =

         document.getElementById('priceMin');
      
        priceSlider.noUiSlider.on('update', function(values, handle) {
            var value = values[handle];
            if (handle) {
                inputPriceMax.innerHTML = value;

            } else {
                inputPriceMin.innerHTML = value;
            }

            pri_min = $('#priceMin').html();
            pri_max = $('#priceMax').html();
            var hcatid = $("#hcatid").val();
            search_priceproduct(hcatid,pri_min,pri_max);
            /*search_priceproduct(hcatid,inputPriceMin,inputPriceMax);*/

        });


    }

    $('.remove_scategory').click(function(){
        var id = $(this).attr('rm_sub_cat');
        
          category_list.splice(category_list.indexOf(id),1);
          param.splice(param.indexOf(id),1);
          if(category_list.length === 0)
          {
             window.location = base_url+'productlist';
          }
          else
          {
           search_pro(category_list);
          }  

    });

  //products.ajax_req(({category : cat , pmin : pmin , pmax : pmax}), 'search_prolist_filter').done(function (response) {
    function search_pro(cat) {
          products.ajax_req(({category : cat }), 'search_prolist_filter').done(function (response) {
            var res = $.parseJSON(response);
            /*console.log(res);*/
           var subcatid = res[0]._SubcatID;
           /*console.log(subcatid);*/
            $("#hcatid").val(subcatid);
            var data_html = '';
            var id = '';
            for(var i = 0;i < res.length ; i++){
                data_html += '<div class="product-item large category1">'
                              +'<div class="product-item-inside">'
                                +'<div class="product-item-info">'
                                  +'<div class="product-item-photo">'
                                    +'<div class="product-item-label label-sale">'
                                      +'<span>-20%</span></div>'
                                      +'<div class="product-item-gallery-main">'
                                      +'<a href="'+base_url+'product_detail/'+res[i]._ID+'">'
                                        +'<img class="product-image-photo" src="assets/uploads/product/'+res[i]['_Image'][0]['cat']+'" alt="">'
                                      +'</a>'
                                    +'</div>'
                                    +'<a href="'+base_url+'quick_view/'+res[i]._ID+'" title="Quick View" class="quick-view-link quick-view-btn">'
                                      +'<i class="icon icon-eye"></i><span>Quick View</span>'
                                    +'</a>'
                                  +'</div>'
                                  +'<div class="product-item-details">'
                                    +'<div class="product-item-name"> <a title="'+res[i]._Name+'" href="'+base_url+'product_detail/'+res[i]._ID+'" class="product-item-link">'+res[i]._Name+'</a></div>'
                                      +'<div class="product-item-description">'+res[i]._Description+'</div>'
                                        +'<div class="price-box"> <span class="price-container"> <span class="price-wrapper"> <span class="old-price">$190.00</span>'
                                          +'<span class="price special-price">'+res[i]._Price+'</span> </span></span>'
                                        +'</div>'
                                      +'<div class="product-item-rating"> <i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i></div>'
                                     +'<a href="'+base_url+'addtocart_shop/'+res[i]._ID+'">'
                                    +'<button class="btn add-to-cart" data-product="'+res[i]._ID+'"> <i class="icon icon-cart"></i><span>Add to Cart</span> </button></a>'
                                  +'</div>'
                                +'</div>'
                              +'</div>'
                            +'</div>';
                            
            }
            $('#before_search').hide();
            $('#after_search').show();
            $('#after_search').html(data_html);

          

          });
    }
    /*search product list function complete*/
    function search_priceproduct(catid,minprice,maxprice){
      products.ajax_req(({category : catid ,minprice:minprice,maxprice:maxprice}), 'filterprice_product').done(function (response) {
            var res = $.parseJSON(response);
            var data_html = '';
            for(var i = 0;i < res.length ; i++){
                data_html += '<div class="product-item large category1">'
                              +'<div class="product-item-inside">'
                                +'<div class="product-item-info">'
                                  +'<div class="product-item-photo">'
                                    +'<div class="product-item-label label-sale">'
                                      +'<span>-20%</span></div>'
                                      +'<div class="product-item-gallery-main">'
                                      +'<a href="'+base_url+'product_detail/'+res[i]._ID+'">'
                                        +'<img class="product-image-photo" src="assets/uploads/product/'+res[i]['_Image'][0]['cat']+'" alt="">'
                                      +'</a>'
                                    +'</div>'
                                    +'<a href="'+base_url+'quick_view/'+res[i]._ID+'" title="Quick View" class="quick-view-link quick-view-btn">'
                                      +'<i class="icon icon-eye"></i><span>Quick View</span>'
                                    +'</a>'
                                  +'</div>'
                                  +'<div class="product-item-details">'
                                    +'<div class="product-item-name"> <a title="'+res[i]._Name+'" href="'+base_url+'product_detail/'+res[i]._ID+'" class="product-item-link">'+res[i]._Name+'</a></div>'
                                      +'<div class="product-item-description">'+res[i]._Description+'</div>'
                                        +'<div class="price-box"> <span class="price-container"> <span class="price-wrapper"> <span class="old-price">$190.00</span>'
                                          +'<span class="price special-price">'+res[i]._Price+'</span> </span></span>'
                                        +'</div>'
                                      +'<div class="product-item-rating"> <i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i></div>'
                                     +'<a href="'+base_url+'addtocart_shop/'+res[i]._ID+'">'
                                    +'<button class="btn add-to-cart" data-product="'+res[i]._ID+'"> <i class="icon icon-cart"></i><span>Add to Cart</span> </button></a>'
                                  +'</div>'
                                +'</div>'
                              +'</div>'
                            +'</div>';
            }
            $('#before_search').hide();
            $('#after_search').show();
            $('#after_search').html(data_html);
          });

  }
  },


    _validproduct: function () {
      $("#addproduct_form").validate({
                rules: {
                    name: {
                          required: true,   
                    },
                    description: {
                          required: true,   
                    },
                    category: {
                          required: true,   
                    },
                    subcategory: {
                          required: true,   
                    },
                    special: {
                          required: true,   
                    }
                },
                messages: {
                    name: {
                        required: "Please Enter Name",
                    },
                    description: {
                        required: "Please Enter Description",
                    },
                    category: {
                        required: "Please Select Category",
                    },
                    subcategory: {
                        required: "Please Select Subcategory",
                    },
                    special: {
                          required: "Please Select special",   
                    }
                },
                errorElement: "em",
                errorPlacement: function ( error, element ) {
                    error.addClass( "help-block" );
                      error.insertAfter( element );
                    
                },
                highlight: function ( element, errorClass, validClass ) {
                    $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
                },
                unhighlight: function (element, errorClass, validClass) {
                    $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
                },
               submitHandler: function (form) {
                  
                      products.ajax_req(($("#addproduct_form").serialize()), 'addproduct_action').done(function (response) {
                        var res = $.parseJSON(response);
                            products.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                      });
                        return false;
                    
                }
          })
    },

    _valideditproduct: function () {
      $("#productedit").validate({
                rules: {
                    name: {
                          required: true,   
                    },
                    description: {
                          required: true,   
                    },
                    category: {
                          required: true,   
                    },
                    subcategory: {
                          required: true,   
                    },
                    special: {
                          required: true,   
                    }
                },
                messages: {
                    name: {
                        required: "Please Enter Name",
                    },
                    description: {
                        required: "Please Enter Description",
                    },
                    category: {
                        required: "Please Select Category",
                    },
                    subcategory: {
                        required: "Please Select Subcategory",
                    },
                    special: {
                          required: "Please Select special",   
                    }
                },
                errorElement: "em",
                errorPlacement: function ( error, element ) {
                    error.addClass( "help-block" );
                      error.insertAfter( element );
                    
                },
                highlight: function ( element, errorClass, validClass ) {
                    $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
                },
                unhighlight: function (element, errorClass, validClass) {
                    $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
                },
               submitHandler: function (form) {
                  
                      products.ajax_req(($("#productedit").serialize()), 'editproduct_action').done(function (response) {
                        var res = $.parseJSON(response);
                            products.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                      });
                        return false;
                    
                }
          })
    },

   _addprodetail:function(){
        $("#adddetil_form").validate({
           onkeyup: false,
           onclick: false,
           onfocusout: false,
            rules: {
                key: {
                    required: true, 
                },   
                value: {
                    required: true,
                }
            },
            messages: {
                key: {
                  required: "Please Enter key",
                },
                value: {
                    required: "Please Enter value",
                }
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                error.addClass( "help-block" );
                error.insertAfter( element );
                
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
            submitHandler: function (form) {
            
                var form_data =  $('#adddetil_form').serialize();
                    $.ajax({
                            url: base_url + 'addprodetail_action', // point to server-side controller method
                            datatype : "application/json", // what to expect back from the server
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                                 var res = $.parseJSON(response);
                                  products.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                                  window.location = base_url+res.url;
                            },
                            
                        });
                    return false;
               
            }
        })

    },

    _galleryaction: function () {
        $("#gallery_form").validate({
          
          submitHandler : function(form){
            var data = new FormData();

                var form_data = $('#gallery_form').serializeArray();
                $.each(form_data, function (key, input) {
                    data.append(input.name, input.value);
                });

               //File data
                var file_data = $('input[name="attachfile[]"]')[0].files;
                
                for (var i = 0; i < file_data.length; i++) {
                    data.append("attachfile[]", file_data[i]);
                }

                //Custom data
                products.ajax_req_img(data, 'gallery_action').done(function (response) {
                    var res = $.parseJSON(response);
                    products.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                    setTimeout(function(){ 
                        window.location = base_url+res.url;
                    }, 3000);
                return false;
                });
          }
        });
    },

     ajax_req_img: function(fields, url, type = 'application/json') {
        return $.ajax({
            url:base_url+url,
            processData: false,
            contentType: false,
            type:'POST',
            data: fields,
            datatype : "application/json"
        });
    },


    ajax_req: function(fields, url) {
      return $.ajax({
          url:base_url+url,
          type:'POST',
          data: fields,
          datatype : "application/json"
      });
    },

    notifyWithtEle: function (msg,type,pos,timeout) {
        pos = "";
    timeout = "";
        var noty = new Noty({
      theme:'metroui',
      text: msg,
      type: type,
      layout: (pos != "") ? pos : 'topRight',
      timeout: (timeout != "") ? timeout : 2000,
      closeWith: ['click'],
      animation: {
        open: 'animated slideInRight',
        close: 'animated slideOutRight'
      }

    });
        noty.show();
    },

}