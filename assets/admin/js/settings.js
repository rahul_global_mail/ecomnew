var base_url = $("#myBase").attr('href');

$(function() {
    ecommerce.init();
    
}); 


ecommerce = {
  init:function () {
   ecommerce._generalform();
   ecommerce._socialform();
  },
    _generalform: function(){
        $("#general_form").validate({
           onkeyup: false,
           onclick: false,
           onfocusout: false,
            rules: {
                key: {
                    required: true, 
                },   
                value: {
                    required: true,
                }
            },
            messages: {
                key: {
                  required: "Please Enter Key",
                },
                value: {
                    required: "Please Enter Value",
                }
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                error.addClass( "help-block" );
                error.insertAfter( element );
                
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
         submitHandler: function (form) {
            
                var form_data =  $('#general_form').serialize();

                    $.ajax({
                            url: base_url + 'genderal_settings', // point to server-side controller method
                            datatype : "application/json", // what to expect back from the server
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                                  var res = $.parseJSON(response);
                                  ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                                  window.location = base_url+res.url;
                            },
                            
                        });
                    return false;
            }
        })
    },

     _socialform: function(){
        $("#social_form").validate({
          
         submitHandler: function (form) {
            
                var form_data =  $('#social_form').serialize();

                    $.ajax({
                            url: base_url + 'social_settings', // point to server-side controller method
                            datatype : "application/json", // what to expect back from the server
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                                  var res = $.parseJSON(response);
                                  ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                                  window.location = base_url+res.url;
                            },
                            
                        });
                    return false;
            }
        })
    },

    notifyWithtEle: function (msg,type,pos,timeout) {
      pos = "";
      timeout = "";
      var noty = new Noty({
      theme:'metroui',
      text: msg,
      type: type,
      layout: (pos != "") ? pos : 'topRight',
      timeout: (timeout != "") ? timeout : 2000,
      closeWith: ['click'],
      animation: {
        open: 'animated slideInRight',
        close: 'animated slideOutRight'
      }

    });
        noty.show();
    },
}