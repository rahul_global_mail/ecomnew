var base_url = $("#myBase").attr('href');

$(function() {
    user.init();
    
}); 
user = {
  init:function () {
  
    user._extrafunction();
    user._edituser();
   
  },
    _extrafunction:function(){
    $(document).ready(function () {
      bsCustomFileInput.init();
      $('#delete_all').click(function(){
      var checkbox = $('.delete_checkbox:checked');
  
      if(checkbox.length > 0)
      {
       var checkbox_value = [];
       $(checkbox).each(function(){
        checkbox_value.push($(this).val());
       });
     
       Swal.fire({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!',
              showLoaderOnConfirm: true,
              preConfirm: () => {
                    $.ajax({
                            type: 'POST',
                            url: base_url+'deluser',
                            data:{id:checkbox_value},
                            success: function(response){
                                var temp = JSON.parse(response);
                                    if(temp.type == 'success'){
                                        Swal.fire({
                                                title: temp.msg,
                                                type: "success",
                                                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                                                confirmButtonText: 'Ok',
                                                closeOnConfirm: false,
                                                onAfterClose:()=>{
                                                  redirecturl(temp.url)
                                                }
                                        })
                                    }else {
                                        Swal.fire("No records found", '', "error");
                                    }

                            }

                        });
              }
        }) ;
   function  redirecturl(url =""){
                 window.location.href = url;
              }

  }
  else
  {
   alert('Select atleast one records');
  }

 });

        bsCustomFileInput.init();


    $('#state').change(function () {

      var state_id = $(this).val();
        user.ajax_req({id: state_id},"getcity").done(function (response) {
            var res = $.parseJSON(response);
                $("#city").empty();
                $("#city").append("<option value=''>Please select</option>");
                $.each(res,function (index,value){
                    $("#city").append("<option value='"+value.id+"' > "+value.name+" </option>");
                });
                 })
    })

    if($("#useredit").length > 0 ){
       var state_id =  $("#state").val();
       if(state_id)
       {
          user.ajax_req({id:state_id}, 'getcity').done(function (response) {
                    var res = $.parseJSON(response);

                $("#city").empty();
                $("#city").append("<option value=''>Please select</option>");
                var html = '';
                $.each(res,function (index,value){
                    if(value.id == $('#cityedit').val()){
                        html += "<option selected='selected' value='"+value.id+"' > "+value.name+" </option>";
                    }else{
                        html += "<option value='"+value.id+"' > "+value.name+" </option>";
                    }
                });
                $("#city").append(html);
          });
          return false;
       }
    }
  })

    },

   
     _edituser : function () {
        $("#edituser_form").validate({
            rules: {
                fname: {
                    required: true,   
                },   
                lname: {
                    required:true,
                },
                email:{
                    required:true,
                    email:true
                
                },
                mno :{
                    required:true,
                    digits:true,
                    minlength:10,
                    maxlength:12
                },
                password: {
                    required :true,
                },
                gender: {
                    required :true,
                },
                state:{
                    required:true,
                   
                },
                city:{
                    required:true,
                },
                zipcode :{
                    required:true,
                },
                
            },
            messages: {
                fname: {
                  required: "Please Enter Firstname",
                },
                lname: {
                    required :"Please Enter Lastname",
                },
                email:{
                    required:"Please Enter Email Address",
                    email:"Please Enter Valid Email"

                },
                mno: {
                    required:"Please Enter Mobile No",
                    digits:"Please Enter Valid Number",
                    minlength:"Mobile No is too short (minimum is 10 characters).",
                },
                password :{
                    required :"Please Enter Password",
                },
                gender :{
                    required :"Please Select Gender",
                },
                state :{
                    required :"Please Select State",
                    
                },
                city:{
                    required :"Please Select City",
                },
                zipcode :{
                    required:"Please Enter Zipcode",
                },
               
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                error.addClass( "help-block" );

                if ( element.prop( "type" ) === "checkbox" ) {
                    error.insertAfter( element.parent( "label" ) );
                }
                else if(element.prop( "class" ) === "form-control datetimepicker-input"){
                  error.insertAfter( element.parent( "div" ) );
                } 
                else {
                    error.insertAfter( element );
                }
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
         submitHandler: function (form) {
                var file_data = $('#user_photo').prop('files')[0];
                var form_data = new FormData($('#edituser_form')[0]);
                form_data.append('file', file_data);

                $.ajax({
                        url: base_url + 'userupdate', // point to server-side controller method
                       datatype : "application/json", // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                        var res = $.parseJSON(response);
                        if(res.type == 'success'){
                            user.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else if(res.type == 'warning'){
                            user.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else{
                            user.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        }
                        },
                        
                    });
                return false;
            }
        })
    },

   

  notifyWithtEle: function (msg,type,pos,timeout) {
        pos = "";
    timeout = "";
        var noty = new Noty({
      theme:'metroui',
      text: msg,
      type: type,
      layout: (pos != "") ? pos : 'topRight',
      timeout: (timeout != "") ? timeout : 2000,
      closeWith: ['click'],
      animation: {
        open: 'animated slideInRight',
        close: 'animated slideOutRight'
      }

    });
        noty.show();
    },

    ajax_req: function(fields, url) {
      return $.ajax({
          url:base_url+url,
          type:'POST',
          data: fields,
          datatype : "application/json"
      });
    },

}
