var base_url = $("#myBase").attr('href');

$(function() {
    ecommerce.init();

    var $document = $(document);
    
}); 


ecommerce = {
  init:function () {
    ecommerce._registerform();
    ecommerce._profileform();
    ecommerce._propassform();
    ecommerce._loginform();
    ecommerce._forgot_pass();
    ecommerce._resetpass();
    ecommerce._getprice();
    ecommerce._reviewform();
    ecommerce._addtocart();
    ecommerce._validateadd();
    ecommerce._paymentform();
    ecommerce._search_all();
    ecommerce._extra();
   
  },
  _extra:function(){
    $(document).ready(function () {
       /* bsCustomFileInput.init();*/

    $('#state').change(function () {
      var state_id = $(this).val();
        ecommerce.ajax_req({id: state_id},"getallcity").done(function (response) {
            var res = $.parseJSON(response);
                $("#city").empty();
                $("#city").append("<option value=''>Please select</option>");
                $.each(res,function (index,value){
                    $("#city").append("<option value='"+value.id+"' > "+value.name+" </option>");
                });
                 })
    })

        if($("#useredit").length > 0 ){
       var state_id =  $("#state").val();
       if(state_id)
       {
          ecommerce.ajax_req({id:state_id}, 'getallcity').done(function (response) {
                    var res = $.parseJSON(response);

                $("#city").empty();
                $("#city").append("<option value=''>Please select</option>");
                var html = '';
                $.each(res,function (index,value){
                    if(value.id == $('#cityedit').val()){
                        html += "<option selected='selected' value='"+value.id+"' > "+value.name+" </option>";
                    }else{
                        html += "<option value='"+value.id+"' > "+value.name+" </option>";
                    }
                });
                $("#city").append(html);
          });
          return false;
       }
    }

   
$(".blog-post").click(function () {
  $(".blog-post").removeClass("active");
   $(".blog-post").css('border','');
   $(this).addClass("active");  
   $(this).css('border','1px solid black');
  var current = $(this).data('param');
  ecommerce.ajax_req({id:current}, 'checkaddress').done(function (response) {
            var res = $.parseJSON(response);
            $("#aname").val(res.data._Name);
            $("#addr1").val(res.data._Line1);
            $("#addr2").val(res.data._Line2);
            $("#zipcode").val(res.data._Postcode);
            $("#addr_id").val(res.data._ID);
            $("#state :selected").val(res.data.states.id).text(res.data.cities.name);
            $("#city :selected").val(res.data.cities.id).text(res.data.states.name);

            });
  return false;
    
});

/*$( "#priceSlider" ).change(function() {
  alert( "Handler for .change() called." );
  return false;
});
*/

  })
    $(".pay").change(function(){
        var payValue = $("input[type='radio']:checked").val();
        if(payValue==4)
        {
          $("#tprice").show();
        }
        
    });
  },

   _getprice:function(){

      $(document).on('click','.attr_data_get_size,.attr_data_get_color',function(e){
        e.preventDefault();
        var old_arr = $('#get_arr_data').attr('test_data');
        var new_arr = $.parseJSON(old_arr);

        var old_cart_arr = $('#get_cart_arr_data').attr('test_data');
        var new_cart_arr = $.parseJSON(old_cart_arr);
       
        var id = $(this).attr('att_detail');
        var att_id = $(this).attr('att_id');
        var att_value = $(this).data('value');
        var att_check = $(this).attr('att_check');
       
        var qu_html = '';
        var price_html = '';

        for(var i = 0; i <  new_arr.length ; i++){
          if(new_arr[i]._ID == att_id) {

            $('#size_val').val(new_arr[i]._Size);
            $('#color_val').val(new_arr[i]._Color);

            if(new_arr[i]._Quantity > 0){
              qu_html += '<div class="pro_abl product-availability" pro_aviable="1">'
                                +'Availability: <span>In stock</span>'
                              +'</div>';
            }
            else{
               qu_html += '<div class="pro_abl product-availability" pro_aviable="0">'
                                +'Availability: <span>Not available</span>'
                              +'</div>';
            }

            if(new_arr[i]._Quantity == null){
              quan = 0;
            }
            else{
              quan = new_arr[i]._Quantity;
            }

            var new_price1 = '';
                
                if(new_arr[i]._Sellprice > 0){
                  var main_price = new_arr[i]._Sellprice;
                }
                else{
                  var main_price = new_arr[i]._Price;
                }

             $('#qty_value').val('1');

            for(var j = 0 ; j < new_cart_arr.length ; j++){
             if(new_arr[i]._Sellprice > 0){

               if(new_arr[i]._Sellprice > 0){

                var orig_val = new_arr[i]._Sellprice;
              }
              else{
                var orig_val = new_arr[i]._Price;
              }
              
            for(var j = 0 ; j < new_cart_arr.length ; j++){
              if(new_cart_arr[j]._Subtotal != '' && new_cart_arr[j]._Size == new_arr[i]._Size && new_cart_arr[j]._Color == new_arr[i]._Color){
                var main_price = new_cart_arr[j]._Subtotal;
                var new_quan = new_cart_arr[j]._Quantity;
                $('#qty_value').attr('data-max',quan);
                $('#qty_value').val(new_cart_arr[j]._Quantity);
                new_price1 = new_cart_arr[j]._Subtotal;
              }
              else{
               $('#qty_value').attr('data-max',quan);
              }
            }

            price_html += '<span class="special-price" >'
                            +'<span id="main_price" price_val="'+main_price+'" orig_val="'+orig_val+'">';
                              if(new_price1!=''){
                                 price_html += new_price1;
                              }
                              else{
                                if(new_arr[i]._Sellprice > 0){
                                  price_html += '<strike style="color:#000;">'+new_arr[i]._Price+'</strike>'+new_arr[i]._Sellprice;
                                }else{
                                  price_html += new_arr[i]._Price;
                                }
                              }
            price_html += '</span></span>';
            $('.price_html').html(price_html);
          } 

        }
        if(att_check == 'size'){
            $( ".color li" ).each(function() {
              var new_id = $(this).find('a').attr('att_detail');
              console.log(new_id);
                if(new_id != att_value){
                  $(this).addClass('absent-option');
                  $(this).removeClass('active');
                }else{
                  $(this).removeClass('active');
                  $(this).removeClass('absent-option');
                }
            });
        }else{
          $( ".size li" ).each(function() {
              var new_id = $(this).find('a').attr('att_detail');
                if(new_id != att_value){
                  $(this).addClass('absent-option');
                  $(this).removeClass('active');
                }else{
                  $(this).removeClass('active');
                  $(this).removeClass('absent-option');
                }
            });
        }
        

        $('#quan_avble').empty();
        $('#quan_avble').html(qu_html);
        
      
      });


     $('.qty_change_dec').click(function(){
           var mainid = $(this).data('id');

          old_q = $('#qty_value'+mainid).attr('inc_val');
          new_q = parseInt(old_q) - 1;  
          old_price = $('#main_price'+mainid).attr('price_val');
          orig_val = $('#main_price'+mainid).attr('orig_val');
          new_price = parseInt(old_price) - parseInt(orig_val);

          $('#main_price'+mainid).attr('price_val',new_price);
          $('#main_price'+mainid).html(new_price);
          $('#main_price'+mainid).html(new_price);
          $('#qty_value'+mainid).val(new_q);
          $('#qty_value'+mainid).attr('inc_val',new_q);
          $('#sub_total'+mainid).val(new_price);
      });

      $('.qty_change_inc').click(function(){
          var mainid = $(this).data('id');

          old_q = $('#qty_value'+mainid).attr('inc_val');
          new_q = parseInt(old_q) + 1;
          old_price = $('#main_price'+mainid).attr('price_val');
          orig_val = $('#main_price'+mainid).attr('orig_val');
          new_price = parseInt(old_price) + parseInt(orig_val);

          $('#main_price'+mainid).attr('price_val',new_price);
          $('#main_price'+mainid).html(new_price);
          $('#main_price'+mainid).html(new_price);
          $('#qty_value'+mainid).val(new_q);
          $('#qty_value'+mainid).attr('inc_val',new_q);
          $('#sub_total'+mainid).val(new_price);
      });

      $('.qty_change_dec_pro').click(function(){
          old_q = $('#qty_value').val();
          new_q = parseInt(old_q) - 1;
          old_price = $('#main_price').attr('price_val');
          orig_val = $('#main_price').attr('orig_val');
          new_price = parseInt(old_price) - parseInt(orig_val);
          $('#main_price').attr('price_val',new_price);
          $('#main_price').html(new_price);
          $('#qty_value').val(new_q);
      });

      $('.qty_change_inc_pro').click(function(){

          var max_value = $('#qty_value').attr('data-max');

          old_q = $('#qty_value').val();
          new_q = parseInt(old_q) + 1;
         
          if(new_q > max_value){
            if(max_value == 0){ 
             ecommerce.notifyWithtEle( " Product is not available...","warning" ,'topRight', 2000);
            }
            else{
              ecommerce.notifyWithtEle( " You can't add more than "+max_value+" quantity.","warning" ,'topRight', 2000);
            }
          }
          else{
            old_price = $('#main_price').attr('price_val');
            orig_val = $('#main_price').attr('orig_val');
            new_price = parseInt(old_price) + parseInt(orig_val);
            $('#main_price').attr('price_val',new_price);
            $('#main_price').html(new_price);
            $('#qty_value').val(new_q);
          }
      });

      $('.update_cart').click(function(){
          ecommerce.ajax_req(($("#cart_update_form").serialize()), 'cart_update').done(function (response) {
                    var res = $.parseJSON(response);

                      ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;
          });
          return false;
      });

  	},
  	_getTotal:function (type) {
  		var total = 0;
  		$(".cart_subtotal").each(function() {
  			var suc = parseFloat($(this).attr('subtotal'));
  			if(type == 'add'){
  				total += suc;
  			}
  			if(type == 'minus'){
  				total -= suc;
  			}
  		});
  		total = Math.abs(total);
  		return total;
  	},
  _addtocart:function() {
        $('#addto_cart').click(function(){

            check_abl = $('.pro_abl').attr('pro_aviable');
            if(check_abl == 1){
              price = $('#main_price').attr('orig_val');
              stotal = $('#main_price').attr('price_val');
              size_val = $('#size_val').val();
              color_val = $('#color_val').val();
              qty = $('#qty_value').val();
              pid = $('#pr_id').val();
           
              ecommerce.ajax_req({size_val:size_val,color_val:color_val,price:price,qty:qty,pid:pid,stotal:stotal}, 'addtocart_cart/'+pid).done(function (response) {
              var res = $.parseJSON(response);

               ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                          window.location = base_url+res.url;
                    
              });
            }
            else{
              $('#modal_addtocart').modal('show');
            }


        });
    },

   _reviewform:function (){
    $("#review_from").validate({
            rules: {
                title: {
                    required: true,
                },
                review: {
                    required:true,
                },
            },
            messages: {
                title: {
                    required: "Please Enter Title.",
                },
                review: {
                    required:"Please Enter Review.",
                },
            },
            errorClass: "error-class",
            errorElement: "div",
            errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
           unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
            submitHandler: function (form) {

                   ecommerce.ajax_req(($("#review_from").serialize()), 'reviewform_action').done(function (response) {
                    var res = $.parseJSON(response);

                      ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;
                });
          return false;
          }
        });
  },


  _paymentform:function(){
$("#payment_form").validate({
            submitHandler: function (form) {
              
                   ecommerce.ajax_req(($("#payment_form").serialize()), 'createorder').done(function (response) {
                    var res = $.parseJSON(response);

                      ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;
                });

          return false;
          }
        });
  },

  _profileform:function (){

        $.validator.addMethod("validateemail", function(value, element, jdata) {
        var x = $.ajax({
            type: "POST",
            url: base_url+"email_exist",
            async: false,
            dataType: "json",
            data: jdata,
        }).responseText;
        
        return (x === 'false') ? false : true;

    }, "Email already exist!");
        
    $("#profile_form").validate({
            rules: {
                fname: {
                    required: true,
                },
                lname: {
                    required:true,
                },
                email: {
                  required:true,
                  email: true,
                  validateemail: {
                    email: function(){
                        return $("#email").val();
                    },
                    _email: function(){
                        return $("#_email").val();
                    },
                }  
             
                },
                mno :{
                    required:true,
                    digits:true,
                    minlength:10,
                    maxlength:12
                },
                gender: {
                    required :true,
                },
                
            },
            messages: {
                fname: {
                    required: "Please Enter First Name.",
                },
                lname: {
                    required:"Please Enter Last Name.",
                },
                email: {
                  required:"Please Enter Email ID.",
                },
                mno: {
                    required:"Please Enter Mobile No",
                    digits:"Please Enter Valid Number",
                    minlength:"Mobile No is too short (minimum is 10 characters).",
                },
                gender :{
                    required :"Please Select Gender",
                },
                
            },
            errorClass: "error-class",
            errorElement: "div",
            errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
           unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
            submitHandler: function (form) {
              var file_data = $('#user_photo').prop('files')[0];
                var form_data = new FormData($('#profile_form')[0]);
                form_data.append('file', file_data);

                $.ajax({
                        url: base_url + 'profile_action', // point to server-side controller method
                       datatype : "application/json", // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                        var res = $.parseJSON(response);
                            ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        },
                    });
                return false;
            }
              });
  },

   _propassform:function (){
    $("#profilepass_form").validate({
            rules: {
                cpass: {
                    required:true,
                },
                newpass: {
                   required:true,
                }
            },
            messages: {
                cpass: {
                    required:"Please Enter Password.",
                },
                newpass: {
                   required:"Please Enter New Password",
                }
            },
            errorClass: "error-class",
            errorElement: "div",
            errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
           unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
            submitHandler: function (form) {
              
              ecommerce.ajax_req(($("#profilepass_form").serialize()), 'propass_action').done(function (response) {
                var res = $.parseJSON(response);
                ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                window.location = base_url+res.url;
              });
              return false;
            }
              });
  },

  _validateadd:function(){
    $("#checkadd").validate({
            rules: {
                aname: {
                    required: true,
                },
                addr1: {
                    required:true,
                },
                addr2: {
                  required:true,
                },
                zip: {
                    required:true,
                },
                state:{
                  required:true,
                },
                city:{
                  required:true,
                }
            },
            messages: {
                aname: {
                    required: "Please Enter Address.",
                },
                addr1: {
                    required:"Please Enter Address.",
                },
                addr2: {
                  required:"Please Enter Address.",
                },
                zip: {
                    required:"Please Enter Zipcode.",
                },
                state:{
                  required:"Please Enter Zipcode.",
                },
                city:{
                  required:"Please Enter Zipcode."
                  ,
                }
            },
            errorClass: "error-class",
            errorElement: "div",
            errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
           unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
            submitHandler: function (form) {
              
                   ecommerce.ajax_req(($("#checkadd").serialize()), 'otheradd').done(function (response) {
                    var res = $.parseJSON(response);

                      ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;
                });
          return false;
          }
            
        });

  },

  _registerform:function (){

     $.validator.addMethod("validateemail", function(value, element, jdata) {
        var x = $.ajax({
            type: "POST",
            url: base_url+"email_exist",
            async: false,
            dataType: "json",
            data: jdata,
        }).responseText;
        
        return (x === 'false') ? false : true;

    }, "Email already exist!");
    $("#register_form").validate({
            rules: {
                fname: {
                    required: true,
                },
                lname: {
                    required:true,
                },
                mno: {
                    required:true,
                    digits:true,
                    minlength:10,
                },
                email: {
                  required:true,
                  email: true,
                  validateemail: {
                    email: function(){
                        return $("#email").val();
                    },
                  } 
                },
                pass: {
                    required:true,
                },
            },
            messages: {
                fname: {
                    required: "Please Enter First Name.",
                },
                lname: {
                    required:"Please Enter Last Name.",
                },
                mno:{
                  required:"Please Enter Mobile no",
                  digits:"Please Enter only digit"
                },
                email: {
                  required:"Please Enter Email ID.",
                  remote: "Email already exist."
                },
                pass: {
                    required:"Please Enter Password.",
                },
            },
            errorClass: "error-class",
            errorElement: "div",
            errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
           unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
            submitHandler: function (form) {
              
                   ecommerce.ajax_req(($("#register_form").serialize()), 'register_action').done(function (response) {
                    var res = $.parseJSON(response);

                      ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;
                });
          return false;
          }
        });
  },

  _loginform: function(){
     $("#login_form").validate({
            rules: {
                email: {
                    required: true,
                },
                pass: {
                    required:true,
                },
            },
            messages: {
                email: {
                    required: "Please Enter Email ID.",
                },
                pass: {
                    required:"Please Enter Password.",
                },
               
            },
            errorClass: "error-class",
            errorElement: "div",
            errorPlacement: function(error, element) {
                  if(element.parent('.input-group').length) {
                      error.insertAfter(element.parent());
                  } else {
                      error.insertAfter(element);
                  }
              },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
           unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
            submitHandler: function (form) {
              
                ecommerce.ajax_req(($("#login_form").serialize()), 'login_action').done(function (response) {
                    var res = $.parseJSON(response);

                      ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;
           });
          return false;
          }
        });
  },

  _search_all:function(){
    $('#search_id').change(function(){
     /* var search_id = $('#search_id').val();
       ecommerce.ajax_req({search_id : search_id}, 'all_search_action').done(function (response) {
                var res = $.parseJSON(response);

                  ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                    window.location = base_url+res.url;
       });*/
       $('#all_search').submit();
    });
    
  },

  _forgot_pass:function(){
        $("#forgot_form").validate({
            rules: {
                email: {
                    required: true,
                }
            },
            messages: {
                email: {
                    required: "Please Enter Email ID.",
                }
            },
            errorClass: "error-class",
            errorElement: "div",
            errorPlacement: function(error, element) {
                  if(element.parent('.input-group').length) {
                      error.insertAfter(element.parent());
                  } else {
                      error.insertAfter(element);
                  }
              },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
           unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
            submitHandler: function (form) {
              
                ecommerce.ajax_req(($("#forgot_form").serialize()), 'forgotpass_action').done(function (response) {
                    var res = $.parseJSON(response);

                      ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;
             });
            return false;
            }
        });
  },

  _resetpass: function() {
     $("#reset_form").validate({
          rules : {
            pass: "required",
            cpass: {
                equalTo: "#password"
            }
          },
          messages : {
            pass : {
              required : "Please Enter Password."
            },
            cpass :"Enter Confirm Password Same as Password"
          },
           submitHandler: function (form) {
              
                   ecommerce.ajax_req(($("#reset_form").serialize()), 'resetpass_action').done(function (response) {
                    var res = $.parseJSON(response);

                      ecommerce.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;
                });
          return false;
          }
       });
  },
 
  notifyWithtEle: function (msg,type,pos,timeout) {
        pos = "";
    timeout = "";
        var noty = new Noty({
      theme:'metroui',
      text: msg,
      type: type,
      layout: (pos != "") ? pos : 'topRight',
      timeout: (timeout != "") ? timeout : 2000,
      closeWith: ['click'],
      animation: {
        open: 'animated slideInRight',
        close: 'animated slideOutRight'
      }

    });
        noty.show();
    },

    ajax_req: function(fields, url) {
      return $.ajax({
          url:base_url+url,
          type:'POST',
          data: fields,
          datatype : "application/json"
      });
    },
    showAjaxModal: function (url,title,size) {
        // SHOWING AJAX PRELOADER IMAGE
        jQuery('#modal_ajax .modal-body').html('<div style="text-align:center;"><img src="http://localhost/storeapp/assets/images/loading.gif" style="max-width: 150px;"/></div>');

        // LOADING THE AJAX MODAL
        jQuery('#modal_ajax').modal('show', {backdrop: 'true'});
    	
    	var modelSize = (size) ? 'modal-lg' : '';
      
      var modelSize = (size) ? size : '';
        // SHOW AJAX RESPONSE ON REQUEST SUCCESS
        $.ajax({
            url: url,
            success: function(response)
            {
                jQuery('#modal_ajax .modal-dialog').addClass(modelSize);
                jQuery('#modal_ajax .modal-title').html("");
                jQuery('#modal_ajax .modal-body').html("");
                jQuery('#modal_ajax .modal-title').html('<strong>' + title + '</strong>');
                jQuery('#modal_ajax .modal-body').html(response);
            }
        });
    }
}