-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 17, 2020 at 02:23 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `commerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `ec_coupon`
--

DROP TABLE IF EXISTS `ec_coupon`;
CREATE TABLE IF NOT EXISTS `ec_coupon` (
  `_ID` int(11) NOT NULL AUTO_INCREMENT,
  `_Code` varchar(256) DEFAULT NULL,
  `_Description` varchar(255) DEFAULT NULL,
  `_Type` int(11) DEFAULT NULL,
  `_Amount` int(100) DEFAULT NULL,
  `_Fromdate` date DEFAULT NULL,
  `_Todate` date DEFAULT NULL,
  `_CatID` json DEFAULT NULL,
  `_Status` int(11) DEFAULT NULL,
  `_Image` varchar(256) DEFAULT NULL,
  `_Created` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ec_coupon`
--

INSERT INTO `ec_coupon` (`_ID`, `_Code`, `_Description`, `_Type`, `_Amount`, `_Fromdate`, `_Todate`, `_CatID`, `_Status`, `_Image`, `_Created`) VALUES
(1, '50Off', 'ef', 2, 50, '2014-07-20', '2024-07-20', NULL, 1, 'amg-gt-r-thumb.png', '2020-07-14 10:07:10.000000'),
(3, '70off', 'sdasd', 2, 70, '2020-07-20', '2020-07-20', '[\"39\", \"41\"]', 1, '1D553EA.jpg', '2020-07-17 12:28:45.000000');
COMMIT;
