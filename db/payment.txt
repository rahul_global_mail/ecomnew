
--
-- Table structure for table `ec_payment`
--

DROP TABLE IF EXISTS `ec_payment`;
CREATE TABLE IF NOT EXISTS `ec_payment` (
  `_ID` int(11) NOT NULL AUTO_INCREMENT,
  `_Name` varchar(256) DEFAULT NULL COMMENT 'getway name',
  `_Description` varchar(500) DEFAULT NULL,
  `_Details` varchar(500) DEFAULT NULL,
  `_Created` datetime DEFAULT NULL,
  PRIMARY KEY (`_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ec_payment`
--

INSERT INTO `ec_payment` (`_ID`, `_Name`, `_Description`, `_Details`, `_Created`) VALUES
(1, 'paypal', 'paypal description', '{\"key\":[\"test01@gmail.com\",\"test04@gmail.com\"],\"value\":[\"test01value@gmail.com\",\"test04value@gmail.com\"]}', '2020-07-10 08:53:05'),
(2, 'stripe', 'stripe descirption', '{\"key\":[\"dev01@gmail.com\",\"dev02@gmail.com\",\"dev03@gmail.com\"],\"value\":[\"dev01value@gmail.com\",\"dev02value@gmail.com\",\"dev03value@gmail.com\"]}', '2020-07-09 09:54:16'),
(3, 'paytm', 'paytm description', NULL, '2020-07-09 09:54:38');
COMMIT;
