-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 24, 2020 at 02:13 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `commerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `ec_users`
--

DROP TABLE IF EXISTS `ec_users`;
CREATE TABLE IF NOT EXISTS `ec_users` (
  `_ID` int(11) NOT NULL AUTO_INCREMENT,
  `_Firstname` varchar(100) DEFAULT NULL,
  `_Lastname` varchar(100) DEFAULT NULL,
  `_Email` varchar(100) DEFAULT NULL,
  `_Mobile` bigint(100) DEFAULT NULL,
  `_Password` varchar(50) DEFAULT NULL,
  `_Gender` varchar(50) DEFAULT NULL,
  `_Profilepic` varchar(255) DEFAULT NULL,
  `reset_token` varchar(255) DEFAULT NULL,
  `_Created` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ec_users`
--

INSERT INTO `ec_users` (`_ID`, `_Firstname`, `_Lastname`, `_Email`, `_Mobile`, `_Password`, `_Gender`, `_Profilepic`, `reset_token`, `_Created`) VALUES
(3, 'yogita patil p', 'patil', 'yogita@gmail.com', 4785123690, 'dfad8cef05dce92abf4164c2fe548985', 'Female', '0_a43d7_81bdfcad_XXXL.jpg', NULL, '2020-07-04 12:18:53.000000'),
(4, 'tejubcv', 'patel', 't@gmail.com', 4758963210, '4c1625d7eb96f8b43cb33e0459b0e105', 'Female', '154673.jpg', NULL, '2020-07-04 12:51:40.000000'),
(5, 'Rahul', 'Patel', 'rahul@gmail.com', 9090909090, '9746ed8e99d7774ea6f7527df7800239', 'Male', NULL, NULL, '2020-07-16 18:08:55.000000'),
(6, 'Rahul', 'Patel', 'ram@gmail.com', 7589632100, '9001587b0fed20a2cb33e43a51bb3f83', 'Male', 'images (1).jpg', '5b7eb31dcd8dd2c630e736f402a70187', '2020-07-23 13:02:55.000000'),
(8, 'mansi', 'vani', 'm@gmail.com', NULL, '43f07713440bb0d1c09374ed583391bf', NULL, NULL, NULL, '2020-07-24 09:07:01.000000');
COMMIT;
